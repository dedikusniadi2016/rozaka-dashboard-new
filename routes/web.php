<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {return view('welcome');});
Route::get('/', function () { return redirect('dashboard/index'); });

// auth routes
Auth::routes();

/* Dashboard */
Route::get('dashboard', function () { return redirect('dashboard/index'); });
Route::get('dashboard/index', 'DashboardController@index')->name('dashboard.index');

/* Profile */
Route::get('profile', function () { return redirect('profile/my-profile'); });
Route::get('profile/my-profile', 'ProfileController@myProfile')->name('profile.my-profile');

/* Master */
Route::get('master', function() { return redirect('master/outlets'); });
Route::get('master/employees', 'MasterController@employees')->name('master.employees');

/* Outlet */
// Route::get('outlets', function () { return redirect('outlets'); });
Route::get('outlets', 'OutletController@index')->name('outlets');
Route::get('outlets/create', 'OutletController@create')->name('outlets.create');
Route::get('outlets/edit/{outlet}', 'OutletController@edit')->name('outlets.edit');
Route::post('outlets', 'OutletController@store')->name('outlets.store');
Route::put('outlets/{outlet}', 'OutletController@update')->name('outlets.update');
// Route::resource('outlets', 'OutletController');

/* Employees */
// Route::get('employees', function() { return redirect('employees'); });
Route::get('employees', 'EmployeeController@index')->name('employees');
Route::get('employees/create', 'EmployeeController@create')->name('employees.create');
Route::get('employees/edit/{employee}', 'EmployeeController@edit')->name('employees.edit');
Route::post('employees', 'EmployeeController@store')->name('employees.store');
Route::put('employees/{employee}', 'EmployeeController@update')->name('employees.update');

// pecarian
//Route::get('employees/search', 'EmployeeController@search')->name('employees.search');

/* Category Ingredient */
Route::get('ingredient', function() { return redirect('ingcats'); });
Route::get('ingredient/ingcats', 'IngcatController@index')->name('ingcats');
Route::get('ingredient/ingcats/create', 'IngcatController@create')->name('ingcats.create');
Route::get('ingredient/ingcats/edit/{ingcat}', 'IngcatController@edit')->name('ingcats.edit');
Route::post('ingredient/ingcats', 'IngcatController@store')->name('ingcats.store');
Route::put('ingredient/ingcats/{ingcat}', 'IngcatController@update')->name('ingcats.update');

// pencarian
//Route::get('ingredient/ingcats/search', 'IngcatController@search')->name('ingcats.search');

/* Ingredient */
Route::get('ingredient/ingredients', 'IngredientController@index')->name('ingredients');
Route::get('ingredient/ingredients/create', 'IngredientController@create')->name('ingredients.create');
Route::get('ingredient/ingredients/edit/{ingredient}', 'IngredientController@edit')->name('ingredients.edit');
Route::post('ingredient/ingredients', 'IngredientController@store')->name('ingredients.store');
Route::put('ingredient/ingredients/{ingredient}', 'IngredientController@update')->name('ingredients.update');

// pencarian
//Route::get('ingredient/ingredients/search', 'IngredientController@search')->name('ingredients.search');

/* customers */
Route::get('customers', 'CustomerController@index')->name('customers');
Route::get('customers/create', 'CustomerController@create')->name('customers.create');
Route::get('customers/edit/{customer}', 'CustomerController@edit')->name('customers.edit');
Route::post('customers', 'CustomerController@store')->name('customers.store');
Route::put('customers/{customer}', 'CustomerController@update')->name('customers.update');

// pencarian
//Route::get('customers/search', 'CustomerController@search')->name('customers.search');


/* Foods Management */
Route::get('food', function() { return redirect('food/maincategory'); });
Route::get('food/maincategory', 'FoodmCategoryController@index')->name('foodm');
Route::get('food/maincategory/create', 'FoodmCategoryController@create')->name('foodm.create');
Route::get('food/maincategory/edit/{foodmcategory}', 'FoodmCategoryController@edit')->name('foodm.edit');
Route::post('food/maincategory', 'FoodmCategoryController@store')->name('foodm.store');
Route::put('food/maincategory/{foodmcategory}', 'FoodmCategoryController@update')->name('foodm.update');

// pencarian
//Route::get('food/maincategory/search', 'FoodmCategoryController@search')->name('foodm.search');

/* Foods Category */
Route::get('food/category', 'FoodCategoryController@index')->name('foodc');
Route::get('food/category/create', 'FoodCategoryController@create')->name('foodc.create');
Route::get('food/category/edit/{foodcategory}', 'FoodCategoryController@edit')->name('foodc.edit');
Route::post('food/category', 'FoodCategoryController@store')->name('foodc.store');
Route::put('food/category/{foodcategory}', 'FoodCategoryController@update')->name('foodc.update');
/* modifier */
Route::get('food/modifiers', 'ModifierController@index')->name('modifiers');
Route::get('food/modifiers/create', 'ModifierController@create')->name('modifiers.create');
Route::get('food/modifiers/edit/{modifier}', 'ModifierController@edit')->name('modifiers.edit');
Route::post('food/modifiers', 'ModifierController@store')->name('modifiers.store');
Route::put('food/modifiers/{modifier}', 'ModifierController@update')->name('modifiers.update');

Route::get('food/modifiers/ingredient/{modifier}', 'ModifierController@ingredient')->name('modifiers.ingredient');
Route::post('food/modifiers/ingredient/{modifier}', 'ModifierController@addingredient')->name('modifiers.addingredient');
Route::delete('food/modifiers/ingredient/{modifieringredient}', 'ModifierController@destroyingredient')->name('modifiers.destroyingredient');
/*  food menu */
Route::get('food/foodmenus', 'FoodMenuController@index')->name('foodmenus');
Route::get('food/foodmenus/create', 'FoodMenuController@create')->name('foodmenus.create');
Route::get('food/foodmenus/edit/{foodmenu}', 'FoodMenuController@edit')->name('foodmenus.edit');
Route::post('food/foodmenus', 'FoodMenuController@store')->name('foodmenus.store');
Route::put('food/foodmenus/{foodmenu}', 'FoodMenuController@update')->name('foodmenus.update');
/*ingredient food */
Route::get('food/foodmenus/ingredient/{foodmenu}', 'FoodMenuController@ingredient')->name('foodmenus.ingredient');
Route::post('food/foodmenus/ingredient/{foodmenu}', 'FoodMenuController@addingredient')->name('foodmenus.addingredient');
Route::delete('food/foodmenus/ingredient/{foodmenuingredient}', 'FoodMenuController@destroyingredient')->name('foodmenus.destroyingredient');
/* modifier ingredient */
Route::get('food/foodmenus/modifier/{foodmenu}', 'FoodMenuController@modifier')->name('foodmenus.modifier');
Route::post('food/foodmenus/modifier/{foodmenu}', 'FoodMenuController@addmodifier')->name('foodmenus.addmodifier');
Route::delete('food/foodmenus/modifier/{foodmodifier}', 'FoodMenuController@destroymodifier')->name('foodmenus.destroymodifier');

/* food promo */
Route::get('food/promo', 'FoodPromoController@index')->name('foodp');
Route::get('food/promo/create', 'FoodPromoController@create')->name('foodp.create');
Route::get('food/promo/{foodpromo}', 'FoodPromoController@edit')->name('foodp.edit');
Route::post('food/promo', 'FoodPromoController@store')->name('foodp.store');
Route::put('food/promo/{foodpromo}', 'FoodPromoController@update')->name('foodp.update');


// supplier
Route::get('suppliers', 'SuppliersController@index')->name('suppliers');
Route::get('suppliers/create', 'SuppliersController@create')->name('suppliers.create');
Route::get('suppliers/edit/{supplier}', 'SuppliersController@edit')->name('suppliers.edit');
Route::post('suppliers', 'SuppliersController@store')->name('suppliers.store');
Route::put('suppliers/{supplier}', 'SuppliersController@update')->name('suppliers.update');


// tables
Route::get('tables', 'TableController@index')->name('tables');
Route::get('tables/create', 'TableController@create')->name('tables.create');
Route::get('tables/edit/{table}', 'TableController@edit')->name('tables.edit');
Route::post('tables', 'TableController@store')->name('tables.store');
Route::put('tables/{table}', 'TableController@update')->name('tables.update');

// iventory
Route::get('iventory', 'IventoryController@index')->name('iventory');
Route::get('iventory/create', 'IventoryController@create')->name('iventory.create');
Route::get('iventory/edit/{iventory}', 'IventoryController@edit')->name('iventory.edit');
Route::post('iventory', 'IventoryController@store')->name('iventory.store');
Route::put('iventory/{iventory}', 'IventoryController@update')->name('iventory.update');

//iventory ingredient
Route::get('iventory/ingredient/{iventory}', 'IventoryController@ingredient')->name('iventory.ingredient');
Route::post('iventory/ingredient/{iventory}', 'IventoryController@addingredient')->name('iventory.addingredient');
Route::delete('iventory/ingredient/{iventory}', 'IventoryController@destroyingredient')->name('iventory.destroyingredient');

//wastes
Route::get('wastes', 'WastesController@index')->name('wastes');
Route::get('wastes/create', 'WastesController@create')->name('wastes.create');
Route::post('wastes', 'WastesController@store')->name('wastes.store');
Route::get('wastes/edit/{wastes}', 'WastesController@edit')->name('wastes.edit');
Route::put('wastes/{wastes}', 'WastesController@update')->name('wastes.update');

Route::get('wastes/ingredient/{wastes}', 'WastesController@ingredient')->name('wastes.ingredient');
Route::post('wastes/ingredient/{wastes}', 'WastesController@addingredient')->name('wastes.addingredient');
Route::delete('wastes/ingredient/{wastes}', 'WastesController@destroyingredient')->name('wastes.destroyingredient');



