-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table db_waroeng.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_waroeng.migrations: ~0 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_waroeng.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_attendance
CREATE TABLE IF NOT EXISTS `tbl_attendance` (
  `id` varchar(36) NOT NULL,
  `reference_no` varchar(50) NOT NULL,
  `employee_id` varchar(36) NOT NULL,
  `date` date NOT NULL,
  `in_time` time NOT NULL,
  `out_time` time NOT NULL,
  `note` text DEFAULT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(50) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_attendance_employee_id_foreign` (`employee_id`),
  KEY `tbl_attendance_user_id_foreign` (`user_id`),
  KEY `tbl_attendance_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_attendance_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employees` (`id`),
  CONSTRAINT `tbl_attendance_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_attendance_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_attendance: ~0 rows (approximately)
DELETE FROM `tbl_attendance`;
/*!40000 ALTER TABLE `tbl_attendance` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_attendance` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_customers
CREATE TABLE IF NOT EXISTS `tbl_customers` (
  `id` varchar(36) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(300) DEFAULT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `date_of_birth` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tbl_customers_email_unique` (`email`),
  KEY `tbl_customers_user_id_foreign` (`user_id`),
  KEY `tbl_cusomters_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_cusomters_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_customers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_customers: ~3 rows (approximately)
DELETE FROM `tbl_customers`;
/*!40000 ALTER TABLE `tbl_customers` DISABLE KEYS */;
INSERT INTO `tbl_customers` (`id`, `name`, `phone`, `email`, `address`, `user_id`, `outlet_id`, `del_status`, `date_of_birth`, `created_at`, `updated_at`) VALUES
	('80b8cba9-feb3-42c7-8524-23eff8e6dcdd', 'Novel Basewad', '085227281672', 'adQ@gmail.com', 'Jakarta', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2020-01-27', '2020-01-27 13:59:52', '2020-01-27 13:59:52'),
	('c1dd9595-2f92-11ea-8234-0068eb356c91', 'Customer Warung', '0899', 'customer@gmail.com', 'Jakarta', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2020-01-05', '2020-01-05 15:08:11', '2020-01-05 15:08:12'),
	('fb6edf83-2fa0-11ea-8234-0068eb356c91', 'Edi Kusmono', '9989', 'edi@gmail.com', 'Bandung', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2020-01-05', '2020-01-05 16:51:08', '2020-01-05 16:51:09');
/*!40000 ALTER TABLE `tbl_customers` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_customer_due_receives
CREATE TABLE IF NOT EXISTS `tbl_customer_due_receives` (
  `id` varchar(36) NOT NULL,
  `reference_no` varchar(50) NOT NULL,
  `only_date` date NOT NULL,
  `date` datetime NOT NULL,
  `customer_id` varchar(36) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `note` text DEFAULT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(50) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_customer_due_receives_customer_id_foreign` (`customer_id`),
  KEY `tbl_customer_due_receives_user_id_foreign` (`user_id`),
  KEY `tbl_customer_due_receives_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_customer_due_receives_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `tbl_customers` (`id`),
  CONSTRAINT `tbl_customer_due_receives_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_customer_due_receives_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_customer_due_receives: ~0 rows (approximately)
DELETE FROM `tbl_customer_due_receives`;
/*!40000 ALTER TABLE `tbl_customer_due_receives` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_customer_due_receives` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_employees
CREATE TABLE IF NOT EXISTS `tbl_employees` (
  `id` varchar(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `user_login_id` varchar(36) NOT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_employees_user_login_id_foreign` (`user_login_id`),
  KEY `tbl_employees_user_id_foreign` (`user_id`),
  KEY `tbl_employees_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_employees_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_employees_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  CONSTRAINT `tbl_employees_user_login_id_foreign` FOREIGN KEY (`user_login_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_employees: ~5 rows (approximately)
DELETE FROM `tbl_employees`;
/*!40000 ALTER TABLE `tbl_employees` DISABLE KEYS */;
INSERT INTO `tbl_employees` (`id`, `name`, `designation`, `phone`, `description`, `user_id`, `outlet_id`, `user_login_id`, `del_status`, `created_at`, `updated_at`) VALUES
	('0380211d-4061-4cff-bd80-76c47cce9785', 'Edi Kuncuro', 'Waiter', '089979', NULL, '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', '293bfaf1-f105-40ec-ad96-528b99a53b36', 'Live', '2019-12-26 08:52:00', '2019-12-26 08:52:00'),
	('6c44f9f3-9c65-4d93-8919-1ac6885c79f8', 'Kusmono', 'Admin', '089979', NULL, '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '5b668313-19f9-4385-a676-3e5e14223fb9', '0bb44bcf-11ea-48a0-bab9-4fee54a66e34', 'Live', '2019-12-26 08:53:02', '2020-01-28 08:08:33'),
	('9aa33528-218c-42ec-892f-efde3a570298', 'Susi', 'Casier', '089979', NULL, '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'a67ae7bc-7877-4f32-976a-c783df0a057c', 'Live', '2019-12-19 07:37:33', '2019-12-19 07:40:56'),
	('d0ebae8e-9458-4ab0-aa78-f2eaa4c59eb7', 'Novel Baswedan', 'Admin', '666', NULL, '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'ac6f05fe-c360-4f14-81e7-d07e21e1ab10', 'Live', '2020-01-25 04:37:58', '2020-01-27 05:56:41'),
	('f7fd82fe-f2e5-44f3-8afe-99593f8e6b87', 'Edi Kuswantoro', 'Waiter', '089979', NULL, '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'b706326f-732d-4ac7-bfe4-e1ef23579852', 'Live', '2019-12-19 07:44:46', '2019-12-19 07:44:46');
/*!40000 ALTER TABLE `tbl_employees` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_expenses
CREATE TABLE IF NOT EXISTS `tbl_expenses` (
  `id` varchar(36) NOT NULL,
  `date` date NOT NULL,
  `amount` float(10,2) NOT NULL,
  `category_id` varchar(36) NOT NULL,
  `employee_id` varchar(36) NOT NULL,
  `note` text DEFAULT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_expenses_category_id_foreign` (`category_id`),
  KEY `tbl_expenses_employee_id_foreign` (`employee_id`),
  KEY `tbl_expenses_user_id_foreign` (`user_id`),
  KEY `tbl_expenses_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_expenses_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `tbl_expense_items` (`id`),
  CONSTRAINT `tbl_expenses_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employees` (`id`),
  CONSTRAINT `tbl_expenses_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_expenses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_expenses: ~0 rows (approximately)
DELETE FROM `tbl_expenses`;
/*!40000 ALTER TABLE `tbl_expenses` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_expenses` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_expense_items
CREATE TABLE IF NOT EXISTS `tbl_expense_items` (
  `id` varchar(36) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text DEFAULT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_expense_items_user_id_foreign` (`user_id`),
  KEY `tbl_expense_items_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_expense_items_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_expense_items_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_expense_items: ~0 rows (approximately)
DELETE FROM `tbl_expense_items`;
/*!40000 ALTER TABLE `tbl_expense_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_expense_items` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_food_main_menu_category
CREATE TABLE IF NOT EXISTS `tbl_food_main_menu_category` (
  `id` varchar(36) NOT NULL,
  `ma_ca_name` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_food_main_menu_category_user_id_foreign` (`user_id`),
  KEY `tbl_food_main_menu_category_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_food_main_menu_category_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_food_main_menu_category_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_food_main_menu_category: ~5 rows (approximately)
DELETE FROM `tbl_food_main_menu_category`;
/*!40000 ALTER TABLE `tbl_food_main_menu_category` DISABLE KEYS */;
INSERT INTO `tbl_food_main_menu_category` (`id`, `ma_ca_name`, `description`, `user_id`, `outlet_id`, `del_status`, `created_at`, `updated_at`) VALUES
	('1e2231c8-0e34-4d3b-92f2-4edec7b1e05c', 'Minuman', 'Semua jenis minuman', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '5b668313-19f9-4385-a676-3e5e14223fb9', 'Live', '2020-01-27 04:45:26', '2020-01-27 04:59:24'),
	('83fbd750-10bd-4f98-bbc7-b7770487adda', 'Coffee', 'Semua jenis coffee', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2020-01-27 06:04:01', '2020-01-27 06:04:01'),
	('9541cbef-0d38-4bf9-9145-ce5bba7d337e', 'Makanan', 'Semua jenis makanan', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '5b668313-19f9-4385-a676-3e5e14223fb9', 'Live', '2020-01-27 04:45:53', '2020-01-27 04:45:53'),
	('c32efe83-5208-4ae8-afed-0e80441a0965', 'Makanan', 'Semua jenis makanan', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-19 12:12:00', '2019-12-19 12:13:28'),
	('d721af2b-6b96-4819-ad21-5e2d04559142', 'Juice', 'Semua jenis minuman juice', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-19 12:18:16', '2020-01-27 06:04:21');
/*!40000 ALTER TABLE `tbl_food_main_menu_category` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_food_menus
CREATE TABLE IF NOT EXISTS `tbl_food_menus` (
  `id` varchar(36) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `category_id` varchar(36) NOT NULL,
  `sale_price` float(10,2) DEFAULT NULL,
  `tax_information` text DEFAULT NULL,
  `vat_id` int(11) DEFAULT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `photo` varchar(250) DEFAULT NULL,
  `veg_item` varchar(50) DEFAULT 'Veg No',
  `beverage_item` varchar(50) DEFAULT 'Beverage No',
  `bar_item` varchar(50) DEFAULT 'Bar No',
  `del_status` varchar(10) NOT NULL DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_food_menus_category_id_foreign` (`category_id`),
  KEY `tbl_food_menus_user_id_foreign` (`user_id`),
  KEY `tbl_food_menus_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_food_menus_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `tbl_food_menu_categories` (`id`),
  CONSTRAINT `tbl_food_menus_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_food_menus_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_food_menus: ~5 rows (approximately)
DELETE FROM `tbl_food_menus`;
/*!40000 ALTER TABLE `tbl_food_menus` DISABLE KEYS */;
INSERT INTO `tbl_food_menus` (`id`, `code`, `name`, `description`, `category_id`, `sale_price`, `tax_information`, `vat_id`, `user_id`, `outlet_id`, `photo`, `veg_item`, `beverage_item`, `bar_item`, `del_status`, `created_at`, `updated_at`) VALUES
	('4b846c77-1b4a-4a23-921f-77e8d2faf500', 'S002', 'Swirl Cake', NULL, 'bae8ffd1-8c14-4482-8b8e-aa429565d123', 120000.00, NULL, NULL, '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Swirl_cake_1-600x400.jpg', NULL, NULL, NULL, 'Live', '2020-01-28 02:50:59', '2020-01-28 03:53:25'),
	('77c6f6bb-ddd2-4ed3-874a-1422465730e7', 'C002', 'Coffee Good Latte Lacattoze', NULL, '695a1c08-84b2-4c89-acfa-2ae9598eecab', 12000.00, NULL, NULL, '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'download.jpg', NULL, NULL, NULL, 'Live', '2019-12-20 01:25:10', '2020-01-28 03:53:34'),
	('867607b9-cc80-4283-8765-b69b0ef50884', 'B002', 'Nasi Goreng Biasa', NULL, 'bae8ffd1-8c14-4482-8b8e-aa429565d123', 25000.00, NULL, NULL, '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'nasi-goreng-teri.jpg', NULL, NULL, NULL, 'Live', '2020-01-05 05:12:08', '2020-01-28 03:53:42'),
	('8ad3341f-81da-446d-b5fd-a9970ae772cc', 'D002', 'Coffee Creame Latte Lacattoze Blue Barray', NULL, '695a1c08-84b2-4c89-acfa-2ae9598eecab', 15000.00, NULL, NULL, '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'mi-tek-tek_20171020_155621.jpg', NULL, NULL, NULL, 'Live', '2019-12-20 01:26:33', '2020-01-28 03:36:32'),
	('f0f9c291-295a-4aa2-8026-b3778c119498', 'A002', 'Nasi Goreng OK', NULL, 'bae8ffd1-8c14-4482-8b8e-aa429565d123', 25000.00, NULL, NULL, '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'nasi-goreng-teri.jpg', NULL, NULL, NULL, 'Live', '2020-01-05 04:35:05', '2020-01-28 03:53:57');
/*!40000 ALTER TABLE `tbl_food_menus` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_food_menus_ingredients
CREATE TABLE IF NOT EXISTS `tbl_food_menus_ingredients` (
  `id` varchar(36) NOT NULL,
  `ingredient_id` varchar(36) NOT NULL,
  `consumption` float(10,2) NOT NULL,
  `unit_id` varchar(36) NOT NULL,
  `food_menu_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_food_menus_ingredient_ingredient_id_foreign` (`ingredient_id`),
  KEY `tbl_food_menus_ingredient_unit_id_foreign` (`unit_id`),
  KEY `tbl_food_menus_ingredient_food_menu_id_foreign` (`food_menu_id`),
  KEY `tbl_food_menus_ingredient_user_id_foreign` (`user_id`),
  KEY `tbl_food_menus_ingredient_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_food_menus_ingredient_food_menu_id_foreign` FOREIGN KEY (`food_menu_id`) REFERENCES `tbl_food_menus` (`id`),
  CONSTRAINT `tbl_food_menus_ingredient_ingredient_id_foreign` FOREIGN KEY (`ingredient_id`) REFERENCES `tbl_ingredients` (`id`),
  CONSTRAINT `tbl_food_menus_ingredient_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_food_menus_ingredient_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `tbl_units` (`id`),
  CONSTRAINT `tbl_food_menus_ingredient_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_food_menus_ingredients: ~4 rows (approximately)
DELETE FROM `tbl_food_menus_ingredients`;
/*!40000 ALTER TABLE `tbl_food_menus_ingredients` DISABLE KEYS */;
INSERT INTO `tbl_food_menus_ingredients` (`id`, `ingredient_id`, `consumption`, `unit_id`, `food_menu_id`, `user_id`, `outlet_id`, `del_status`, `created_at`, `updated_at`) VALUES
	('20afab74-70f5-4496-b181-a12176bdc1e8', '6f966d4c-69f1-4fa8-8641-1a42ee59135d', 10.20, '10623070-1f28-45d2-a9d0-271a2bf7a403', '77c6f6bb-ddd2-4ed3-874a-1422465730e7', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-20 02:02:14', '2019-12-20 02:02:14'),
	('54b8abc8-c6ac-4d37-bb36-b370579651ba', '38741e77-50ab-4077-b83a-ba1ce4246e72', 2.00, '10623070-1f28-45d2-a9d0-271a2bf7a403', '4b846c77-1b4a-4a23-921f-77e8d2faf500', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2020-01-28 08:33:39', '2020-01-28 08:33:39'),
	('d2f378f0-5c4f-4a3b-a0e5-7c3e12fe43c9', '6f966d4c-69f1-4fa8-8641-1a42ee59135d', 1.00, '10623070-1f28-45d2-a9d0-271a2bf7a403', '4b846c77-1b4a-4a23-921f-77e8d2faf500', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2020-01-28 08:33:55', '2020-01-28 08:33:55'),
	('dcca73ce-dfc2-441f-bc0d-60aa82fb5296', '38741e77-50ab-4077-b83a-ba1ce4246e72', 10.20, '10623070-1f28-45d2-a9d0-271a2bf7a403', '77c6f6bb-ddd2-4ed3-874a-1422465730e7', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2020-01-28 04:32:20', '2020-01-28 04:32:20');
/*!40000 ALTER TABLE `tbl_food_menus_ingredients` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_food_menus_modifiers
CREATE TABLE IF NOT EXISTS `tbl_food_menus_modifiers` (
  `id` varchar(36) NOT NULL,
  `modifier_id` varchar(36) NOT NULL,
  `food_menu_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_food_menus_modifiers_food_menu_id_foreign` (`food_menu_id`),
  KEY `tbl_food_menus_modifiers_user_id_foreign` (`user_id`),
  KEY `tbl_food_menus_modifiers_outlet_id_foreign` (`outlet_id`),
  KEY `tbl_food_menus_modifiers_modifier_id_foreign` (`modifier_id`),
  CONSTRAINT `tbl_food_menus_modifiers_food_menu_id_foreign` FOREIGN KEY (`food_menu_id`) REFERENCES `tbl_food_menus` (`id`),
  CONSTRAINT `tbl_food_menus_modifiers_modifier_id_foreign` FOREIGN KEY (`modifier_id`) REFERENCES `tbl_modifiers` (`id`),
  CONSTRAINT `tbl_food_menus_modifiers_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_food_menus_modifiers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_food_menus_modifiers: ~4 rows (approximately)
DELETE FROM `tbl_food_menus_modifiers`;
/*!40000 ALTER TABLE `tbl_food_menus_modifiers` DISABLE KEYS */;
INSERT INTO `tbl_food_menus_modifiers` (`id`, `modifier_id`, `food_menu_id`, `user_id`, `outlet_id`, `del_status`, `created_at`, `updated_at`) VALUES
	('07614c3f-9c02-454e-9b1e-3e24461af8b3', 'f4585031-efe7-4cad-a0e8-c9c824afa15d', '867607b9-cc80-4283-8765-b69b0ef50884', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2020-01-28 10:43:52', '2020-01-28 10:43:52'),
	('7856a266-ce7f-4779-888b-61f25a71a73c', '467416a2-9b60-4009-adfb-122700c76593', '867607b9-cc80-4283-8765-b69b0ef50884', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2020-01-28 10:43:43', '2020-01-28 10:43:43'),
	('a8d12876-889d-4e8d-b16d-36fdd2fd3b45', 'f4585031-efe7-4cad-a0e8-c9c824afa15d', '77c6f6bb-ddd2-4ed3-874a-1422465730e7', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-21 08:54:13', '2019-12-21 09:28:09'),
	('c266da30-a897-49cd-b27f-43108907295d', 'bf90721e-68db-4acb-8344-72cf2fa74fd3', '77c6f6bb-ddd2-4ed3-874a-1422465730e7', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2020-01-28 10:35:18', '2020-01-28 10:35:18');
/*!40000 ALTER TABLE `tbl_food_menus_modifiers` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_food_menus_promo
CREATE TABLE IF NOT EXISTS `tbl_food_menus_promo` (
  `id` varchar(36) NOT NULL,
  `food_menu_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `promo_name` varchar(150) NOT NULL,
  `promo_type` int(1) NOT NULL DEFAULT 0,
  `promo_value` double(10,2) NOT NULL DEFAULT 0.00,
  `date_start` date NOT NULL,
  `date_done` date NOT NULL,
  `description` text DEFAULT NULL,
  `del_status` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_food_menus_promo_food_menu_id_foreign` (`food_menu_id`),
  KEY `tbl_food_menus_promo_user_id_foreign` (`user_id`),
  KEY `tbl_food_menus_promo_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_food_menus_promo_food_menu_id_foreign` FOREIGN KEY (`food_menu_id`) REFERENCES `tbl_food_menus` (`id`),
  CONSTRAINT `tbl_food_menus_promo_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_food_menus_promo_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_food_menus_promo: ~0 rows (approximately)
DELETE FROM `tbl_food_menus_promo`;
/*!40000 ALTER TABLE `tbl_food_menus_promo` DISABLE KEYS */;
INSERT INTO `tbl_food_menus_promo` (`id`, `food_menu_id`, `user_id`, `outlet_id`, `promo_name`, `promo_type`, `promo_value`, `date_start`, `date_done`, `description`, `del_status`, `created_at`, `updated_at`) VALUES
	('ef0a94eb-25f9-4e98-ab9b-db598d4782a1', '77c6f6bb-ddd2-4ed3-874a-1422465730e7', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Ultah', 2, 20000.00, '2020-01-28', '2020-01-31', 'Promo hari ultah', 'Live', '2020-01-28 14:07:25', '2020-01-28 14:22:41');
/*!40000 ALTER TABLE `tbl_food_menus_promo` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_food_menu_categories
CREATE TABLE IF NOT EXISTS `tbl_food_menu_categories` (
  `id` varchar(36) NOT NULL,
  `category_name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `ma_ca_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(50) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_food_menu_categories_ma_ca_id_foreign` (`ma_ca_id`),
  KEY `tbl_food_menu_categories_user_id_foreign` (`user_id`),
  KEY `tbl_food_menu_categories_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_food_menu_categories_ma_ca_id_foreign` FOREIGN KEY (`ma_ca_id`) REFERENCES `tbl_food_main_menu_category` (`id`),
  CONSTRAINT `tbl_food_menu_categories_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_food_menu_categories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_food_menu_categories: ~6 rows (approximately)
DELETE FROM `tbl_food_menu_categories`;
/*!40000 ALTER TABLE `tbl_food_menu_categories` DISABLE KEYS */;
INSERT INTO `tbl_food_menu_categories` (`id`, `category_name`, `description`, `ma_ca_id`, `user_id`, `outlet_id`, `del_status`, `created_at`, `updated_at`) VALUES
	('37428eb7-3bcf-4e42-a17d-0af5e8f4a550', 'Coffee Ku Nikmat', 'tes', '83fbd750-10bd-4f98-bbc7-b7770487adda', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2020-01-27 06:24:34', '2020-01-27 06:24:34'),
	('66171462-2234-4796-83e4-9e8e5eaf88fc', 'Coffee Creame', NULL, '83fbd750-10bd-4f98-bbc7-b7770487adda', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2020-01-27 06:31:42', '2020-01-27 06:41:18'),
	('695a1c08-84b2-4c89-acfa-2ae9598eecab', 'Coffee Creame Latte', NULL, '83fbd750-10bd-4f98-bbc7-b7770487adda', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-20 01:07:23', '2020-01-27 06:33:57'),
	('91f17fc7-5e6d-44c5-ae29-3d4f5726c3f4', 'Coffee Latte', NULL, '83fbd750-10bd-4f98-bbc7-b7770487adda', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2020-01-27 06:32:36', '2020-01-27 06:41:27'),
	('bae8ffd1-8c14-4482-8b8e-aa429565d123', 'Nasi Goreng Creeze', 'Semua Nasi Goreng', 'c32efe83-5208-4ae8-afed-0e80441a0965', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2020-01-02 07:55:02', '2020-01-02 07:55:02'),
	('f8d6b1f5-9084-4ef6-8b36-220ac8c9855e', 'Coffee Single Origin', 'Coffe', '83fbd750-10bd-4f98-bbc7-b7770487adda', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-20 01:04:17', '2020-01-27 06:34:07');
/*!40000 ALTER TABLE `tbl_food_menu_categories` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_holds
CREATE TABLE IF NOT EXISTS `tbl_holds` (
  `id` varchar(36) NOT NULL,
  `customer_id` varchar(36) NOT NULL,
  `hold_no` varchar(30) NOT NULL DEFAULT '000000',
  `total_items` int(10) NOT NULL,
  `sub_total` float(10,2) NOT NULL,
  `paid_amount` double(10,2) NOT NULL,
  `due_amount` double(10,2) NOT NULL,
  `due_payment_date` date NOT NULL,
  `disc` varchar(50) NOT NULL,
  `disc_actual` float(10,2) NOT NULL,
  `vat` float(10,2) NOT NULL,
  `total_payable` float(10,2) NOT NULL,
  `payment_method_id` varchar(36) NOT NULL,
  `table_id` varchar(36) NOT NULL,
  `total_item_discount_amount` float(10,2) NOT NULL,
  `sub_total_with_discount` float(10,2) NOT NULL,
  `sub_total_discount_amount` float(10,2) NOT NULL,
  `total_discount_amount` float(10,2) NOT NULL,
  `delivery_charge` float(10,2) NOT NULL,
  `sub_total_discount_value` varchar(10) NOT NULL,
  `sub_total_discount_type` varchar(20) NOT NULL,
  `token_no` varchar(50) DEFAULT NULL,
  `sale_date` varchar(20) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `sale_time` varchar(15) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `waiter_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `order_status` tinyint(1) NOT NULL COMMENT '1=new order, 2=cancelled order, 3=invoiced order',
  `sale_vat_objects` text DEFAULT NULL,
  `order_type` tinyint(1) NOT NULL COMMENT '1=dine in, 2 = take away, 3 = delivery',
  `del_status` varchar(10) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_holds_customer_id_foreign` (`customer_id`),
  KEY `tbl_holds_table_id_foreign` (`table_id`),
  KEY `tbl_holds_user_id_foreign` (`user_id`),
  KEY `tbl_holds_waiter_id_foreign` (`waiter_id`),
  KEY `tbl_holds_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_holds_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `tbl_customers` (`id`),
  CONSTRAINT `tbl_holds_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_holds_table_id_foreign` FOREIGN KEY (`table_id`) REFERENCES `tbl_tables` (`id`),
  CONSTRAINT `tbl_holds_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  CONSTRAINT `tbl_holds_waiter_id_foreign` FOREIGN KEY (`waiter_id`) REFERENCES `tbl_employees` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_holds: ~0 rows (approximately)
DELETE FROM `tbl_holds`;
/*!40000 ALTER TABLE `tbl_holds` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_holds` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_holds_details
CREATE TABLE IF NOT EXISTS `tbl_holds_details` (
  `id` varchar(36) NOT NULL,
  `food_menu_id` varchar(36) NOT NULL,
  `menu_name` varchar(50) NOT NULL,
  `qty` int(10) NOT NULL,
  `menu_price_without_discount` float(10,2) NOT NULL,
  `menu_price_with_discount` float(10,2) NOT NULL,
  `menu_unit_price` float(10,2) NOT NULL,
  `menu_vat_percentage` float(10,2) NOT NULL,
  `menu_taxes` text DEFAULT NULL,
  `menu_discount_value` varchar(20) DEFAULT NULL,
  `discount_type` varchar(20) NOT NULL,
  `menu_note` varchar(150) DEFAULT NULL,
  `discount_amount` double(10,2) DEFAULT NULL,
  `holds_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(50) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_holds_details_food_menu_id_foreign` (`food_menu_id`),
  KEY `tbl_holds_details_holds_id_foreign` (`holds_id`),
  KEY `tbl_holds_details_user_id_foreign` (`user_id`),
  KEY `tbl_holds_details_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_holds_details_food_menu_id_foreign` FOREIGN KEY (`food_menu_id`) REFERENCES `tbl_food_menus` (`id`),
  CONSTRAINT `tbl_holds_details_holds_id_foreign` FOREIGN KEY (`holds_id`) REFERENCES `tbl_holds` (`id`),
  CONSTRAINT `tbl_holds_details_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_holds_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_holds_details: ~0 rows (approximately)
DELETE FROM `tbl_holds_details`;
/*!40000 ALTER TABLE `tbl_holds_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_holds_details` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_holds_details_modifiers
CREATE TABLE IF NOT EXISTS `tbl_holds_details_modifiers` (
  `id` varchar(36) NOT NULL,
  `modifier_id` varchar(36) NOT NULL,
  `modifier_price` float(10,2) NOT NULL,
  `food_menu_id` varchar(36) NOT NULL,
  `holds_id` varchar(36) NOT NULL,
  `holds_details_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `customer_id` varchar(36) NOT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_holds_details_modifiers_modifier_id_foreign` (`modifier_id`),
  KEY `tbl_holds_details_modifiers_food_menu_id_foreign` (`food_menu_id`),
  KEY `tbl_holds_details_modifiers_holds_id_foreign` (`holds_id`),
  KEY `tbl_holds_details_modifiers_holds_details_id_foreign` (`holds_details_id`),
  KEY `tbl_holds_details_modifiers_user_id_foreign` (`user_id`),
  KEY `tbl_holds_details_modifiers_outlet_id_foreign` (`outlet_id`),
  KEY `tbl_holds_details_modifiers_customer_id_foreign` (`customer_id`),
  CONSTRAINT `tbl_holds_details_modifiers_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `tbl_customers` (`id`),
  CONSTRAINT `tbl_holds_details_modifiers_food_menu_id_foreign` FOREIGN KEY (`food_menu_id`) REFERENCES `tbl_food_menus` (`id`),
  CONSTRAINT `tbl_holds_details_modifiers_holds_details_id_foreign` FOREIGN KEY (`holds_details_id`) REFERENCES `tbl_holds_details` (`id`),
  CONSTRAINT `tbl_holds_details_modifiers_holds_id_foreign` FOREIGN KEY (`holds_id`) REFERENCES `tbl_holds` (`id`),
  CONSTRAINT `tbl_holds_details_modifiers_modifier_id_foreign` FOREIGN KEY (`modifier_id`) REFERENCES `tbl_modifiers` (`id`),
  CONSTRAINT `tbl_holds_details_modifiers_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_holds_details_modifiers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_holds_details_modifiers: ~0 rows (approximately)
DELETE FROM `tbl_holds_details_modifiers`;
/*!40000 ALTER TABLE `tbl_holds_details_modifiers` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_holds_details_modifiers` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_ingredients
CREATE TABLE IF NOT EXISTS `tbl_ingredients` (
  `id` varchar(36) NOT NULL,
  `code` varchar(50) NOT NULL DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `category_id` varchar(36) NOT NULL,
  `purchase_price` float(10,2) DEFAULT NULL,
  `alert_quantity` float(10,2) DEFAULT NULL,
  `unit_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_ingredients_unit_id_foreign` (`unit_id`),
  KEY `tbl_ingredients_user_id_foreign` (`user_id`),
  KEY `tbl_ingredients_category_id_foreign` (`category_id`),
  CONSTRAINT `tbl_ingredients_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `tbl_ingredient_categories` (`id`),
  CONSTRAINT `tbl_ingredients_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `tbl_units` (`id`),
  CONSTRAINT `tbl_ingredients_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_ingredients: ~3 rows (approximately)
DELETE FROM `tbl_ingredients`;
/*!40000 ALTER TABLE `tbl_ingredients` DISABLE KEYS */;
INSERT INTO `tbl_ingredients` (`id`, `code`, `name`, `category_id`, `purchase_price`, `alert_quantity`, `unit_id`, `user_id`, `del_status`, `created_at`, `updated_at`) VALUES
	('38741e77-50ab-4077-b83a-ba1ce4246e72', '001', 'Bawang Merah', '54d47671-20bd-4852-a909-6b344d6b95d4', 20000.00, 10.00, '10623070-1f28-45d2-a9d0-271a2bf7a403', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', 'Live', '2019-12-26 18:13:09', '2019-12-26 18:13:09'),
	('6f966d4c-69f1-4fa8-8641-1a42ee59135d', '003', 'Bawang Putih', '54d47671-20bd-4852-a909-6b344d6b95d4', 20000.00, 10.00, 'a9f2f258-d40f-4d2b-9654-bb1bc1c4a0ac', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', 'Live', '2019-12-19 10:47:06', '2020-01-27 01:55:01'),
	('d44d17c0-db1b-4258-9963-1edc9085756d', '002', 'Wortel', '84aed134-e223-43fe-9906-de243887ba65', 30000.00, 10.00, '401855d0-6a2a-4893-9789-df3636667f8f', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', 'Live', '2020-01-27 01:39:08', '2020-01-27 01:39:08');
/*!40000 ALTER TABLE `tbl_ingredients` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_ingredient_categories
CREATE TABLE IF NOT EXISTS `tbl_ingredient_categories` (
  `id` varchar(36) NOT NULL,
  `category_name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `user_id` varchar(36) NOT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_ingredient_categories_user_id_foreign` (`user_id`),
  CONSTRAINT `tbl_ingredient_categories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_ingredient_categories: ~5 rows (approximately)
DELETE FROM `tbl_ingredient_categories`;
/*!40000 ALTER TABLE `tbl_ingredient_categories` DISABLE KEYS */;
INSERT INTO `tbl_ingredient_categories` (`id`, `category_name`, `description`, `user_id`, `del_status`, `created_at`, `updated_at`) VALUES
	('54d47671-20bd-4852-a909-6b344d6b95d4', 'Bawang', 'Master semua bawang merah', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', 'Live', '2019-12-19 08:09:33', '2019-12-19 08:11:06'),
	('84aed134-e223-43fe-9906-de243887ba65', 'Sayur', 'Semua sayur disini', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', 'Live', '2019-12-19 08:08:47', '2019-12-19 08:08:47'),
	('a0a798ee-03f6-4810-bade-fb13f0bd0edb', 'Egg Dishes', 'Semua bawang', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', 'Live', '2020-01-26 02:18:46', '2020-01-26 02:18:46'),
	('b535b837-076a-482d-9eb8-90f7d0df4e89', 'Buah-Buahan', 'Semua buah-buahan disini', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', 'Live', '2020-01-26 02:07:40', '2020-01-26 02:07:40'),
	('e27afa9e-c46c-4cfe-ae9b-1cb97d549cda', 'Apple Dishes', 'All category ingredient for apple dishes', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', 'Live', '2019-12-26 12:28:28', '2020-01-26 02:23:12');
/*!40000 ALTER TABLE `tbl_ingredient_categories` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_inventory_adjustment
CREATE TABLE IF NOT EXISTS `tbl_inventory_adjustment` (
  `id` varchar(36) NOT NULL,
  `reference_no` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `note` text DEFAULT NULL,
  `employee_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_inventory_adjustment_employee_id_foreign` (`employee_id`),
  KEY `tbl_inventory_adjustment_user_id_foreign` (`user_id`),
  KEY `tbl_inventory_adjustment_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_inventory_adjustment_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employees` (`id`),
  CONSTRAINT `tbl_inventory_adjustment_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_inventory_adjustment_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_inventory_adjustment: ~0 rows (approximately)
DELETE FROM `tbl_inventory_adjustment`;
/*!40000 ALTER TABLE `tbl_inventory_adjustment` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_inventory_adjustment` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_inventory_adjustment_ingredients
CREATE TABLE IF NOT EXISTS `tbl_inventory_adjustment_ingredients` (
  `id` varchar(36) NOT NULL,
  `ingredient_id` varchar(36) NOT NULL,
  `consumption_amount` float(10,2) NOT NULL,
  `inventory_adjustment_id` varchar(36) NOT NULL,
  `consumption_status` enum('Plus','Minus','','') DEFAULT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_inventory_adjustment_ingredients_ingredient_id_foreign` (`ingredient_id`),
  KEY `tbl_inventory_adjustment_ingredients_adjustment_id_foreign` (`inventory_adjustment_id`),
  KEY `tbl_inventory_adjustment_ingredients_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_inventory_adjustment_ingredients_adjustment_id_foreign` FOREIGN KEY (`inventory_adjustment_id`) REFERENCES `tbl_inventory_adjustment` (`id`),
  CONSTRAINT `tbl_inventory_adjustment_ingredients_ingredient_id_foreign` FOREIGN KEY (`ingredient_id`) REFERENCES `tbl_ingredients` (`id`),
  CONSTRAINT `tbl_inventory_adjustment_ingredients_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_inventory_adjustment_ingredients: ~0 rows (approximately)
DELETE FROM `tbl_inventory_adjustment_ingredients`;
/*!40000 ALTER TABLE `tbl_inventory_adjustment_ingredients` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_inventory_adjustment_ingredients` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_modifiers
CREATE TABLE IF NOT EXISTS `tbl_modifiers` (
  `id` varchar(36) NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` float(10,2) NOT NULL,
  `description` text DEFAULT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_modifiers_user_id_foreign` (`user_id`),
  KEY `tbl_modifiers_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_modifiers_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_modifiers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_modifiers: ~3 rows (approximately)
DELETE FROM `tbl_modifiers`;
/*!40000 ALTER TABLE `tbl_modifiers` DISABLE KEYS */;
INSERT INTO `tbl_modifiers` (`id`, `name`, `price`, `description`, `user_id`, `outlet_id`, `del_status`, `created_at`, `updated_at`) VALUES
	('467416a2-9b60-4009-adfb-122700c76593', 'Nasi Goreng Baso Gila', 2000.00, NULL, '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-20 06:35:09', '2019-12-20 06:47:28'),
	('bf90721e-68db-4acb-8344-72cf2fa74fd3', 'Nasi Sepur', 3000.00, NULL, '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2020-01-27 08:26:52', '2020-01-27 08:36:01'),
	('f4585031-efe7-4cad-a0e8-c9c824afa15d', 'Nasi Goreng Baso Biasa', 0.00, NULL, '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-20 06:34:14', '2019-12-20 06:34:14');
/*!40000 ALTER TABLE `tbl_modifiers` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_modifier_ingredients
CREATE TABLE IF NOT EXISTS `tbl_modifier_ingredients` (
  `id` varchar(36) NOT NULL,
  `ingredient_id` varchar(36) NOT NULL,
  `consumption` float(10,2) NOT NULL,
  `modifier_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `unit_id` varchar(36) NOT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_modifier_ingredients_ingredient_id_foreign` (`ingredient_id`),
  KEY `tbl_modifier_ingredients_modifier_id_foreign` (`modifier_id`),
  KEY `tbl_modifier_ingredients_user_id_foreign` (`user_id`),
  KEY `tbl_modifier_ingredients_outlet_id_foreign` (`outlet_id`),
  KEY `tbl_modifier_ingredients_unit_id_foreign` (`unit_id`),
  CONSTRAINT `tbl_modifier_ingredients_ingredient_id_foreign` FOREIGN KEY (`ingredient_id`) REFERENCES `tbl_ingredients` (`id`),
  CONSTRAINT `tbl_modifier_ingredients_modifier_id_foreign` FOREIGN KEY (`modifier_id`) REFERENCES `tbl_modifiers` (`id`),
  CONSTRAINT `tbl_modifier_ingredients_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_modifier_ingredients_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `tbl_units` (`id`),
  CONSTRAINT `tbl_modifier_ingredients_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_modifier_ingredients: ~3 rows (approximately)
DELETE FROM `tbl_modifier_ingredients`;
/*!40000 ALTER TABLE `tbl_modifier_ingredients` DISABLE KEYS */;
INSERT INTO `tbl_modifier_ingredients` (`id`, `ingredient_id`, `consumption`, `modifier_id`, `user_id`, `outlet_id`, `unit_id`, `del_status`, `created_at`, `updated_at`) VALUES
	('7b6e4414-cf4d-4a40-8a1b-25565b32613e', '38741e77-50ab-4077-b83a-ba1ce4246e72', 10.00, 'bf90721e-68db-4acb-8344-72cf2fa74fd3', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', '10623070-1f28-45d2-a9d0-271a2bf7a403', 'Live', '2020-01-27 13:22:05', '2020-01-27 13:22:05'),
	('db78ea15-c72b-44b7-840c-aee65ea1d536', 'd44d17c0-db1b-4258-9963-1edc9085756d', 10.00, '467416a2-9b60-4009-adfb-122700c76593', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', '10623070-1f28-45d2-a9d0-271a2bf7a403', 'Live', '2020-01-27 13:21:34', '2020-01-27 13:21:34'),
	('f785d083-4376-4dc1-a65f-192779c7c85e', '38741e77-50ab-4077-b83a-ba1ce4246e72', 3.00, '467416a2-9b60-4009-adfb-122700c76593', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', '10623070-1f28-45d2-a9d0-271a2bf7a403', 'Live', '2020-01-27 13:46:15', '2020-01-27 13:46:15');
/*!40000 ALTER TABLE `tbl_modifier_ingredients` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_notifications
CREATE TABLE IF NOT EXISTS `tbl_notifications` (
  `id` varchar(36) NOT NULL,
  `notification` text NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `tbl_notifications_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_notifications_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_notifications: ~0 rows (approximately)
DELETE FROM `tbl_notifications`;
/*!40000 ALTER TABLE `tbl_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_notifications` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_notification_bar_kitchen_panel
CREATE TABLE IF NOT EXISTS `tbl_notification_bar_kitchen_panel` (
  `id` varchar(36) NOT NULL,
  `notification` text NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `tbl_notification_bar_kitchen_panel_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_notification_bar_kitchen_panel_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_notification_bar_kitchen_panel: ~0 rows (approximately)
DELETE FROM `tbl_notification_bar_kitchen_panel`;
/*!40000 ALTER TABLE `tbl_notification_bar_kitchen_panel` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_notification_bar_kitchen_panel` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_orders_table
CREATE TABLE IF NOT EXISTS `tbl_orders_table` (
  `id` varchar(36) NOT NULL,
  `persons` int(5) NOT NULL,
  `booking_time` datetime NOT NULL,
  `sale_id` varchar(36) NOT NULL,
  `sale_no` varchar(20) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `table_id` varchar(36) NOT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  PRIMARY KEY (`id`),
  KEY `tbl_orders_table_sale_id_foreign` (`sale_id`),
  KEY `tbl_orders_table_outlet_id_foreign` (`outlet_id`),
  KEY `tbl_orders_table_table_id_foreign` (`table_id`),
  CONSTRAINT `tbl_orders_table_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_orders_table_sale_id_foreign` FOREIGN KEY (`sale_id`) REFERENCES `tbl_sales` (`id`),
  CONSTRAINT `tbl_orders_table_table_id_foreign` FOREIGN KEY (`table_id`) REFERENCES `tbl_tables` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_orders_table: ~0 rows (approximately)
DELETE FROM `tbl_orders_table`;
/*!40000 ALTER TABLE `tbl_orders_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_orders_table` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_outlets
CREATE TABLE IF NOT EXISTS `tbl_outlets` (
  `id` varchar(36) NOT NULL,
  `outlet_name` varchar(50) NOT NULL,
  `outlet_code` varchar(10) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `invoice_print` enum('Yes','No') NOT NULL,
  `starting_date` date NOT NULL,
  `invoice_footer` varchar(500) NOT NULL,
  `collect_tax` varchar(10) DEFAULT NULL,
  `tax_title` varchar(10) DEFAULT NULL,
  `tax_registration_no` varchar(30) DEFAULT NULL,
  `tax_is_gst` varchar(10) DEFAULT NULL,
  `state_code` varchar(10) DEFAULT NULL,
  `pre_or_post_payment` varchar(150) DEFAULT 'Post Payment',
  `currency` varchar(50) NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `date_format` varchar(50) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_outlets_user_id_foreign` (`user_id`),
  CONSTRAINT `tbl_outlets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_outlets: ~4 rows (approximately)
DELETE FROM `tbl_outlets`;
/*!40000 ALTER TABLE `tbl_outlets` DISABLE KEYS */;
INSERT INTO `tbl_outlets` (`id`, `outlet_name`, `outlet_code`, `address`, `phone`, `invoice_print`, `starting_date`, `invoice_footer`, `collect_tax`, `tax_title`, `tax_registration_no`, `tax_is_gst`, `state_code`, `pre_or_post_payment`, `currency`, `timezone`, `date_format`, `user_id`, `del_status`, `created_at`, `updated_at`) VALUES
	('5b668313-19f9-4385-a676-3e5e14223fb9', 'Warung Gue 1', '09', 'Jakarta', '085227281672', 'Yes', '2020-01-24', 'Termakasih', '', '', '', '', '', 'Post Payment', 'Rupiah', 'Asia/Jakarta', 'Y-m-d', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', 'Live', '2020-01-24 01:53:59', '2020-01-25 04:40:16'),
	('5f019932-1db4-4418-80b6-a29f23a88ca7', 'Cafe Co-Working 1', '04', 'Bandung', '0741 699', 'Yes', '2015-01-01', 'Termikasih', 'No', NULL, NULL, NULL, NULL, 'Post Payment', 'Rupiah', 'Asia/Jakarta', 'd/M/y', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', 'Live', '2019-12-26 08:52:35', '2020-01-24 06:30:09'),
	('7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Cafe Co-Working', '04', 'Bandung', '0741 699', 'Yes', '2015-01-01', 'Termikasih', 'No', NULL, NULL, NULL, NULL, 'Post Payment', 'Rupiah', 'Asia/Jakarta', 'd/M/y', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', 'Live', '2019-12-19 07:14:41', '2019-12-19 07:15:35'),
	('da2a5e79-7317-4c12-be03-c06bdfcff0d9', 'Warung Maknyak', '34', 'Jambi', '0988', 'Yes', '2020-01-24', 'Termikasih', '', '', '', '', '', 'Post Payment', 'Rupiah', 'Asia/Jakarta', 'Y-m-d', '8d2e9341-a759-49c9-bf71-398db50bb742', 'Live', '2020-01-24 06:40:44', '2020-01-24 06:40:44');
/*!40000 ALTER TABLE `tbl_outlets` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_outlet_taxes
CREATE TABLE IF NOT EXISTS `tbl_outlet_taxes` (
  `id` varchar(36) NOT NULL,
  `tax` varchar(50) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_outlet_taxes_outlet_id_foreign` (`outlet_id`),
  KEY `tbl_outlet_taxes_user_id_foreign` (`user_id`),
  CONSTRAINT `tbl_outlet_taxes_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_outlet_taxes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_outlet_taxes: ~0 rows (approximately)
DELETE FROM `tbl_outlet_taxes`;
/*!40000 ALTER TABLE `tbl_outlet_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_outlet_taxes` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_payment_category
CREATE TABLE IF NOT EXISTS `tbl_payment_category` (
  `id` varchar(36) NOT NULL,
  `category_name` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_payment_category: ~0 rows (approximately)
DELETE FROM `tbl_payment_category`;
/*!40000 ALTER TABLE `tbl_payment_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_payment_category` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_payment_methods
CREATE TABLE IF NOT EXISTS `tbl_payment_methods` (
  `id` varchar(36) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `category_id` varchar(36) NOT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_payment_methods_user_id_foreign` (`user_id`),
  KEY `tbl_payment_methods_outlet_id_foreign` (`outlet_id`),
  KEY `tbl_payment_methods_category_id_foreign` (`category_id`),
  CONSTRAINT `tbl_payment_methods_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `tbl_payment_category` (`id`),
  CONSTRAINT `tbl_payment_methods_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_payment_methods_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_payment_methods: ~0 rows (approximately)
DELETE FROM `tbl_payment_methods`;
/*!40000 ALTER TABLE `tbl_payment_methods` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_payment_methods` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_purchase
CREATE TABLE IF NOT EXISTS `tbl_purchase` (
  `id` varchar(36) NOT NULL,
  `reference_no` varchar(50) NOT NULL,
  `supplier_id` varchar(36) NOT NULL,
  `date` date NOT NULL,
  `subtotal` float(10,2) NOT NULL,
  `other` float(10,2) DEFAULT NULL,
  `grand_total` float(10,2) NOT NULL,
  `paid` float(10,2) NOT NULL,
  `due` float(10,2) NOT NULL,
  `note` text DEFAULT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(50) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_purchase_supplier_id_foreign` (`supplier_id`),
  KEY `tbl_purchase_user_id_foreign` (`user_id`),
  KEY `tbl_purchase_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_purchase_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_purchase_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `tbl_suppliers` (`id`),
  CONSTRAINT `tbl_purchase_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_purchase: ~0 rows (approximately)
DELETE FROM `tbl_purchase`;
/*!40000 ALTER TABLE `tbl_purchase` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_purchase` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_purchase_ingredients
CREATE TABLE IF NOT EXISTS `tbl_purchase_ingredients` (
  `id` varchar(36) NOT NULL,
  `ingredient_id` varchar(36) NOT NULL,
  `unit_price` float(10,2) NOT NULL,
  `quantity_amount` float(10,2) NOT NULL,
  `total` float(10,2) NOT NULL,
  `purchase_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_purchase_ingredient_purchase_id_foreign` (`purchase_id`),
  KEY `tbl_purchase_ingredient_ingredient_id_foreign` (`ingredient_id`),
  KEY `tbl_purchase_ingredient_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_purchase_ingredient_ingredient_id_foreign` FOREIGN KEY (`ingredient_id`) REFERENCES `tbl_ingredients` (`id`),
  CONSTRAINT `tbl_purchase_ingredient_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_purchase_ingredient_purchase_id_foreign` FOREIGN KEY (`purchase_id`) REFERENCES `tbl_purchase` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_purchase_ingredients: ~0 rows (approximately)
DELETE FROM `tbl_purchase_ingredients`;
/*!40000 ALTER TABLE `tbl_purchase_ingredients` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_purchase_ingredients` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_sales
CREATE TABLE IF NOT EXISTS `tbl_sales` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `customer_id` varchar(36) NOT NULL,
  `sale_no` varchar(30) NOT NULL DEFAULT '000000',
  `total_items` int(10) DEFAULT NULL,
  `sub_total` float(10,2) DEFAULT NULL,
  `paid_amount` double(10,2) DEFAULT NULL,
  `due_amount` float(10,2) DEFAULT NULL,
  `given_amount` float(10,2) DEFAULT NULL,
  `change_amount` float(10,2) DEFAULT NULL,
  `disc` varchar(50) DEFAULT NULL,
  `disc_actual` float(10,2) DEFAULT NULL,
  `vat` float(10,2) DEFAULT NULL,
  `total_payable` float(10,2) DEFAULT NULL,
  `payment_method_id` int(10) DEFAULT NULL,
  `close_time` time NOT NULL,
  `table_id` varchar(36) NOT NULL,
  `total_item_discount_amount` float(10,2) NOT NULL,
  `sub_total_with_discount` float(10,2) NOT NULL,
  `sub_total_discount_amount` float(10,2) NOT NULL,
  `total_discount_amount` float(10,2) NOT NULL,
  `delivery_charge` float(10,2) NOT NULL,
  `sub_total_discount_value` varchar(10) NOT NULL,
  `sub_total_discount_type` varchar(20) NOT NULL,
  `sale_date` varchar(20) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `order_time` time NOT NULL,
  `cooking_start_time` datetime DEFAULT NULL,
  `cooking_done_time` datetime DEFAULT NULL,
  `modified` enum('Yes','No') NOT NULL DEFAULT 'No',
  `user_id` varchar(36) NOT NULL,
  `waiter_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `order_status` tinyint(1) NOT NULL COMMENT '1=new order, 2=invoiced order, 3=closed order',
  `order_type` tinyint(1) NOT NULL COMMENT '1=dine in, 2 = take away, 3 = delivery',
  `del_status` varchar(50) DEFAULT 'Live',
  `sale_vat_objects` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_sales_table_id_foreign` (`table_id`),
  KEY `tbl_sales_user_id_foreign` (`user_id`),
  KEY `tbl_sales_waiter_id_foreign` (`waiter_id`),
  KEY `tbl_sales_outlet_id_foreign` (`outlet_id`),
  KEY `tbl_sales_customer_id_foreign` (`customer_id`),
  CONSTRAINT `tbl_sales_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `tbl_customers` (`id`),
  CONSTRAINT `tbl_sales_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_sales_table_id_foreign` FOREIGN KEY (`table_id`) REFERENCES `tbl_tables` (`id`),
  CONSTRAINT `tbl_sales_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`),
  CONSTRAINT `tbl_sales_waiter_id_foreign` FOREIGN KEY (`waiter_id`) REFERENCES `tbl_employees` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_sales: ~0 rows (approximately)
DELETE FROM `tbl_sales`;
/*!40000 ALTER TABLE `tbl_sales` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_sales` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_sales_details
CREATE TABLE IF NOT EXISTS `tbl_sales_details` (
  `id` varchar(36) NOT NULL,
  `food_menu_id` varchar(36) NOT NULL,
  `menu_name` varchar(50) DEFAULT NULL,
  `qty` int(10) NOT NULL,
  `menu_price_without_discount` float(10,2) NOT NULL,
  `menu_price_with_discount` float(10,2) NOT NULL,
  `menu_unit_price` float(10,2) NOT NULL,
  `menu_vat_percentage` float(10,2) NOT NULL,
  `menu_taxes` text DEFAULT NULL,
  `menu_discount_value` varchar(20) DEFAULT NULL,
  `discount_type` varchar(20) NOT NULL,
  `menu_note` varchar(150) DEFAULT NULL,
  `discount_amount` double(10,2) DEFAULT NULL,
  `item_type` varchar(50) DEFAULT NULL,
  `cooking_status` varchar(30) DEFAULT NULL,
  `cooking_start_time` datetime NOT NULL,
  `cooking_done_time` datetime NOT NULL,
  `previous_id` varchar(36) NOT NULL,
  `sales_id` varchar(36) NOT NULL,
  `order_status` tinyint(1) NOT NULL COMMENT '1=new order,2=invoiced order, 3=closed order',
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(50) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_sales_details_food_menu_id_foreign` (`food_menu_id`),
  KEY `tbl_sales_details_sales_id_foreign` (`sales_id`),
  KEY `tbl_sales_details_user_id_foreign` (`user_id`),
  KEY `tbl_sales_details_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_sales_details_food_menu_id_foreign` FOREIGN KEY (`food_menu_id`) REFERENCES `tbl_food_menus` (`id`),
  CONSTRAINT `tbl_sales_details_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_sales_details_sales_id_foreign` FOREIGN KEY (`sales_id`) REFERENCES `tbl_sales` (`id`),
  CONSTRAINT `tbl_sales_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_sales_details: ~0 rows (approximately)
DELETE FROM `tbl_sales_details`;
/*!40000 ALTER TABLE `tbl_sales_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_sales_details` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_sales_details_modifiers
CREATE TABLE IF NOT EXISTS `tbl_sales_details_modifiers` (
  `id` varchar(36) NOT NULL,
  `modifier_id` varchar(36) NOT NULL,
  `modifier_price` float(10,2) NOT NULL,
  `food_menu_id` varchar(36) NOT NULL,
  `sales_id` varchar(36) NOT NULL,
  `order_status` tinyint(1) NOT NULL COMMENT '1=new order,2=invoiced order, 3=closed order',
  `sales_details_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `customer_id` varchar(36) NOT NULL,
  `crated_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_sales_details_modifiers_modifier_id_foreign` (`modifier_id`),
  KEY `tbl_sales_details_modifiers_sales_id_foreign` (`sales_id`),
  KEY `tbl_sales_details_modifiers_sales_details_id_foreign` (`sales_details_id`),
  KEY `tbl_sales_details_modifiers_user_id_foreign` (`user_id`),
  KEY `tbl_sales_details_modifiers_outlet_id_foreign` (`outlet_id`),
  KEY `tbl_sales_details_modifiers_customer_id_foreign` (`customer_id`),
  CONSTRAINT `tbl_sales_details_modifiers_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `tbl_customers` (`id`),
  CONSTRAINT `tbl_sales_details_modifiers_modifier_id_foreign` FOREIGN KEY (`modifier_id`) REFERENCES `tbl_modifiers` (`id`),
  CONSTRAINT `tbl_sales_details_modifiers_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_sales_details_modifiers_sales_details_id_foreign` FOREIGN KEY (`sales_details_id`) REFERENCES `tbl_sales_details` (`id`),
  CONSTRAINT `tbl_sales_details_modifiers_sales_id_foreign` FOREIGN KEY (`sales_id`) REFERENCES `tbl_sales` (`id`),
  CONSTRAINT `tbl_sales_details_modifiers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_sales_details_modifiers: ~0 rows (approximately)
DELETE FROM `tbl_sales_details_modifiers`;
/*!40000 ALTER TABLE `tbl_sales_details_modifiers` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_sales_details_modifiers` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_sale_consumptions
CREATE TABLE IF NOT EXISTS `tbl_sale_consumptions` (
  `id` varchar(36) NOT NULL,
  `sale_id` varchar(36) NOT NULL,
  `order_status` tinyint(1) NOT NULL COMMENT '1=new order,2=invoiced order, 3=closed order',
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(50) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_sale_consumptions_sale_id_foreign` (`sale_id`),
  KEY `tbl_sale_consumptions_user_id_foreign` (`user_id`),
  KEY `tbl_sale_consumptions_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_sale_consumptions_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_sale_consumptions_sale_id_foreign` FOREIGN KEY (`sale_id`) REFERENCES `tbl_sales` (`id`),
  CONSTRAINT `tbl_sale_consumptions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_sale_consumptions: ~0 rows (approximately)
DELETE FROM `tbl_sale_consumptions`;
/*!40000 ALTER TABLE `tbl_sale_consumptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_sale_consumptions` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_sale_consumptions_of_menus
CREATE TABLE IF NOT EXISTS `tbl_sale_consumptions_of_menus` (
  `id` varchar(36) NOT NULL,
  `ingredient_id` varchar(36) NOT NULL,
  `consumption` float(10,2) DEFAULT NULL,
  `sale_consumption_id` varchar(36) NOT NULL,
  `sales_id` varchar(36) NOT NULL,
  `order_status` tinyint(1) NOT NULL COMMENT '1=new order,2=invoiced order, 3=closed order',
  `food_menu_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(50) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_sale_consumptions_of_menus_ingredient_id_foreign` (`ingredient_id`),
  KEY `tbl_sale_consumptions_of_menus_sale_consumption_id_foreign` (`sale_consumption_id`),
  KEY `tbl_sale_consumptions_of_menus_sales_id_foreign` (`sales_id`),
  KEY `tbl_sale_consumptions_of_menus_food_menu_id_foreign` (`food_menu_id`),
  KEY `tbl_sale_consumptions_of_menus_user_id_foreign` (`user_id`),
  KEY `tbl_sale_consumptions_of_menus_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_sale_consumptions_of_menus_food_menu_id_foreign` FOREIGN KEY (`food_menu_id`) REFERENCES `tbl_food_menus` (`id`),
  CONSTRAINT `tbl_sale_consumptions_of_menus_ingredient_id_foreign` FOREIGN KEY (`ingredient_id`) REFERENCES `tbl_ingredients` (`id`),
  CONSTRAINT `tbl_sale_consumptions_of_menus_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_sale_consumptions_of_menus_sale_consumption_id_foreign` FOREIGN KEY (`sale_consumption_id`) REFERENCES `tbl_sale_consumptions` (`id`),
  CONSTRAINT `tbl_sale_consumptions_of_menus_sales_id_foreign` FOREIGN KEY (`sales_id`) REFERENCES `tbl_sales` (`id`),
  CONSTRAINT `tbl_sale_consumptions_of_menus_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_sale_consumptions_of_menus: ~0 rows (approximately)
DELETE FROM `tbl_sale_consumptions_of_menus`;
/*!40000 ALTER TABLE `tbl_sale_consumptions_of_menus` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_sale_consumptions_of_menus` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_sale_consumptions_of_modifiers_of_menus
CREATE TABLE IF NOT EXISTS `tbl_sale_consumptions_of_modifiers_of_menus` (
  `id` varchar(36) NOT NULL,
  `ingredient_id` varchar(36) NOT NULL,
  `consumption` float(10,2) DEFAULT NULL,
  `sale_consumption_id` varchar(36) NOT NULL,
  `sales_id` varchar(36) NOT NULL,
  `order_status` tinyint(1) NOT NULL COMMENT '1=new order,2=invoiced order, 3=closed order',
  `food_menu_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(50) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_sale_consumptions_of_modifiers_ingredient_id_foreign` (`ingredient_id`),
  KEY `tbl_sale_consumptions_of_modifiers_sale_consumption_id_foreign` (`sale_consumption_id`),
  KEY `tbl_sale_consumptions_of_modifiers_sales_id_foreign` (`sales_id`),
  KEY `tbl_sale_consumptions_of_modifiers_food_menu_id_foreign` (`food_menu_id`),
  KEY `tbl_sale_consumptions_of_modifiers_user_id_foreign` (`user_id`),
  KEY `tbl_sale_consumptions_of_modifiers_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_sale_consumptions_of_modifiers_food_menu_id_foreign` FOREIGN KEY (`food_menu_id`) REFERENCES `tbl_food_menus` (`id`),
  CONSTRAINT `tbl_sale_consumptions_of_modifiers_ingredient_id_foreign` FOREIGN KEY (`ingredient_id`) REFERENCES `tbl_ingredients` (`id`),
  CONSTRAINT `tbl_sale_consumptions_of_modifiers_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_sale_consumptions_of_modifiers_sale_consumption_id_foreign` FOREIGN KEY (`sale_consumption_id`) REFERENCES `tbl_sale_consumptions` (`id`),
  CONSTRAINT `tbl_sale_consumptions_of_modifiers_sales_id_foreign` FOREIGN KEY (`sales_id`) REFERENCES `tbl_sales` (`id`),
  CONSTRAINT `tbl_sale_consumptions_of_modifiers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_sale_consumptions_of_modifiers_of_menus: ~0 rows (approximately)
DELETE FROM `tbl_sale_consumptions_of_modifiers_of_menus`;
/*!40000 ALTER TABLE `tbl_sale_consumptions_of_modifiers_of_menus` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_sale_consumptions_of_modifiers_of_menus` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_suppliers
CREATE TABLE IF NOT EXISTS `tbl_suppliers` (
  `id` varchar(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `contact_person` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(300) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `user_id` varchar(36) NOT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_suppliers_user_id_foreign` (`user_id`),
  CONSTRAINT `tbl_suppliers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_suppliers: ~0 rows (approximately)
DELETE FROM `tbl_suppliers`;
/*!40000 ALTER TABLE `tbl_suppliers` DISABLE KEYS */;
INSERT INTO `tbl_suppliers` (`id`, `name`, `contact_person`, `phone`, `email`, `address`, `description`, `user_id`, `del_status`, `created_at`, `updated_at`) VALUES
	('c9616d6f-ae8e-4470-9c0a-1a7e8ba325e9', 'Ade Indra', '55', '0852', 'ade2@gmail.com', 'Jambi', 'Update Deskripsi', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', 'Live', '2019-12-19 11:49:49', '2019-12-19 11:51:31');
/*!40000 ALTER TABLE `tbl_suppliers` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_supplier_payments
CREATE TABLE IF NOT EXISTS `tbl_supplier_payments` (
  `id` varchar(36) NOT NULL,
  `date` date NOT NULL,
  `supplier_id` varchar(36) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `note` text DEFAULT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(50) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_supplier_payments_user_id_foreign` (`user_id`),
  KEY `tbl_supplier_payments_outlet_id_foreign` (`outlet_id`),
  KEY `tbl_supplier_payments_supplier_id_foreign` (`supplier_id`),
  CONSTRAINT `tbl_supplier_payments_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_supplier_payments_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `tbl_suppliers` (`id`),
  CONSTRAINT `tbl_supplier_payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_supplier_payments: ~0 rows (approximately)
DELETE FROM `tbl_supplier_payments`;
/*!40000 ALTER TABLE `tbl_supplier_payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_supplier_payments` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_tables
CREATE TABLE IF NOT EXISTS `tbl_tables` (
  `id` varchar(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `sit_capacity` int(11) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(50) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_tables_user_id_foreign` (`user_id`),
  KEY `tbl_tables_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_tables_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_tables_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_tables: ~34 rows (approximately)
DELETE FROM `tbl_tables`;
/*!40000 ALTER TABLE `tbl_tables` DISABLE KEYS */;
INSERT INTO `tbl_tables` (`id`, `name`, `sit_capacity`, `position`, `description`, `user_id`, `outlet_id`, `del_status`, `created_at`, `updated_at`) VALUES
	('050c243e-2c40-4495-b2fc-e66a88d701ae', 'Meja 2', 5, 'Lantai 1', NULL, '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-26 08:23:40', '2019-12-26 08:23:40'),
	('1200e8e5-bf12-48b8-925e-f31ec9ac1f08', 'Meja 12', 234, '334', 'Setup', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-29 08:03:20', '2019-12-29 08:03:20'),
	('1979b1f3-59ac-4748-8b4a-bb19c6a1a604', 'Meja 1', 5, 'Lantai 1 OK', 'Kursi empuk', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-19 11:32:43', '2020-01-04 06:08:31'),
	('28db2681-5a3e-4562-a8aa-202b0e186097', 'Meja 14', 4, 'Lantai 6', 'OK', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-29 08:32:44', '2019-12-29 08:32:44'),
	('29910a9a-5d54-4d7c-8dd1-8508f542de40', 'Meja Lagi', 3, 'Posisi', 'Deskripsi', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-26 08:41:07', '2019-12-26 08:41:07'),
	('30759589-f30c-41df-88d9-8c8176a093f8', 'Data', 34, 'Jambi', '3434asfdasdf', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Deleted', '2019-12-26 08:48:14', '2020-01-01 03:01:24'),
	('30e8efea-5a56-45da-b8a9-1cd884cb7f10', 'Meja 6 API', 78, 'Lantai 10', 'Edi Ganteng', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-26 08:31:33', '2019-12-26 08:31:33'),
	('31ee1c71-dfe0-47e7-a254-08be3ed5b372', 'Meja 34', 87, 'Lantai 20', 'Asek', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Deleted', '2019-12-26 08:34:41', '2020-01-01 03:01:13'),
	('35541553-e906-4ab8-b07b-908cc1ad2e6f', 'Meja 2', 5, 'Lantai 2', 'Tambah Data Meja 2', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '5f019932-1db4-4418-80b6-a29f23a88ca7', 'Live', '2019-12-26 08:54:46', '2019-12-26 08:54:46'),
	('3717b9bb-bb37-4a66-958f-001671bc609c', 'Maja Judi', 44, 'Lantai K', 'asdaa', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '5f019932-1db4-4418-80b6-a29f23a88ca7', 'Live', '2019-12-26 17:53:53', '2019-12-26 17:53:53'),
	('398b1f3b-63bc-49f3-a23b-d9ce7d10b37b', 'Meja Kasir', 3, 'Lantai Dasar', 'Ok', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '5f019932-1db4-4418-80b6-a29f23a88ca7', 'Live', '2019-12-26 17:49:58', '2019-12-26 17:49:58'),
	('3a6be124-3801-4d75-99c2-2aac3e4e0f25', 'MEJA 1', 4, 'Lantai 6', 'OK', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-31 09:25:45', '2019-12-31 09:25:45'),
	('3b37f6e7-b9f5-45e0-919b-4b045ce005c9', 'Meja 100', 47, 'Lantai 30', 'Serahin Kegua', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-26 08:33:16', '2019-12-26 08:33:16'),
	('43960d3a-8ca4-4de9-99aa-fbe0b657c623', 'Meja Ku', 4, 'Lantai 10', 'OK', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-26 10:19:40', '2019-12-26 10:19:40'),
	('74354dcd-06ea-430e-818c-d2044c2c3deb', 'Edi Kuncoro Meja', 455, 'Lantai 11', 'Enak Bro', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '5f019932-1db4-4418-80b6-a29f23a88ca7', 'Deleted', '2019-12-26 09:01:13', '2020-01-01 03:03:36'),
	('7a6fd2af-6df4-4ec0-a8b9-eaee5c4411e6', 'Meja 4', 4, 'Lantai 100', 'Edi lagi', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '5f019932-1db4-4418-80b6-a29f23a88ca7', 'Live', '2019-12-26 08:59:02', '2019-12-26 08:59:02'),
	('7b797949-3735-4934-9ddf-aaf070623ab1', 'Meja Kasir', 3, 'Lantai Dasar', 'Ok', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '5f019932-1db4-4418-80b6-a29f23a88ca7', 'Live', '2019-12-26 17:49:37', '2019-12-26 17:49:37'),
	('821495ba-0327-49bf-a212-d7b920472b55', 'MEJA KU', 3, 'LANTAI DASAR', 'OK', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Deleted', '2020-01-04 06:08:56', '2020-01-04 06:09:06'),
	('8540316d-1f08-4268-90d3-6c5144e2f10b', 'Meja 11', 3, 'Lantai Tengah', 'Ok', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-26 13:12:36', '2019-12-26 13:12:36'),
	('85537807-8e33-483e-b4fd-69aeba69a0be', 'Meja 1', 5, 'Lantai 1', 'Tambah Data Meja 1', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '5f019932-1db4-4418-80b6-a29f23a88ca7', 'Live', '2019-12-26 08:53:49', '2019-12-26 08:53:49'),
	('85a83966-353e-4d3a-9043-c24a1d0847a6', 'Meja 3', 4, 'Lantai 1', 'Meja Baru', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '5f019932-1db4-4418-80b6-a29f23a88ca7', 'Live', '2019-12-26 08:57:13', '2019-12-26 08:57:13'),
	('99496e72-dc49-482d-ad47-8fd1620957c5', 'Meja 10', 4, 'Lantai 3', 'Ok', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-26 10:25:02', '2019-12-26 10:25:02'),
	('a607fa11-f153-4053-8b84-c04af5ebf4c0', 'Meja 13', 4, 'Lantai 5', 'Ok', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-29 08:31:27', '2019-12-29 08:31:27'),
	('a6969023-e4f0-4a1b-b0eb-ea27be12308a', 'Meja Domino', 2, '33', '344', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '5f019932-1db4-4418-80b6-a29f23a88ca7', 'Live', '2019-12-26 17:52:56', '2019-12-26 17:52:56'),
	('a6ef30b1-41da-4c42-8ff7-e8cb28c36f02', 'Meja Kasir 2', 3, 'Lantai Dasar', 'Ok', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '5f019932-1db4-4418-80b6-a29f23a88ca7', 'Live', '2019-12-26 17:50:30', '2019-12-26 17:50:30'),
	('aa229709-a466-421f-8fd1-58d71ecc3f38', 'Meja 5 Lagi', 54, 'Lantai 23', 'Tambanah', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-26 08:37:31', '2019-12-26 08:37:31'),
	('b9c72dcc-778e-4cfb-8fae-49677c231f5d', 'Meja 2', 5, 'Lantai 1', NULL, '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-19 11:40:38', '2019-12-19 11:40:38'),
	('c7e15d29-4224-49ad-8e6f-fe714424811a', 'Meja 8', 8, 'Lantai 1', 'Ok', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-26 10:17:35', '2019-12-26 10:17:35'),
	('d4a369c8-57db-45a1-a2a6-617dc5c91c0d', 'Meja Baru', 12, 'Lantai Dasar', 'Kaammuu', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-26 09:24:53', '2019-12-26 09:24:53'),
	('da449cd5-7b1a-431e-80a3-d7da41cf6117', 'Meja 2', 5, 'Lantai 1', NULL, '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Deleted', '2019-12-31 09:33:10', '2019-12-31 12:19:21'),
	('e451ccfe-1cb2-4ace-8801-581835a6203e', 'Meja 5', 10, 'Lantai 2', 'Di input dari mobile', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-26 08:29:42', '2019-12-26 08:29:42'),
	('f3420254-7423-4d81-961c-a0668fe4b0a4', 'Meja 4', 3, 'Lantai 2', 'Warna Merah', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-26 08:24:15', '2019-12-26 08:24:15'),
	('f784291d-5809-4490-a111-ee0b5496dff0', 'Meja Apa Saja', 4, 'Jakarta', 'Jambi', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-26 08:44:08', '2019-12-26 08:44:08'),
	('f9a53b44-b14b-4c6c-b694-77984bebdb7b', 'MEJA 1', 555, 'Lantai 6', 'OK', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-31 09:25:34', '2019-12-31 09:25:34'),
	('fa9ada25-8418-4cd8-8e40-d8afebf53812', 'Meja 10', 4, 'Lantai 6', 'Ok', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '7a8c2766-c82c-4f89-a7c3-282f801eb612', 'Live', '2019-12-26 10:18:31', '2019-12-26 10:18:31'),
	('fc124a2c-2185-49ca-ac09-8841a5a6e40c', 'Meja Kuncoro', 4, 'Mas Prawito Dirumah', 'Lagi Dong', '02f31b6a-d61f-498a-bfdb-51f506c4b5fc', '5f019932-1db4-4418-80b6-a29f23a88ca7', 'Live', '2019-12-26 09:01:55', '2019-12-26 09:01:55');
/*!40000 ALTER TABLE `tbl_tables` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_units
CREATE TABLE IF NOT EXISTS `tbl_units` (
  `id` varchar(36) NOT NULL,
  `unit_name` varchar(10) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_units: ~2 rows (approximately)
DELETE FROM `tbl_units`;
/*!40000 ALTER TABLE `tbl_units` DISABLE KEYS */;
INSERT INTO `tbl_units` (`id`, `unit_name`, `description`, `del_status`, `created_at`, `updated_at`) VALUES
	('10623070-1f28-45d2-a9d0-271a2bf7a403', 'Ons', 'hg (hektogram)', 'Live', '2019-12-19 09:48:08', '2019-12-19 09:51:11'),
	('401855d0-6a2a-4893-9789-df3636667f8f', 'Kg', 'Kilogram', 'Live', '2019-12-19 09:44:38', '2019-12-19 09:44:38'),
	('a9f2f258-d40f-4d2b-9654-bb1bc1c4a0ac', 'g', 'Gram', 'Live', '2019-12-19 09:45:18', '2019-12-19 09:45:18');
/*!40000 ALTER TABLE `tbl_units` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_users
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` varchar(36) NOT NULL,
  `full_name` varchar(150) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email_address` varchar(50) NOT NULL,
  `language` varchar(100) NOT NULL DEFAULT 'english',
  `card_name` varchar(10) NOT NULL,
  `card_number` varchar(20) NOT NULL,
  `active_status` varchar(25) NOT NULL DEFAULT 'Active',
  `user_login_id` varchar(36) NOT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tbl_users_user_login_id_foreign` (`user_login_id`),
  CONSTRAINT `tbl_users_user_login_id_foreign` FOREIGN KEY (`user_login_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_users: ~0 rows (approximately)
DELETE FROM `tbl_users`;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` (`id`, `full_name`, `phone`, `email_address`, `language`, `card_name`, `card_number`, `active_status`, `user_login_id`, `del_status`, `created_at`, `updated_at`) VALUES
	('02f31b6a-d61f-498a-bfdb-51f506c4b5fc', 'Muhammad Ari Tri Pajar', '085227281672', 'ari@gmail.com', 'english', 'ktp', '989899878', 'Active', 'a1bff834-4a26-47a4-b99a-35872baa2bd1', 'Live', '2019-12-19 05:18:05', '2019-12-19 05:18:05'),
	('8d2e9341-a759-49c9-bf71-398db50bb742', 'Ade Indra Saputra', '085227281672', 'ade@gmail.com', 'english', 'ktp', '989899878', 'Active', 'a3c5caf8-ff2e-4fa0-9d56-2cb911c8bad0', 'Live', '2020-01-24 06:38:31', '2020-01-24 06:38:31');
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_vats
CREATE TABLE IF NOT EXISTS `tbl_vats` (
  `id` varchar(36) NOT NULL,
  `name` varchar(50) NOT NULL,
  `percentage` float(10,2) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `update_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_vats_user_id_foreign` (`user_id`),
  KEY `tbl_vats_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_vats_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_vats_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_vats: ~0 rows (approximately)
DELETE FROM `tbl_vats`;
/*!40000 ALTER TABLE `tbl_vats` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_vats` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_wastes
CREATE TABLE IF NOT EXISTS `tbl_wastes` (
  `id` varchar(36) NOT NULL,
  `reference_no` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `total_loss` float(10,2) NOT NULL,
  `note` text DEFAULT NULL,
  `employee_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `food_menu_id` varchar(36) NOT NULL,
  `food_menu_waste_qty` varchar(36) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_wastes_employee_id_foreign` (`employee_id`),
  KEY `tbl_wastes_user_id_foreign` (`user_id`),
  KEY `tbl_wastes_outlet_id_foreign` (`outlet_id`),
  KEY `tbl_wastes_food_menu_id_foreign` (`food_menu_id`),
  CONSTRAINT `tbl_wastes_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employees` (`id`),
  CONSTRAINT `tbl_wastes_food_menu_id_foreign` FOREIGN KEY (`food_menu_id`) REFERENCES `tbl_food_menus` (`id`),
  CONSTRAINT `tbl_wastes_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_wastes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.tbl_wastes: ~0 rows (approximately)
DELETE FROM `tbl_wastes`;
/*!40000 ALTER TABLE `tbl_wastes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_wastes` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.tbl_waste_ingredients
CREATE TABLE IF NOT EXISTS `tbl_waste_ingredients` (
  `id` varchar(36) NOT NULL,
  `ingredient_id` varchar(36) NOT NULL,
  `waste_amount` float(10,2) NOT NULL,
  `last_purchase_price` float(10,2) NOT NULL,
  `loss_amount` float(10,2) NOT NULL,
  `waste_id` varchar(36) NOT NULL,
  `outlet_id` varchar(36) NOT NULL,
  `del_status` varchar(10) DEFAULT 'Live',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_waste_ingredients_ingredient_id_foreign` (`ingredient_id`),
  KEY `tbl_waste_ingredients_waste_id_foreign` (`waste_id`),
  KEY `tbl_waste_ingredients_outlet_id_foreign` (`outlet_id`),
  CONSTRAINT `tbl_waste_ingredients_ingredient_id_foreign` FOREIGN KEY (`ingredient_id`) REFERENCES `tbl_ingredients` (`id`),
  CONSTRAINT `tbl_waste_ingredients_outlet_id_foreign` FOREIGN KEY (`outlet_id`) REFERENCES `tbl_outlets` (`id`),
  CONSTRAINT `tbl_waste_ingredients_waste_id_foreign` FOREIGN KEY (`waste_id`) REFERENCES `tbl_wastes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table db_waroeng.tbl_waste_ingredients: ~0 rows (approximately)
DELETE FROM `tbl_waste_ingredients`;
/*!40000 ALTER TABLE `tbl_waste_ingredients` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_waste_ingredients` ENABLE KEYS */;

-- Dumping structure for table db_waroeng.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(36) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `api_token` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(45) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table db_waroeng.users: ~7 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `api_token`, `password`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
	('0bb44bcf-11ea-48a0-bab9-4fee54a66e34', 'Kusmono', 'kusmono@gmail.com', NULL, '$2y$10$7a9nsYX1nAO2Hw0.X0/uBOIghvkLJctXpG.WfsxCPvaQIiZZ.Tqz6', '$2y$10$v7bc5yspHNzjWYJO6CeiCu0MOGUojYdS4WIhDTtkxmR4.K7dBUyO6', 'Admin', NULL, '2019-12-26 08:53:02', '2020-01-28 08:08:33'),
	('293bfaf1-f105-40ec-ad96-528b99a53b36', 'Edi Kuncuro', 'kuncoro@gmail.com', NULL, '$2y$10$CiZZ93oWueeGAlCPRJs1D.G/O9sUk8j7qF6C.uyjD5YZpVYCb28Aq', '$2y$10$blJkeZK58xYFzGcRatijueTfhb8ygNJ0.bUyh8Cn/sOE2pTWk4Jni', 'Waiter', NULL, '2019-12-26 08:52:00', '2019-12-26 08:52:00'),
	('a1bff834-4a26-47a4-b99a-35872baa2bd1', 'Muhammad Ari Tri Pajar', 'ari@gmail.com', NULL, '$2y$10$y17vDchPQuut19AJDlHfJOwYb/AQJaoIvwX9.DItFgGdTbacVAcgy', '$2y$10$NrU8veXSWZVQkkk6liEpX.y.NKg0nbUtkcXVvmCY1ulDuMida36F.', 'Owner', 'QZOEisberH9mLbwqfWc1igJYLIx7JG3bFBBOshyXI7q5tR1z7mYssTUeTSjs', '2019-12-19 05:18:05', '2019-12-19 05:18:05'),
	('a3c5caf8-ff2e-4fa0-9d56-2cb911c8bad0', 'Ade Indra Saputra', 'ade@gmail.com', NULL, '$2y$10$kTpXC24s7kwB9gq/.B49FeBgR6/Msvrv2mc6TDAsr85/.nuOCTkCC', '$2y$10$3vNiN0tYTLu8sLNGvyUKKulF2DMV84bRqe1nAe.vj3mHwbM2UotDK', 'owner', NULL, '2020-01-24 06:38:31', '2020-01-24 06:38:31'),
	('a67ae7bc-7877-4f32-976a-c783df0a057c', 'Susi', 'susi@gmail.com', NULL, '$2y$10$llwPZkAz/oAO/tUAVE0lNeKZynzZlRgv1a7V4o88tAjoZ8wKrWVA2', '$2y$10$sIHLQPwYuDCxsJrArEi1luJ9QYQK7u8zl867iaAN8mnCYSo9CNXSm', 'Casier', NULL, '2019-12-19 07:37:33', '2020-01-28 14:57:48'),
	('ac6f05fe-c360-4f14-81e7-d07e21e1ab10', 'Novel Baswedan', 'novel@gmail.com', NULL, '$2y$10$1jE04TSKQKqxvc0E4ZjrzO8mImN5of4Bswpdum8dXOkS7r7Ti79O2', '$2y$10$KqxQEbI9h0MprQ6VjjAwsOUYA66AYbVzgHRHaQ.N4c9DsZZ5aAi8G', 'Admin', NULL, '2020-01-25 04:37:58', '2020-01-25 09:40:51'),
	('b706326f-732d-4ac7-bfe4-e1ef23579852', 'Edi Kuswantoro', 'edi@gmail.com', NULL, '$2y$10$HnKJnsKb2bgqk8SA4K1m3uyjuz9n9wWmUcMFjoGSz./o0SIc9H9b2', '$2y$10$mO4MvcRqe4HF5Ks.lZgzq.ijq.nd1DODTl.1J313GxmqGNGaV5kUW', 'Kitchen', NULL, '2019-12-19 07:44:46', '2019-12-19 07:44:46');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
