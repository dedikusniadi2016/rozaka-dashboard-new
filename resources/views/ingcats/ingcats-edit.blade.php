@extends('layout.master')
@section('title', 'Edit Category Ingredient')
@section('parentPageTitle', 'Ingredients')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
@stop
@section('content')
<!-- Vertical Layout -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <!-- <div class="alert alert-warning" role="alert">
            <strong>Bootstrap</strong> Better check yourself, <a target="_blank" href="https://getbootstrap.com/docs/4.2/components/forms/">View More</a>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
            </button>
        </div> -->
        <div class="card">
            <div class="header">
                <h2><strong>Edit</strong> Category Ingredient</h2>
            </div>
            <div class="body">
                <form method="POST" action="{{ route('ingcats.update', $ingcat->id) }}">
                    @method('put')
                    @csrf
                    <label for="category_name">Category Name</label>
                    <div class="form-group">                                
                        <input type="text" id="category_name" name="category_name" class="form-control @error('category_name') is-invalid @enderror" placeholder="Category Name" value="{{ $ingcat->category_name }}">
                        @error('category_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <label for="description">Descrtiption</label>
                    <div class="form-group">
                        <div class="form-line">
                            <textarea rows="4" class="form-control no-resize" name="description" id="description" placeholder="Please type what you want...">{{ $ingcat->description }}</textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">UPDATE</button>
                </form>
            </div>
        </div>
    </div>
</div>

@stop