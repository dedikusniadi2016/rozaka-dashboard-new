<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <div class="navbar-brand">
        <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
        <a href="{{route('dashboard.index')}}"><img src="{{ asset('assets/images/logo-rozaka.svg')}}" width="25" alt="Aero"><span class="m-l-10">ROZAKA POS</span></a>
    </div>
    <div class="menu">
        <ul class="list">
            <li>
                <div class="user-info">
                    <div class="image"><a href="#"><img src="{{asset('assets/images/profile_av.jpg')}}" alt="User"></a></div>
                    <div class="detail">
                        <h4>{{ Str::words(Auth::user()->name, 1, '...') }}</h4>
                        <small>{{ Auth::user()->level }} - @if( Auth::user()->level === 'Admin') {{ Auth::user()->employee->outlet->outlet_name }} @endif</small>
                    </div>
                </div>
            </li>        
            @if(Auth::user()->level === 'Owner')
            <li class="{{ Request::segment(1) === 'dashboard' ? 'active open' : null }}"><a href="{{route('dashboard.index')}}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
            <li class="{{ Request::segment(1) === 'my-profile' ? 'active open' : null }}"><a href="{{route('profile.my-profile')}}"><i class="zmdi zmdi-account"></i><span>My Profile</span></a></li>
            <li class="{{ Request::segment(1) === 'outlets' ? 'active open' : null }}"><a href="{{route('outlets')}}"><i class="zmdi zmdi-store-24"></i><span>Outlets</span></a></li>
            <li class="{{ Request::segment(1) === 'employees' ? 'active open' : null }}"><a href="{{route('employees')}}"><i class="zmdi zmdi-accounts"></i><span>Employees</span></a></li>
            @endif

            @if(Auth::user()->level === 'Admin')
            <li class="{{ Request::segment(1) === 'dashboard' ? 'active open' : null }}"><a href="{{route('dashboard.index')}}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
            <li class="{{ Request::segment(1) === 'employees' ? 'active open' : null }}"><a href="{{route('employees')}}"><i class="zmdi zmdi-accounts"></i><span>Employees</span></a></li>
            <li class="{{ Request::segment(1) === 'customers' ? 'active open' : null }}"><a href="{{route('customers')}}"><i class="zmdi zmdi-accounts"></i><span>Customers</span></a></li>
            <li class="{{ Request::segment(1) === 'ingredient' ? 'active open' : null }}">
                <a href="#ingredient" class="menu-toggle"><i class="zmdi zmdi-labels"></i> <span>Ingredient</span></a>
                <ul class="ml-menu">
                    <li class="{{ Request::segment(2) === 'ingcats' ? 'active' : null }}"><a href="{{route('ingcats')}}">Category</a></li>
                    <li class="{{ Request::segment(2) === 'ingredients' ? 'active' : null }}"><a href="{{route('ingredients')}}">Ingredients</a></li>
                </ul>
            </li>
            <li class="{{ Request::segment(1) === 'food' ? 'active open' : null }}">
                <a href="#food" class="menu-toggle"><i class="zmdi zmdi-cake"></i> <span>Foods</span></a>
                <ul class="ml-menu">
                    <li class="{{ Request::segment(2) === 'maincategory' ? 'active' : null }}"><a href="{{route('foodm')}}">Main Category</a></li>
                    <li class="{{ Request::segment(2) === 'category' ? 'active' : null }}"><a href="{{route('foodc')}}">Food Category</a></li>
                    <li class="{{ Request::segment(2) === 'modifiers' ? 'active' : null }}"><a href="{{route('modifiers')}}">Modifiers</a></li>
                    <li class="{{ Request::segment(2) === 'foodmenus' ? 'active' : null }}"><a href="{{route('foodmenus')}}">Food Menu</a></li>
                    <li class="{{ Request::segment(2) === 'promo' ? 'active' : null }}"><a href="{{route('foodp')}}">Setting Promo</a></li>
                </ul>
            </li>

            <li class="{{ Request::segment(1) === 'suppliers' ? 'active open' : null }}"><a href="{{route('suppliers')}}"><i class="zmdi zmdi-accounts"></i><span>Supplier</span></a></li>

            <li class="{{ Request::segment(1) === 'tables' ? 'active open' : null }}"><a href="{{route('tables')}}"><i class="zmdi zmdi-accounts"></i><span>Tables</span></a></li>

            <li class="{{ Request::segment(1) === 'iventory' ? 'active open' : null }}"><a href="{{route('iventory')}}"><i class="zmdi zmdi-accounts"></i><span>Iventory</span></a></li>

            <li class="{{ Request::segment(1) === 'wastes' ? 'active open' : null }}"><a href="{{route('wastes')}}"><i class="zmdi zmdi-accounts"></i><span>Wastes</span></a></li>



            

            @endif
            
    </div>
</aside>
