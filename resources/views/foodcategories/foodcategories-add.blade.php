@extends('layout.master')
@section('title', 'Add Category Food')
@section('parentPageTitle', 'Food')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
@stop
@section('content')
<!-- Vertical Layout -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <!-- <div class="alert alert-warning" role="alert">
            <strong>Bootstrap</strong> Better check yourself, <a target="_blank" href="https://getbootstrap.com/docs/4.2/components/forms/">View More</a>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
            </button>
        </div> -->
        <div class="card">
            <div class="header">
                <h2><strong>Add</strong> Category</h2>
            </div>
            <div class="body">
                <form method="POST" action="{{ route('foodc.store') }}">
                    @csrf
                    <label for="category_name">Category Name</label>
                    <div class="form-group">                                
                        <input type="text" id="category_name" name="category_name" class="form-control @error('category_name') is-invalid @enderror" placeholder="Main Category Name" value="{{ old('category_name') }}">
                        @error('category_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <label for="description">Descrtiption</label>
                    <div class="form-group">
                        <div class="form-line">
                            <textarea rows="4" class="form-control no-resize" name="description" id="description" placeholder="Please type what you want...">{{ old('description') }}</textarea>
                        </div>
                    </div>

                    <div class="row clearfix">
                      <div class="col-sm-6">
                        <label for="ma_ca_id">Main Category</label>
                        <div class="form-group">                                
                            <select name="ma_ca_id" id="ma_ca_id" class="form-control show-tick @error('ma_ca_id') is-invalid @enderror">
                                <option value="">-- Please select --</option>
                                @foreach($foodmcategories as $foodmcategory)
                                <option value="{{ $foodmcategory->id }}">{{ $foodmcategory->ma_ca_name }}</option>
                                <@endforeach
                            </select>
                            @error('ma_ca_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                      
                    </div>
                    
                    <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</div>

@stop