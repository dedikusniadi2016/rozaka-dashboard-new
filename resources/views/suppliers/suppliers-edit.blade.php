@extends('layout.master')
@section('title', 'Add suppliers')
@section('parentPageTitle', 'suppliers')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
@stop
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="header">
                <h2><strong>Edit</strong> Suppliers </h2>
            </div>
            <div class="body">
                <form method="POST" action="{{ route('suppliers.update', $supplier->id) }}">
                @method('put')
                    @csrf
                    <label for="name">Suppliers Name</label>
                    <div class="form-group">                                
                        <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Supplier Name" value="{{ $supplier->name }}">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="row clearfix">
                      <div class="col-sm-6">
                        <label for="contact_person">Contact Person</label>
                        <div class="form-group">                                
                            <input type="text" id="contact_person" name="contact_person" class="form-control @error('contact_person') is-invalid @enderror" placeholder="Contact Person" value="{{ $supplier->contact_person }}">
                            @error('contact_person')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                       <div class="col-sm-6">
                        <label for="phone">phone</label>
                        <div class="form-group">                                
                            <input type="text" id="phone" name="phone" class="form-control @error('phone') is-invalid @enderror" placeholder="Phone" value="{{ $supplier->phone }}">
                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                  </div>
                   
                    <div class="row clearfix">
                      <div class="col-sm-6">
                        <label for="email">Email</label>
                        <div class="form-group">                                
                            <input type="email" id="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{ $supplier->email }}">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <label for="email">Address</label>
                        <div class="form-group">                                
                             <textarea rows="4" class="form-control no-resize" name="address" id="address" placeholder="Please type what you want...">{{ $supplier->address }}</textarea>
                        </div>
                      </div>
                    </div>

                    <label for="name">Description</label>
                    <div class="form-group">     
                     <div class="form-line">      
                        <textarea rows="4" class="form-control no-resize" name="description" id="description" placeholder="Please type what you want...">{{ $supplier->description }}</textarea>
                    </div>
                </div>
                    <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</div>

@stop
@section('page-script')
<script src="{{asset('assets/plugins/momentjs/moment.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
<script src="{{asset('assets/js/pages/forms/basic-form-elements.js')}}"></script>
@stop