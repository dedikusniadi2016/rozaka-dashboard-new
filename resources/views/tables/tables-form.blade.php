@extends('layout.master')
@section('title', 'Add Tables')
@section('parentPageTitle', 'tables')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
@stop
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="header">
                <h2><strong>Add</strong> Tables </h2>
            </div>
            <div class="body">
                <form method="POST" action="{{ route('tables.store') }}">
                    @csrf
                    <label for="name">Tables Name</label>
                    <div class="form-group">                                
                        <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Supplier Name" value="{{ old('name') }}">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="row clearfix">
                      <div class="col-sm-6">
                        <label for="sit_capacity"> Sit Capacity </label>
                        <div class="form-group">                                
                            <input type="text" id="sit_capacity" name="sit_capacity" class="form-control @error('sit_capacity') is-invalid @enderror" placeholder="Sit Capacity" value="{{ old('sit_capacity') }}">
                            @error('sit_capacity')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>

                       <div class="col-sm-6">
                        <label for="position">Position</label>
                        <div class="form-group">                                
                            <input type="text" id="position" name="position" class="form-control @error('position') is-invalid @enderror" placeholder="position" value="{{ old('position') }}">
                            @error('position')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                  </div>
                   
                    <label for="name">Description</label>
                    <div class="form-group">     
                     <div class="form-line">      
                        <textarea rows="4" class="form-control no-resize" name="description" id="description" placeholder="Please type what you want...">{{ old('description') }}</textarea>
                    
                        @error('description')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                          </span>
                            @enderror

                    </div>
                </div>
                    <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</div>

@stop
@section('page-script')
<script src="{{asset('assets/plugins/momentjs/moment.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
<script src="{{asset('assets/js/pages/forms/basic-form-elements.js')}}"></script>
@stop