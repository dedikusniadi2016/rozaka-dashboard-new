@extends('layout.master')
@section('title', 'List Tables')
@section('parentPageTitle', 'tables')
@section('content')

@if (session('status'))
<div class="alert alert-success" role="alert">
    <strong>Info</strong> {{ session('status') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
    </button>
</div>
@endif

<div class="row clearfix">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="card project_list">
            <a href="{{ route('tables.create') }}" class="btn btn-success">Add Table</a>
            <div class="table-responsive contact">
                <table class="table table-hover c_list c_table theme-color">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Sit Capacity</th>
                            <th>Position</th>
                            <th>Description</th>
                            <th>status </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tables as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->sit_capacity   }}</td>
                            <td>{{ $item->position }}</td>
                            <td>{{ $item->description }}</td>
                            <td><span class="badge badge-info">{{ $item->del_status }}</span></td>
                            <td>
                                <div class="dropdown"> 
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="{{ route('tables.edit', $item->id) }}">Edit</a>
                                        <a class="dropdown-item" href="javascript:void(0);">Hapus</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $tables->links() }}
            <!-- <ul class="pagination pagination-primary mt-4">
                
            </ul> -->
        </div>
    </div>
</div>
@stop
@section('page-script')
<script src="{{asset('assets/bundles/sparkline.bundle.js')}}"></script>
<script src="{{asset('assets/js/pages/charts/sparkline.js')}}"></script>
@stop