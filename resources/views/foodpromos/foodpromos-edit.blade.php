@extends('layout.master')
@section('title', 'Add Food Promo')
@section('parentPageTitle', 'Food')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/select2/select2.css')}}"/>
@stop
@section('content')
<!-- Vertical Layout -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <!-- <div class="alert alert-warning" role="alert">
            <strong>Bootstrap</strong> Better check yourself, <a target="_blank" href="https://getbootstrap.com/docs/4.2/components/forms/">View More</a>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
            </button>
        </div> -->
        <div class="card">
            <div class="header">
                <h2><strong>Add</strong> Food Promo</h2>
            </div>
            <div class="body">
                <form method="POST" action="{{ route('foodp.update', $foodpromo->id) }}">
                    @method('put')
                    @csrf
                    <label for="food_menu_id">Food Menu</label>
                    <div class="form-group">                                
                        <select name="food_menu_id" id="food_menu_id" class="form-control show-tick ms select2 @error('food_menu_id') is-invalid @enderror">
                            <option value="">-- Please select --</option>
                            @foreach($foodmenus as $foodmenu)
                            <option value="{{ $foodmenu->id }}" @if($foodmenu->id === $foodpromo->food_menu_id) selected @endif>{{ $foodmenu->name }}</option>
                            <@endforeach
                        </select>
                        @error('food_menu_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <label for="promo_name">Promo Name</label>
                    <div class="form-group">                                
                        <input type="text" id="promo_name" name="promo_name" class="form-control @error('promo_name') is-invalid @enderror" placeholder="Promo Name" value="{{ $foodpromo->promo_name }}">
                        @error('promo_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="row clearfix">
                        <div class="col-sm-4">
                            <label for="promo_type">Promo Type</label>
                            <div class="form-group">                                
                                <select name="promo_type" id="promo_type" class="form-control show-tick @error('promo_type') is-invalid @enderror">
                                    <option value="">-- Please select --</option>
                                    <option value="1" @if($foodpromo->promo_type === 1) selected @endif>Persent (%)</option>
                                    <option value="2" @if($foodpromo->promo_type === 2) selected @endif>Rupiah</option>
                                </select>
                                @error('promo_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <label for="promo_value">Promo Value</label>
                            <div class="form-group">                                
                                <input type="text" id="promo_value" name="promo_value" class="form-control @error('promo_value') is-invalid @enderror" placeholder="Promo Name" value="{{ $foodpromo->promo_value }}">
                                @error('promo_value')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                    </div>

                    <div class="row clearfix mb-4">
                        <div class="col-sm-6">
                            <label for="date_start">Starting Promo</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="zmdi zmdi-calendar"></i></span>
                                </div>
                                <input type="text" id="date_start" name="date_start" class="form-control datepicker @error('date_start') is-invalid @enderror" placeholder="Please choose date..." value="{{ $foodpromo->date_start }}">
                                @error('date_start')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="date_done">End Promo</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="zmdi zmdi-calendar"></i></span>
                                </div>
                                <input type="text" id="date_done" name="date_done" class="form-control datepicker @error('date_done') is-invalid @enderror" placeholder="Please choose date..." value="{{ $foodpromo->date_done }}">
                                @error('date_done')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <label for="description">Descrtiption</label>
                    <div class="form-group">
                        <div class="form-line">
                            <textarea rows="4" class="form-control no-resize" name="description" id="description" placeholder="Please type what you want...">{{ $foodpromo->description }}</textarea>
                        </div>
                    </div>

                    
                    
                    <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">UPDATE</button>
                </form>
            </div>
        </div>
    </div>
</div>

@stop
@section('page-script')
<script src="{{asset('assets/plugins/momentjs/moment.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
<script src="{{asset('assets/plugins/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/js/pages/forms/advanced-form-elements.js')}}"></script>
<script src="{{asset('assets/js/pages/forms/basic-form-elements.js')}}"></script>
@stop