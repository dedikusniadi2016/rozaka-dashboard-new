@extends('layout.master')
@section('title', 'List Setting Promo')
@section('parentPageTitle', 'Food')
@section('content')

@if (session('status'))
<div class="alert alert-success" role="alert">
    <strong>Info</strong> {{ session('status') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
    </button>
</div>
@endif

<div class="row clearfix">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="card project_list">
            <a href="{{ route('foodp.create') }}" class="btn btn-success">Add Promo</a>
            <div class="table-responsive">
                <table class="table table-hover c_table theme-color">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Promo Value</th>
                            <th>Description</th>
                            <th>Outlet</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($foodpromos as $item)
                        <tr>
                            <td><a href="ticket-detail.html" title="">{{ $item->foodmenu->name }}</a><br>
                                <small>Date Promo: {{ $item->date_start }} s/d {{ $item->date_done }}</small>
                            </td>
                            <td>Rp. {{ number_format($item->promo_value,0,",",".") }},-</td>
                            <td>{{ $item->description }}</td>
                            <td>{{ $item->outlet->outlet_name }}</td>
                            <td><span class="badge badge-info">{{ $item->del_status }}</span></td>
                            <td>
                                <div class="dropdown"> 
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="{{ route('foodp.edit', $item->id) }}">Edit</a>
                                        <a class="dropdown-item" href="javascript:void(0);">Hapus</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $foodpromos->links() }}
            <!-- <ul class="pagination pagination-primary mt-4">
                
            </ul> -->
        </div>
    </div>
</div>
@stop
@section('page-script')
<script src="{{asset('assets/bundles/sparkline.bundle.js')}}"></script>
<script src="{{asset('assets/js/pages/charts/sparkline.js')}}"></script>
@stop