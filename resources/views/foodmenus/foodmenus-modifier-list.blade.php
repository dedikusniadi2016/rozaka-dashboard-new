@extends('layout.master')
@section('title', 'List Ingredient Modifiers')
@section('parentPageTitle', 'Food')

@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
@stop

@section('content')

@if (session('status'))
<div class="alert alert-success" role="alert">
    <strong>Info</strong> {{ session('status') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
    </button>
</div>
@endif

<div class="row clearfix">
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="card mb-0 mcard_">
            <div class="header">
                <h2><strong>Food Menu</strong> Info</h2>
            </div>
            <div class="body">
                <img src="{{ asset('uploads/'.$foodmenu->photo) }}" class="rounded-circle" alt="profile-image">
                <small class="text-muted">Food Name: </small>
                <p>{{ $foodmenu->name }}</p>
                <hr>
                <small class="text-muted">Sale Price: </small>
                <p>Rp. {{ number_format($foodmenu->sale_price,0,",",".") }},-</p>
            </div>
        </div>

        <div class="card">
            <div class="header">
                <h2><strong>Add</strong> Modifier</h2>
            </div>
            <div class="body">
                <form method="POST" action="{{ route('foodmenus.addmodifier', $foodmenu->id) }}">
                    @csrf
                    <small class="text-muted">Modifier: </small>
                    <div class="form-group">                                
                        <select name="modifier_id" id="modifier_id" class="form-control show-tick @error('modifier_id') is-invalid @enderror">
                            <option value="">-- Please select --</option>
                            @foreach($modifiers as $modifier)
                            <option value="{{ $modifier->id }}">{{ $modifier->name }}</option>
                            <@endforeach
                        </select>
                        @error('modifier_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-raised btn-success btn-block btn-round waves-effect">ADD MODIFIER</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-sm-6 col-xs-12">
        <div class="card project_list">
            <div class="header">
                <h2><strong>Food</strong> Modifier</h2>
            </div>
            <!-- <a href="{{ route('modifiers.create') }}" class="btn btn-success">Add Ingredient</a> -->
            <div class="table-responsive">
                <table class="table table-hover c_table theme-color">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($foodmodifiers as $item)
                        <tr>
                            <td><a href="ticket-detail.html" title="">{{ $item->modifier->name }}</a></td>
                            <td>Rp. {{ number_format($item->modifier->price,0,",",".") }},-</td>
                            <td><span class="badge badge-info">{{ $item->del_status }}</span></td>
                            <td>
                                <div class="dropdown"> 
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="javascript:void(0);" onclick="event.preventDefault();
                                                     document.getElementById('delete-form-{{ $item->id }}').submit();">Hapus</a>
                                    </div>
                                </div>
                                <form id="delete-form-{{ $item->id }}" action="{{ route('foodmenus.destroymodifier', $item->id) }}" method="POST" style="display: none;">
                                    @method('delete')
                                    @csrf
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $foodmodifiers->links() }}
            <!-- <ul class="pagination pagination-primary mt-4">
                
            </ul> -->
        </div>
    </div>
</div>
@stop
@section('page-script')
<script src="{{asset('assets/bundles/sparkline.bundle.js')}}"></script>
<script src="{{asset('assets/js/pages/charts/sparkline.js')}}"></script>
@stop