@extends('layout.master')
@section('title', 'List Ingredient Modifiers')
@section('parentPageTitle', 'Food')

@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
@stop

@section('content')

@if (session('status'))
<div class="alert alert-success" role="alert">
    <strong>Info</strong> {{ session('status') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
    </button>
</div>
@endif

<div class="row clearfix">
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="card mb-0 mcard_">
            <div class="header">
                <h2><strong>Food Menu</strong> Info</h2>
            </div>
            <div class="body">
                <img src="{{ asset('uploads/'.$foodmenu->photo) }}" class="rounded-circle" alt="profile-image">
                <small class="text-muted">Food Name: </small>
                <p>{{ $foodmenu->name }}</p>
                <hr>
                <small class="text-muted">Sale Price: </small>
                <p>Rp. {{ number_format($foodmenu->sale_price,0,",",".") }},-</p>
            </div>
        </div>

        <div class="card">
            <div class="header">
                <h2><strong>Add</strong> Ingredient</h2>
            </div>
            <div class="body">
                <form method="POST" action="{{ route('foodmenus.addingredient', $foodmenu->id) }}">
                    @csrf
                    <small class="text-muted">Ingredient: </small>
                    <div class="form-group">                                
                        <select name="ingredient_id" id="ingredient_id" class="form-control show-tick @error('ingredient_id') is-invalid @enderror">
                            <option value="">-- Please select --</option>
                            @foreach($ingredients as $ingredient)
                            <option value="{{ $ingredient->id }}">{{ $ingredient->name }}</option>
                            <@endforeach
                        </select>
                        @error('ingredient_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <small class="text-muted">Consumption: </small>
                    <div class="form-group">                                
                        <input type="text" id="consumption" name="consumption" class="form-control @error('consumption') is-invalid @enderror" placeholder="0" value="{{ old('consumption') }}">
                        @error('consumption')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">                                
                        <select name="unit_id" id="unit_id" class="form-control show-tick @error('unit_id') is-invalid @enderror">
                            <option value="">-- Please select --</option>
                            @foreach($units as $unit)
                            <option value="{{ $unit->id }}">{{ $unit->unit_name }}</option>
                            <@endforeach
                        </select>
                        @error('unit_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-raised btn-success btn-block btn-round waves-effect">ADD INGREDIENT</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-sm-6 col-xs-12">
        <div class="card project_list">
            <div class="header">
                <h2><strong>Modifier</strong> Ingredient</h2>
            </div>
            <!-- <a href="{{ route('modifiers.create') }}" class="btn btn-success">Add Ingredient</a> -->
            <div class="table-responsive">
                <table class="table table-hover c_table theme-color">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Consumption</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($foodmenuingredients as $item)
                        <tr>
                            <td><a href="ticket-detail.html" title="">{{ $item->ingredient->name }}</a></td>
                            <td>{{ $item->consumption }} {{ $item->unit->unit_name }}</td>
                            <td><span class="badge badge-info">{{ $item->del_status }}</span></td>
                            <td>
                                <div class="dropdown"> 
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="javascript:void(0);" onclick="event.preventDefault();
                                                     document.getElementById('delete-form-{{ $item->id }}').submit();">Hapus</a>
                                    </div>
                                </div>
                                <form id="delete-form-{{ $item->id }}" action="{{ route('foodmenus.destroyingredient', $item->id) }}" method="POST" style="display: none;">
                                    @method('delete')
                                    @csrf
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $foodmenuingredients->links() }}
            <!-- <ul class="pagination pagination-primary mt-4">
                
            </ul> -->
        </div>
    </div>
</div>
@stop
@section('page-script')
<script src="{{asset('assets/bundles/sparkline.bundle.js')}}"></script>
<script src="{{asset('assets/js/pages/charts/sparkline.js')}}"></script>
@stop