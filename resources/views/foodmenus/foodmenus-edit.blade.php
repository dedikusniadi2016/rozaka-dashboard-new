@extends('layout.master')
@section('title', 'Edit Food Menu')
@section('parentPageTitle', 'Food')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/dropify/css/dropify.min.css')}}"/>
@stop
@section('content')
<!-- Vertical Layout -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <!-- <div class="alert alert-warning" role="alert">
            <strong>Bootstrap</strong> Better check yourself, <a target="_blank" href="https://getbootstrap.com/docs/4.2/components/forms/">View More</a>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
            </button>
        </div> -->
        <div class="card">
            <div class="header">
                <h2><strong>Edit</strong> Food Menu</h2>
            </div>
            <div class="body">
                <form method="POST" action="{{ route('foodmenus.update', $foodmenu->id) }}" enctype="multipart/form-data">
                    @method('put')
                    @csrf
                    <label for="code">Code</label>
                    <div class="form-group">                                
                        <input type="text" id="code" name="code" class="form-control @error('code') is-invalid @enderror" placeholder="code" value="{{ $foodmenu->code }}">
                        @error('code')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <label for="name">Food Name</label>
                    <div class="form-group">                                
                        <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Food Name" value="{{ $foodmenu->name }}">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="row clearfix">
                        <div class="col-md-6">
                            <label for="sale_price">Sale Price</label>
                            <div class="form-group">                                
                                <input type="text" id="sale_price" name="sale_price" class="form-control @error('sale_price') is-invalid @enderror" placeholder="Rp. 0" value="{{ $foodmenu->sale_price }}">
                                @error('sale_price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="category_id">Category</label>
                            <div class="form-group">                                
                                <select name="category_id" id="category_id" class="form-control show-tick @error('category_id') is-invalid @enderror">
                                    <option value="">-- Please select --</option>
                                    @foreach($foodcategories as $foodcategory)
                                    <option value="{{ $foodcategory->id }}" @if($foodcategory->id === $foodmenu->category_id) selected @endif>{{ $foodcategory->category_name }}</option>
                                    <@endforeach
                                </select>
                                @error('category_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <label for="description">Descrtiption</label>
                    <div class="form-group">
                        <div class="form-line">
                            <textarea rows="4" class="form-control no-resize" name="description" id="description" placeholder="Please type what you want...">{{ $foodmenu->description }}</textarea>
                        </div>
                    </div>

                    <label for="photo">Foto</label>
                    <div class="form-group">
                        <input type="file" name="photo" id="photo" class="dropify">
                    </div>
                    
                    <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">UPDATE</button>
                </form>
            </div>
        </div>
    </div>
</div>

@stop
@section('page-script')
<script src="{{asset('assets/plugins/dropify/js/dropify.min.js')}}"></script>
<script src="{{asset('assets/js/pages/forms/dropify.js')}}"></script>
@stop