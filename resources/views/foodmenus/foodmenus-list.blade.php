@extends('layout.master')
@section('title', 'List Food Menus')
@section('parentPageTitle', 'Food')
@section('content')

@if (session('status'))
<div class="alert alert-success" role="alert">
    <strong>Info</strong> {{ session('status') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
    </button>
</div>
@endif

<div class="row clearfix">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="card project_list">
            <a href="{{ route('foodmenus.create') }}" class="btn btn-success">Add Food Menu</a>
            <div class="table-responsive contact">
                <table class="table table-hover c_list c_table theme-color">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Code</th>
                            <th>Category</th>
                            <th>Outlet</th>
                            <th>Sale Price</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($foodmenus as $item)
                        <tr>
                            <td>
                                <img src="{{asset('uploads/'.$item->photo)}}" class="avatar w30" alt="">
                                <p class="c_name">{{ $item->name }}</p>
                            </td>
                            <td>{{ $item->code }}</td>
                            <td>{{ $item->foodcategory->category_name }}</td>
                            <td>{{ $item->outlet->outlet_name }}</td>
                            <td>Rp. {{ number_format($item->sale_price,0,",",".") }},-</td>
                            <td><span class="badge badge-info">{{ $item->del_status }}</span></td>
                            <td>
                                <div class="dropdown"> 
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="{{ route('foodmenus.edit', $item->id) }}">Edit</a>
                                        <a class="dropdown-item" href="javascript:void(0);">Hapus</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="{{ route('foodmenus.ingredient', $item->id) }}">Ingredient</a>
                                        <a class="dropdown-item" href="{{ route('foodmenus.modifier', $item->id) }}">Modifier</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $foodmenus->links() }}
            <!-- <ul class="pagination pagination-primary mt-4">
                
            </ul> -->
        </div>
    </div>
</div>
@stop
@section('page-script')
<script src="{{asset('assets/bundles/sparkline.bundle.js')}}"></script>
<script src="{{asset('assets/js/pages/charts/sparkline.js')}}"></script>
@stop