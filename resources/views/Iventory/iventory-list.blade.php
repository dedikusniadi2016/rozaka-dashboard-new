@extends('layout.master')
@section('title', 'List Iventory')
@section('parentPageTitle', 'Iventory')
@section('content')

@if (session('status'))
<div class="alert alert-success" role="alert">
    <strong>Info</strong> {{ session('status') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
    </button>
</div>
@endif

<div class="row clearfix">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="card project_list">
            <a href="{{ route('iventory.create') }}" class="btn btn-success">Add Inventory Adjustment</a>
            <div class="table-responsive contact">
                <table class="table table-hover c_list c_table theme-color">
                    <thead>
                        <tr>
                            <th>No Referece</th>
                            <th>Date</th>
                            <th>Employee</th>
                            <th>Outlet</th>
                            <th>Note</th>
                            <th>status </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($Iventorys as $item)
                        <tr>
                            <td>{{ $item->reference_no }}</td>
                            <td>{{ $item->date }}</td>
                            <td>{{ $item->employee->name }}</td>
                            <td>{{ $item->outlet->outlet_name }}</td>
                            <td>{{ $item->note }}</td>
                            <td><span class="badge badge-info">{{ $item->del_status }}</span></td>
                            <td>
                                <div class="dropdown"> 
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="{{ route('iventory.edit', $item->id) }}">Edit</a>
                                        <a class="dropdown-item" href="{{ route('iventory.ingredient', $item->id) }}">Ingredient</a>
                                        <a class="dropdown-item" href="javascript:void(0);">Hapus</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $Iventorys->links() }}
            <!-- <ul class="pagination pagination-primary mt-4">
                
            </ul> -->
        </div>
    </div>
</div>
@stop
@section('page-script')
<script src="{{asset('assets/bundles/sparkline.bundle.js')}}"></script>
<script src="{{asset('assets/js/pages/charts/sparkline.js')}}"></script>
@stop