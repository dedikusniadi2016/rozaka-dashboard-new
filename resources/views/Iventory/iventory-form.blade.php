@extends('layout.master')
@section('title', 'Add Iventory')
@section('parentPageTitle', 'Iventory')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
@stop
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="header">
                <h2><strong>Add</strong> Iventory </h2>
            </div>
            <div class="body">
                <form method="POST" action="{{ route('iventory.store') }}">
                    @csrf
                    <label for="reference_no">No Reference</label>
                    <div class="form-group">                                
                        <input type="text" id="reference_no" name="reference_no" class="form-control @error('reference_no') is-invalid @enderror" placeholder="No Reference" value="{{ old('reference_no') }}">
                        @error('reference_no')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                      <div class="row clearfix">
                        <div class="col-sm-6">
                            <label for="date">Date</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="zmdi zmdi-calendar"></i></span>
                                </div>                             
                                <input type="text" id="datepicker" name="date" class="form-control datepicker @error('date') is-invalid @enderror" placeholder="date" value="{{ old('date') }}">
                                @error('date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>  
                        </div>
                    </div>
                   
                    <label for="note">Note</label>
                    <div class="form-group">     
                     <div class="form-line">      
                        <textarea rows="4" class="form-control no-resize" name="note" id="note" placeholder="Please type what you want...">{{ old('note') }}</textarea>
                    
                        @error('note')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                          </span>
                            @enderror

                    </div>
                </div>
                    <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</div>



@stop
@section('page-script')

<script src="{{asset('assets/plugins/momentjs/moment.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
<script src="{{asset('assets/js/pages/forms/basic-form-elements.js')}}"></script>
@stop