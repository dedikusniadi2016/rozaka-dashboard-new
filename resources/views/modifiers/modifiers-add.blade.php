@extends('layout.master')
@section('title', 'Add Modifier')
@section('parentPageTitle', 'Food')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
@stop
@section('content')
<!-- Vertical Layout -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <!-- <div class="alert alert-warning" role="alert">
            <strong>Bootstrap</strong> Better check yourself, <a target="_blank" href="https://getbootstrap.com/docs/4.2/components/forms/">View More</a>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
            </button>
        </div> -->
        <div class="card">
            <div class="header">
                <h2><strong>Add</strong> Modifier</h2>
            </div>
            <div class="body">
                <form method="POST" action="{{ route('modifiers.store') }}">
                    @csrf
                    <label for="name">Modifier Name</label>
                    <div class="form-group">                                
                        <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Category Name" value="{{ old('name') }}">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="row clearfix">
                      <div class="col-sm-6">
                        <label for="price">Price</label>
                        <div class="form-group">                                
                            <input type="text" id="price" name="price" class="form-control @error('price') is-invalid @enderror" placeholder="Rp. 0" value="{{ old('price') }}">
                            @error('price')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                      
                    </div>

                    <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</div>

@stop