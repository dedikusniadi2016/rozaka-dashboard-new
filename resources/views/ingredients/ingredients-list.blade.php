@extends('layout.master')
@section('title', 'List Ingredients')
@section('parentPageTitle', 'Ingredients')
@section('content')

@if (session('status'))
<div class="alert alert-success" role="alert">
    <strong>Info</strong> {{ session('status') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
    </button>
</div>
@endif

<div class="row clearfix">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="card project_list">


        <div class="row">
            <div class="col-sm-8">
                <a href="{{ route('ingredients.create') }}" class="btn btn-success">Add Ingredient</a>
            </div>
          <div class="col-sm-4">
          <form action="{{ route('ingredients.search') }}" method="get">
           <input type="text" name="q" class="form-control" placeholder="search">
                <button type="submit" class="btn btn-primary">Search </button>
           </form>
                </div>
            </div>

            <br>

            <div class="table-responsive">
                <table class="table table-hover c_table theme-color">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Purchase Price</th>
                            <th>Category</th>
                            <th>A. Qty</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ingredients as $item)
                        <tr>
                            <td>{{ $item->code }}</td>
                            <td><a href="ticket-detail.html" title="">{{ $item->name }}</a></td>
                            <td>Rp. {{ number_format($item->purchase_price,0,",",".") }},-</td>
                            <td>{{ $item->ingcat->category_name }}</td>
                            <td>{{ $item->alert_quantity }} {{ $item->unit->unit_name }}</td>
                            <td><span class="badge badge-info">{{ $item->del_status }}</span></td>
                            <td>
                                <div class="dropdown"> 
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="{{ route('ingredients.edit', $item->id) }}">Edit</a>
                                        <a class="dropdown-item" href="javascript:void(0);">Hapus</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $ingredients->links() }}
            <!-- <ul class="pagination pagination-primary mt-4">
                
            </ul> -->
        </div>
    </div>
</div>
@stop
@section('page-script')
<script src="{{asset('assets/bundles/sparkline.bundle.js')}}"></script>
<script src="{{asset('assets/js/pages/charts/sparkline.js')}}"></script>
@stop