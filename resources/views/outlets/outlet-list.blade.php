@extends('layout.master')
@section('title', 'List Outlets')
@section('parentPageTitle', 'Outlets')
@section('content')

@if (session('status'))
<div class="alert alert-success" role="alert">
    <strong>Info</strong> {{ session('status') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
    </button>
</div>
@endif

<div class="row clearfix">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="card project_list">
            <a href="{{ route('outlets.create') }}" class="btn btn-success">Add Outlet</a>
            <div class="table-responsive">
                <table class="table table-hover c_table theme-color">
                    <thead>
                        <tr>
                            <th style="width:50px;">Outlet</th>
                            <th></th>
                            <th>Phone</th>
                            <th>Status</th>
                            <th>Due Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($outlets as $item) : ?>
                        <tr>
                            <td>
                                <i class="zmdi zmdi-store"></i>
                            </td>
                            <td>
                                <a class="single-user-name" href="javascript:void(0);">{{$item->outlet_name}}</a><br>
                                <small>{{$item->address}}</small>
                            </td>
                            <td>
                                <strong>{{$item->phone}}</strong>
                            </td>
                            <td><span class="badge badge-info">{{$item->del_status}}</span></td>
                            <td>{{$item->created_at->diffForHumans()}}</td>
                            <td>
                                <div class="dropdown"> 
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="{{ route('outlets.edit', $item->id) }}">Edit</a>
                                        <a class="dropdown-item" href="javascript:void(0);">Hapus</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        
                    </tbody>
                </table>
            </div>
            
            {{ $outlets->links() }}
            <!-- <ul class="pagination pagination-primary mt-4">
                
                
            </ul> -->
        </div>
    </div>
</div>
@stop