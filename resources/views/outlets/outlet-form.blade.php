@extends('layout.master')
@section('title', 'Form Outlet')
@section('parentPageTitle', 'Outlets')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
@stop
@section('content')
<!-- Vertical Layout -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <!-- <div class="alert alert-warning" role="alert">
            <strong>Bootstrap</strong> Better check yourself, <a target="_blank" href="https://getbootstrap.com/docs/4.2/components/forms/">View More</a>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
            </button>
        </div> -->
        <div class="card">
            <div class="header">
                <h2><strong>Form</strong> Outlet</h2>
            </div>
            <div class="body">
                <form method="POST" action="{{ route('outlets.store') }}">
                    @csrf
                    <div class="row clearfix">
                      <div class="col-sm-8">
                        <label for="outlet_name">Outlet Name</label>
                        <div class="form-group">                                
                            <input type="text" id="outlet_name" name="outlet_name" class="form-control @error('outlet_name') is-invalid @enderror" placeholder="Outlet Name" value="{{ old('outlet_name') }}">
                            @error('outlet_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <label for="outlet_code">Outlet Code</label>
                        <div class="form-group">                                
                            <input type="text" id="outlet_code" name="outlet_code" class="form-control @error('outlet_code') is-invalid @enderror" placeholder="Outlet Code" value="{{ old('outlet_code') }}">
                            @error('outlet_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                    </div>
                    
                    <label for="address">Address</label>
                    <div class="form-group">                                
                        <input type="text" id="address" name="address" class="form-control @error('address') is-invalid @enderror" placeholder="Address" value="{{ old('address') }}">
                        @error('address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="row clearfix">
                      <div class="col-sm-6">
                        <label for="phone">Telephone</label>
                        <div class="form-group">                                
                            <input type="text" id="phone" name="phone" class="form-control @error('phone') is-invalid @enderror" placeholder="Telephone" value="{{ old('phone') }}">
                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <label for="starting_date">Starting</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="zmdi zmdi-calendar"></i></span>
                            </div>
                            <input type="text" id="starting_date" name="starting_date" class="form-control datepicker @error('starting_date') is-invalid @enderror" placeholder="Please choose date..." value="{{ old('starting_date') }}">
                            @error('starting_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                    </div>

                    <label for="invoice_print">Invoice Print</label>
                    <div class="form-group">
                        <div class="radio inlineblock m-r-20">
                            <input type="radio" name="invoice_print" id="yes" class="with-gap" value="Yes">
                            <label for="yes">Yes</label>
                        </div>                                
                        <div class="radio inlineblock">
                            <input type="radio" name="invoice_print" id="no" class="with-gap" value="No">
                            <label for="no">No</label>
                        </div>

                        @error('invoice_print')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <label for="invoice_footer">Invoice Footer</label>
                    <div class="form-group">                                
                        <input type="text" id="invoice_footer" name="invoice_footer" class="form-control" placeholder="Address" value="{{ old('invoice_footer') }}">

                        @error('invoice_footer')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</div>

@stop
@section('page-script')
<script src="{{asset('assets/plugins/momentjs/moment.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
<script src="{{asset('assets/js/pages/forms/basic-form-elements.js')}}"></script>
@stop