@extends('layout.master')
@section('title', 'Add Employee')
@section('parentPageTitle', 'Employees')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
@stop
@section('content')
<!-- Vertical Layout -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <!-- <div class="alert alert-warning" role="alert">
            <strong>Bootstrap</strong> Better check yourself, <a target="_blank" href="https://getbootstrap.com/docs/4.2/components/forms/">View More</a>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
            </button>
        </div> -->
        <div class="card">
            <div class="header">
                <h2><strong>Add</strong> Employee</h2>
            </div>
            <div class="body">
                <form method="POST" action="{{ route('employees.store') }}">
                    @csrf
                    <label for="name">Employee Name</label>
                    <div class="form-group">                                
                        <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Employee Name" value="{{ old('name') }}">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="row clearfix">
                      <div class="col-sm-7">
                        <label for="phone">Phone</label>
                        <div class="form-group">                                
                            <input type="text" id="phone" name="phone" class="form-control @error('phone') is-invalid @enderror" placeholder="Phone" value="{{ old('phone') }}">
                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                      <div class="col-sm-5">
                        <label for="designation">Designation</label>
                        <div class="form-group">                                
                            <select name="designation" id="designation" class="form-control show-tick @error('designation') is-invalid @enderror">
                                <option value="">-- Please select --</option>
                                <option value="Admin">Admin</option>
                                <option value="Casier">Casier</option>
                                <option value="Waiter">Waiter</option>
                                <option value="Checker">Checker</option>
                                <option value="Kitchen">Kitchen</option>
                            </select>
                            @error('designation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                    </div>
                    
                    <div class="row clearfix">
                      <div class="col-sm-6">
                        <label for="outlet_id">Outlet</label>
                        <div class="form-group">                                
                            <select name="outlet_id" id="outlet_id" class="form-control show-tick @error('outlet_id') is-invalid @enderror">
                                <option value="">-- Please select --</option>
                                @foreach($outlets as $outlet)
                                <option value="{{ $outlet->id }}">{{ $outlet->outlet_name }}</option>
                                <@endforeach
                            </select>
                            @error('outlet_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                      
                    </div>

                    <div class="row clearfix">
                      <div class="col-sm-6">
                        <label for="email">Email</label>
                        <div class="form-group">                                
                            <input type="text" id="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email') }}">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <label for="password">Password</label>
                        <div class="form-group">                                
                            <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" value="{{ old('password') }}">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                      
                    </div>

                    

                    <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</div>

@stop
@section('page-script')
<script src="{{asset('assets/plugins/momentjs/moment.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
<script src="{{asset('assets/js/pages/forms/basic-form-elements.js')}}"></script>
@stop