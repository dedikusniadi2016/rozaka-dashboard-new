@extends('layout.master')
@section('title', 'List Employees')
@section('parentPageTitle', 'Employees')
@section('content')

@if (session('status'))
<div class="alert alert-success" role="alert">
    <strong>Info</strong> {{ session('status') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
    </button>
</div>
@endif

<div class="row clearfix">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="card project_list">

            <div class="row">
                <div class="col-sm-8">
                    <a href="{{ route('employees.create') }}" class="btn btn-success">Add Employee</a>
                </div>

                <div class="col-sm-4">
                <form action="{{ route('employees.search') }}" method="get">
           <input type="text" name="q" class="form-control" placeholder="search">
                <button type="submit" class="btn btn-primary">Search </button>
           </form>

                </div>
            </div>

            <br>
                   
            <div class="table-responsive contact">
                <table class="table table-hover c_list c_table theme-color">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Designation</th>
                            <th>Phone</th>
                            <th>Outlet</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($employees as $item)
                        <tr>
                            <td>
                                <img src="{{asset('assets/images/xs/avatar1.jpg')}}" class="avatar w30" alt="">
                                <p class="c_name">{{ $item->name }}</p>
                            </td>
                            <td>{{ $item->user->email }}</td>
                            <td>{{ $item->user->level }}</td>
                            <td>{{ $item->phone }}</td>
                            <td>{{ $item->outlet->outlet_name }}</td>
                            <td><span class="badge badge-info">{{ $item->del_status }}</span></td>
                            <td>
                                <div class="dropdown"> 
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="{{ route('employees.edit', $item->id) }}">Edit</a>
                                        <a class="dropdown-item" href="javascript:void(0);">Hapus</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>


            {{ $employees->links() }}
            <!-- <ul class="pagination pagination-primary mt-4">
                
            </ul> -->
        </div>
    </div>
</div>
@stop
@section('page-script')
<script src="{{asset('assets/bundles/sparkline.bundle.js')}}"></script>
<script src="{{asset('assets/js/pages/charts/sparkline.js')}}"></script>
@stop