@extends('layout.master')
@section('title', 'Add Wastes')
@section('parentPageTitle', 'Wastes')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
@stop
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="header">
                <h2><strong>Add</strong> Wastes </h2>
            </div>
            <div class="body">
                <form method="POST" action="{{ route('wastes.store') }}">
                    @csrf
                    <label for="reference_no">No Reference</label>
                    <div class="form-group">                                
                        <input type="text" id="reference_no" name="reference_no" class="form-control @error('reference_no') is-invalid @enderror" placeholder="No Reference" value="{{ old('reference_no') }}">
                        @error('reference_no')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>


                    <label for="food_menu_id"> Food Menu: </label>
                    <div class="form-group">                                
                        <select name="food_menu_id" id="food_menu_id" class="form-control show-tick @error('food_menu_id') is-invalid @enderror">
                            <option value="">-- Please select --</option>
                            @foreach($foodmenu as $foodmenu)
                            <option value="{{ $foodmenu->id }}">{{ $foodmenu->name }}</option>
                            <@endforeach
                        </select>
                        @error('food_menu_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>



                      <div class="row clearfix">
                        <div class="col-sm-12">
                            <label for="date">Date</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="zmdi zmdi-calendar"></i></span>
                                </div>                             
                                <input type="text" id="datepicker" name="date" class="form-control datepicker @error('date') is-invalid @enderror" placeholder="date" value="{{ old('date') }}">
                                @error('date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>  
                        </div>
                    </div>
                    <br>

                   
                    <label for="total_loss">Total Los</label>
                    <div class="form-group">                                
                        <input type="text" id="total_loss" name="total_loss" class="form-control @error('total_loss') is-invalid @enderror" placeholder="Total Los" value="{{ old('total_loss') }}">
                        @error('total_loss')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                   
                    <label for="note">Note</label>
                    <div class="form-group">     
                     <div class="form-line">      
                        <textarea rows="4" class="form-control no-resize" name="note" id="note" placeholder="Please type what you want...">{{ old('note') }}</textarea>
                    
                        @error('note')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                          </span>
                            @enderror
                    </div>
                </div>

                    <label for="food_menu_waste_qty"> Food Menu Wastes Quantity </label>
                    <div class="form-group">                                
                        <input type="text" id="food_menu_waste_qty" name="food_menu_waste_qty" class="form-control @error('food_menu_waste_qty') is-invalid @enderror" placeholder=" Food Menu Wastes Quantity" value="{{ old('food_menu_waste_qty') }}">
                        @error('food_menu_waste_qty')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</div>



@stop
@section('page-script')

<script src="{{asset('assets/plugins/momentjs/moment.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
<script src="{{asset('assets/js/pages/forms/basic-form-elements.js')}}"></script>
@stop