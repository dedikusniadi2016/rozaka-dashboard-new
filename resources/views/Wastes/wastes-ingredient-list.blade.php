@extends('layout.master')
@section('title', 'List iventory Ingredient')
@section('parentPageTitle', 'iventory')

@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
@stop

@section('content')

@if (session('status'))
<div class="alert alert-success" role="alert">
    <strong>Info</strong> {{ session('status') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
    </button>
</div>
@endif

<div class="row clearfix">
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="card mb-0">
            <div class="header">
                <h2><strong>inventory</strong> Info</h2>
            </div>
            <div class="body">
                <small class="text-muted"> No Reference: </small>
                <p>{{ $wastes->reference_no }}</p>
                <hr>
                <small class="text-muted">Date: </small>
                <p> {{ $wastes->date }} </p>
            </div>
        </div>

        <div class="card">
            <div class="header">
                <h2><strong>Add</strong> Ingredient</h2>
            </div>
            <div class="body">
                <form method="POST" action="{{ route('wastes.addingredient', $wastes->id) }}">
             @csrf
                    <small class="text-muted">Ingredient: </small>
                    <div class="form-group">                                
                        <select name="ingredient_id" id="ingredient_id" class="form-control show-tick @error('ingredient_id') is-invalid @enderror">
                            <option value="">-- Please select --</option>
                            @foreach($ingredients as $ingredient)
                            <option value="{{ $ingredient->id }}">{{ $ingredient->name }}</option>
                            <@endforeach
                        </select>
                        @error('ingredient_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <small class="text-muted"> Waste Amount: </small>
                    <div class="form-group">                                
                        <input type="text" id="waste_amount" name="waste_amount" class="form-control @error('waste_amount') is-invalid @enderror" placeholder="0" value="{{ old('waste_amount') }}">
                        @error('waste_amount')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>


                       <small class="text-muted"> Last Purchase Price : </small>
                    <div class="form-group">                                
                        <input type="text" id="last_purchase_price" name="last_purchase_price" class="form-control @error('last_purchase_price') is-invalid @enderror" placeholder="0" value="{{ old('last_purchase_price') }}">
                        @error('last_purchase_price')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>


                    <small class="text-muted"> Loss Amount: </small>
                    <div class="form-group">                                
                        <input type="text" id="loss_amount" name="loss_amount" class="form-control @error('loss_amount') is-invalid @enderror" placeholder="0" value="{{ old('loss_amount') }}">
                        @error('loss_amount')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                <button type="submit" class="btn btn-raised btn-success btn-block btn-round waves-effect">ADD INGREDIENT</button>

                </form>
            </div>
        </div>
    </div>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <div class="card project_list">
            <div class="header">
                <h2><strong>Modifier</strong> Ingredient</h2>
            </div>
            <div class="table-responsive">
                <table class="table table-hover c_table theme-color">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Waste Amount</th>
                            <th>Last Purchase Price </th>
                            <th>Loss Amount </th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($wastesingredients as $item)
                        <tr>
                            <td><a href="ticket-detail.html" title="">{{ $item->ingredient->name }}</a></td>
                            <td>{{ $item->waste_amount }}  </td>
                            <td> {{ $item->last_purchase_price}} </td>
                            <td> {{ $item->loss_amount }} </td>
                            <td><span class="badge badge-info">{{ $item->del_status }}</span></td>
                            <td>
                                <div class="dropdown"> 
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                      <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="javascript:void(0);" onclick="event.preventDefault();
                                                     document.getElementById('delete-form-{{ $item->id }}').submit();">Hapus</a>
                                    </div>
                                </div>
                                <form id="delete-form-{{ $item->id }}" action="{{ route('wastes.destroyingredient', $item->id) }}" method="POST" style="display: none;">
                                    @method('delete')
                                    @csrf
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $wastesingredients->links() }}
        </div>
    </div>
</div>
@stop
@section('page-script')
<script src="{{asset('assets/bundles/sparkline.bundle.js')}}"></script>
<script src="{{asset('assets/js/pages/charts/sparkline.js')}}"></script>
@stop