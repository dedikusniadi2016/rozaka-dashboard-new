<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        // 'App\Outlet' => 'App\Policies\OutletPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('employees', function ($user) {
            return $user->level === 'Owner' || $user->level === 'Admin';
        });

        Gate::define('outlets', function ($user) {
            return $user->level === 'Owner';
        });

        Gate::define('ingcats', function ($user) {
            return $user->level === 'Admin';
        });

        Gate::define('ingredients', function ($user) {
            return $user->level === 'Admin';
        });

        Gate::define('maincategory', function ($user) {
            return $user->level === 'Admin';
        });

        Gate::define('foodcategory', function ($user) {
            return $user->level === 'Admin';
        });

        Gate::define('customers', function ($user) {
            return $user->level === 'Admin';
        });

        Gate::define('modifiers', function ($user) {
            return $user->level === 'Admin';
        });

        Gate::define('foodmenus', function ($user) {
            return $user->level === 'Admin';
        });

        Gate::define('foodpromo', function ($user) {
            return $user->level === 'Admin';
        });
    }
}
