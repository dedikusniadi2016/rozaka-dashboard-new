<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Ingcat;
use App\Unit;
use App\ModifierIngredient;
use App\FoodMenuIngredient;
use App\WastesIngredient;
use App\Adjusment;

class Ingredient extends Model
{
    protected $table = "tbl_ingredients";
    protected $primaryKey = "id";
    public $incrementing = false;

    protected $fillable = [
        'id', 'code', 'name', 'category_id', 'purchase_price', 'alert_quantity', 'unit_id', 'user_id', 'del_status'
    ];

    protected $hidden = [
        'user_id', 'del_status', 
    ];

    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function ingcat()
    {
        return $this->belongsTo(Ingcat::class, 'category_id');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'unit_id');
    }

    public function modifieringredient()
    {
        return $this->hasMany(ModifierIngredient::class, 'ingredient_id');
    }

    public function foodmenuingredients()
    {
        return $this->hasMany(FoodMenuIngredient::class, 'ingredient_id');
    }

    public function Adjusment() 
    {
        return $this->hasMany(Adjusment::class, 'ingredient_id');
    }

     public function WastesIngredient() 
    {
        return $this->hasMany(WastesIngredient::class, 'ingredient_id');
    }
}
