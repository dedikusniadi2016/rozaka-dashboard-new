<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Outlet;
use App\Inventory;

class Employee extends Model
{
    protected $table = 'tbl_employees';
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $fillable = [
        'id', 'name', 'designation', 'phone', 'description', 'user_id', 'outlet_id', 'user_login_id', 'del_status',
    ];

    protected $hidden = [
        'user_id', 'outlet_id', 
    ];

    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_login_id');
    }

    public function inventories()
    {
        return $this->hasMany(Inventory::class, 'employee_id');
    }

    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id');
    }
}
