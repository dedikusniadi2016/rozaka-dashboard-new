<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Modifier;
use App\Ingredient;
use App\Outlet;
use App\Unit;

class ModifierIngredient extends Model
{
    protected $table = "tbl_modifier_ingredients";
    protected $primaryKey = "id";
    public $incrementing = false;

    protected $fillable = [
        'id', 'ingredient_id', 'consumption', 'modifier_id', 'user_id', 'outlet_id', 'unit_id', 'del_status', 
    ];

    protected $hidden = [
        'outlet_id', 'user_id', 
    ];

    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function modifier()
    {
        return $this->belongsTo(Modifier::class, 'modifier_id');
    }

    public function ingredient()
    {
        return $this->belongsTo(Ingredient::class, 'ingredient_id');
    }

    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'unit_id');
    }
}
