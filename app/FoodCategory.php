<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Outlet;
use App\FoodmCategory;
use App\FoodMenu;

class FoodCategory extends Model
{
    protected $table = "tbl_food_menu_categories";
    protected $primaryKey = "id";
    public $incrementing = false;

    protected $fillable = [
        'id', 'category_name', 'description', 'ma_ca_id', 'user_id', 'outlet_id', 'del_status', 
    ];

    protected $hidden = [
        'ma_ca_id', 'user_id', 'outlet_id', 'del_status', 
    ];

    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id');
    }

    public function foodmcategory()
    {
        return $this->belongsTo(FoodmCategory::class, 'ma_ca_id');
    }

    public function foodmenus()
    {
        return $this->hasMany(FoodMenu::class, 'category_id');
    }
}
