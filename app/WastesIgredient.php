<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Outlet;
use App\FoodMenu;
use App\Unit;
use App\Wastes;

class WastesIgredient extends Model {

 protected $table = 'tbl_waste_ingredients';

    protected $primaryKey = "id";
    public $incrementing = false;

    protected $fillable = [
    'id', 'ingredient_id', 'waste_amount','last_purchase_price', 'loss_amount', 'waste_id', 'outlet_id', 'user_id', 'del_status'
    ];

    protected $hidden = [
        'user_id', 'del_status', 
    ];

    
    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }


       public function ingredient()
    {
        return $this->belongsTo(Ingredient::class, 'ingredient_id');
    }

    public function wastes()
    {
        return $this->belongsTo(Wastes::class, 'waste_id');
    }
}