<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    protected $table = 'tbl_tables';
    protected $primaryKey = "id";
    public $incrementing = false;

    protected $fillable = [
        'id', 'name', 'sit_capacity', 'position', 'description' , 'user_id', 'outlet_id', 'del_status',
    ];

    protected $hidden = [
        'user_id', 'del_status', 
    ];

    
    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

}
