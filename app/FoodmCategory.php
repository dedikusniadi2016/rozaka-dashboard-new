<?php

namespace App;
use App\Outlet;
use App\FoodCategory;

use Illuminate\Database\Eloquent\Model;

class FoodmCategory extends Model
{
    protected $table = "tbl_food_main_menu_category";
    protected $primaryKey = "id";
    public $incrementing = false;

    protected $fillable = [
        'id', 'ma_ca_name', 'description', 'user_id', 'outlet_id', 'del_status', 
    ];

    protected $hidden = [
        'user_id', 'outlet_id', 
    ];

    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id');
    }

    public function categories()
    {
        return $this->hasMany(FoodCategory::class, 'ma_ca_id');
    }
}
