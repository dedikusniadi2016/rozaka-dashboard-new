<?php

namespace App\Policies;

use App\User;
use App\FoodModifier;
use Illuminate\Auth\Access\HandlesAuthorization;

class FoodModifierPolicy
{
    use HandlesAuthorization;

    public function delete(User $user, FoodModifier $foodmodifier)
    {
        return $user->employee->user_id === $foodmodifier->user_id && $user->employee->outlet_id === $foodmodifier->outlet_id;
    }
}
