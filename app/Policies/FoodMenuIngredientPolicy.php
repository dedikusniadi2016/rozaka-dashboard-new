<?php

namespace App\Policies;

use App\User;
use App\FoodMenuIngredient;
use Illuminate\Auth\Access\HandlesAuthorization;

class FoodMenuIngredientPolicy
{
    use HandlesAuthorization;

    public function delete(User $user, FoodMenuIngredient $foodmenuingredient)
    {
        return $user->employee->user_id === $foodmenuingredient->user_id && $user->employee->outlet_id === $foodmenuingredient->outlet_id;
    }
}
