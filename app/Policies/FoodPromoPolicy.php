<?php

namespace App\Policies;

use App\User;
use App\FoodPromo;
use Illuminate\Auth\Access\HandlesAuthorization;

class FoodPromoPolicy
{
    use HandlesAuthorization;

    public function update(User $user, FoodPromo $foodpromo)
    {
        return $user->employee->user_id === $foodpromo->user_id && $user->employee->outlet_id === $foodpromo->outlet_id;
    }
}
