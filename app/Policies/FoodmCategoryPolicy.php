<?php

namespace App\Policies;

use App\User;
use App\FoodmCategory;
use Illuminate\Auth\Access\HandlesAuthorization;

class FoodmCategoryPolicy
{
    use HandlesAuthorization;

    public function update(User $user, FoodmCategory $foodmcategory)
    {
        return $user->employee->user_id === $foodmcategory->user_id && $user->employee->outlet_id === $foodmcategory->outlet_id;
    }
}
