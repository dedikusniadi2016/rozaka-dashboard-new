<?php

namespace App\Policies;

use App\User;
use App\ModifierIngredient;
use Illuminate\Auth\Access\HandlesAuthorization;

class ModifierIngredientPolicy
{
    use HandlesAuthorization;

    public function update(User $user, ModifierIngredient $modifieringredient)
    {
        return $user->employee->user_id === $modifieringredient->user_id && $user->employee->outlet_id === $modifieringredient->outlet_id;
    }
    
    public function delete(User $user, ModifierIngredient $modifieringredient)
    {
        return $user->employee->user_id === $modifieringredient->user_id && $user->employee->outlet_id === $modifieringredient->outlet_id;
    }
}
