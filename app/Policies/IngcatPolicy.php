<?php

namespace App\Policies;

use App\User;
use App\Ingcat;
use Illuminate\Auth\Access\HandlesAuthorization;

class IngcatPolicy
{
    use HandlesAuthorization;

    public function update(User $user, Ingcat $ingcat)
    {
        return $user->employee->user_id === $ingcat->user_id;
    }
}
