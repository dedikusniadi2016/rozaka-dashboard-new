<?php

namespace App\Policies;

use App\User;
use App\Ingredient;
use Illuminate\Auth\Access\HandlesAuthorization;

class IngredientPolicy
{
    use HandlesAuthorization;

    public function update(User $user, Ingredient $ingredient)
    {
        return $user->employee->user_id === $ingredient->user_id;
    }
}
