<?php

namespace App\Policies;

use App\User;
use App\FoodMenu;
use Illuminate\Auth\Access\HandlesAuthorization;

class FoodMenuPolicy
{
    use HandlesAuthorization;

    public function update(User $user, FoodMenu $foodmenu)
    {
        return $user->employee->user_id === $foodmenu->user_id && $user->employee->outlet_id === $foodmenu->outlet_id;
    }
}
