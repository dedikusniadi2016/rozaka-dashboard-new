<?php

namespace App\Policies;

use App\User;
use App\Modifier;
use Illuminate\Auth\Access\HandlesAuthorization;

class ModifierPolicy
{
    use HandlesAuthorization;

    public function update(User $user, Modifier $modifier)
    {
        return $user->employee->user_id === $modifier->user_id && $user->employee->outlet_id === $modifier->outlet_id;
    }
}
