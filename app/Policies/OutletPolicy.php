<?php

namespace App\Policies;

use App\User;
use App\Outlet;
use Illuminate\Auth\Access\HandlesAuthorization;

class OutletPolicy
{
    use HandlesAuthorization;

    public function update(User $user, Outlet $outlet)
    {
        return $user->owner->id === $outlet->user_id;
    }
}
