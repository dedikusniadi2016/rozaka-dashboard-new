<?php

namespace App\Policies;

use App\User;
use App\FoodCategory;
use Illuminate\Auth\Access\HandlesAuthorization;

class FoodCategoryPolicy
{
    use HandlesAuthorization;

    public function update(User $user, FoodCategory $foodcategory)
    {
        return $user->employee->user_id === $foodcategory->user_id && $user->employee->outlet_id === $foodcategory->outlet_id;
    }
}
