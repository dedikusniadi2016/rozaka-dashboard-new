<?php

namespace App\Policies;

use App\User;
use App\Employee;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeePolicy
{
    use HandlesAuthorization;

    public function update(User $user, Employee $employee)
    {
        return $user->owner->id === $employee->user_id;
    }

    public function updatebyadmin(User $user, Employee $employee)
    {
        return $user->employee->user_id === $employee->user_id;
    }
}
