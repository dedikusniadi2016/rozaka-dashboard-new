<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Ingredient;

class Ingcat extends Model
{
    protected $table = 'tbl_ingredient_categories';
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $fillable = [
        'id', 'category_name', 'description', 'user_id', 'del_status',
    ];

    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function ingredients()
    {
        return $this->hasMany(Ingredients::class, 'category_id');
    }
}
