<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Modifier;

class FoodModifier extends Model
{
    protected $table = "tbl_food_menus_modifiers";
    protected $primaryKey = "id";
    public $incrementing = false;

    protected $fillable = [
        'id', 'modifier_id', 'food_menu_id', 'user_id', 'outlet_id', 'del_status', 
    ];

    protected $hidden = [
        'outlet_id', 'user_id', 
    ];

    public function modifier()
    {
        return $this->belongsTo(Modifier::class, 'modifier_id');
    }
}
