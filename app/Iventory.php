<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Outlet;
use App\Adjusment;
use App\Employee;

class Iventory extends Model
{
    protected $table = "tbl_inventory_adjustment";
    protected $primaryKey = "id";
    public $incrementing = false;

    protected $fillable = [
        'id', 'reference_no', 'date','note','employee_id', 'user_id', 'outlet_id', 'del_status', 
    ];

    protected $hidden = [
        'outlet_id', 'user_id',
    ];

    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    public function inventory() 
    {
        return $this->belongsTo(Adjusment::class, 'inventory_adjustment_id');
    }
}
