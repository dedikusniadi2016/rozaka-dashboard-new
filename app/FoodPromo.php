<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\FoodMenu;
use App\Outlet;

class FoodPromo extends Model
{
    protected $table = 'tbl_food_menus_promo';
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $fillable = [
        'id', 'food_menu_id', 'user_id', 'outlet_id', 'promo_name', 'promo_type', 'promo_value', 'date_start', 'date_done', 'description', 'del_status'
    ];

    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function foodmenu()
    {
        return $this->belongsTo(FoodMenu::class, 'food_menu_id');
    }

    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id');
    }
}
