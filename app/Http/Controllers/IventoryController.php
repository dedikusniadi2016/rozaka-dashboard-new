<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Illuminate\Http\Request;
use App\Iventory;
use App\Adjusment;
use App\Ingredient;
use Auth;



class IventoryController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Iventorys = Iventory::where('user_id', Auth::user()->employee->user_id)->paginate(10);
              
        return view('Iventory.iventory-list', ['Iventorys' => $Iventorys]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Iventory.iventory-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'reference_no' => 'required', 
            'date' => 'required', 
            'note' => 'required', 
        ]);

        $user_id = Auth::user()->employee->user_id;
        $outlet_id = Auth::user()->employee->outlet_id;
        $employee_id = Auth::user()->employee->id;
        
        Iventory::create([
            'id' => Str::uuid(),
            'reference_no' => $request->reference_no, 
            'date' => $request->date, 
            'note' => $request->note, 
            'employee_id' => $employee_id, 
            'user_id' => $user_id, 
            'outlet_id' => $outlet_id, 
            'del_status' => 'Live'
        ]);

        return redirect()->route('iventory')->with('status', 'Iventory success is created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Iventory $iventory)
    {
        return view('Iventory.iventory-edit', compact('iventory'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Iventory $iventory)
    {
        $iventory->reference_no = $request->get('reference_no', $iventory->reference_no);
        $iventory->date = $request->get('date', $iventory->date);
        $iventory->note = $request->get('note', $iventory->note);
        $iventory->save();
        return redirect('iventory')->with('status', 'Iventory success is updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function destroyingredient( $id, Adjusment  $Adjusment)
    {
        $Adjusment = Adjusment::find($id);
        $Adjusment->delete();
        return redirect()->route('iventory')->with('status', 'Ingredeint success is added.');
    }



      public function ingredient(Iventory $Iventory, Adjusment $Adjusment)
    {
        $adjusmentingredients = Adjusment:: 
        where('inventory_adjustment_id', $Iventory->id)->paginate(10);

        $ingredients = Ingredient::where('user_id', Auth::user()->employee->user_id)->get();
       
        return view('Iventory.iventory-ingredient-list', [
            'adjusmentingredients' => $adjusmentingredients, 
            'ingredients' => $ingredients,
        ])->with(compact('Iventory'));
    }

      public function addingredient(Request $request, Iventory $iventory)
    {

        $request->validate([
            'ingredient_id' => 'required', 
            'consumption_amount' => 'required',
            'consumption_status' => 'required',
        ]);

        $user_id = Auth::user()->employee->user_id;
        $outlet_id = Auth::user()->employee->outlet_id;

        Adjusment::create([
            'id' => Str::uuid(), 
            'ingredient_id' => $request->ingredient_id, 
            'consumption_amount' => $request->consumption_amount, 
            'inventory_adjustment_id' => $iventory->id, 
            'consumption_status' => $request->consumption_status, 
            'outlet_id' => $outlet_id,
            'del_status' => 'Live',
        ]);

        return redirect()->route('iventory.addingredient', $iventory->id)->with('status', 'Modifier success is added.');

        // return redirect()->route('iventory')->with('status', 'Ingredeint success is added.');
    }
}
