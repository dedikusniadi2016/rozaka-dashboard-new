<?php

namespace App\Http\Controllers;

use App\FoodCategory;
use App\FoodmCategory;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class FoodCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('foodcategory');

        $foodcategories = FoodCategory::where('user_id', Auth::user()->employee->user_id)
                        ->where('outlet_id', Auth::user()->employee->outlet_id)->paginate(10);

        return view('foodcategories.foodcategories-list', [
            'foodcategories' => $foodcategories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('foodcategory');

        $foodmcategories = FoodmCategory::where('user_id', Auth::user()->employee->user_id)
                            ->where('outlet_id', Auth::user()->employee->outlet_id)->get();

        return view('foodcategories.foodcategories-add', [
            'foodmcategories' => $foodmcategories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('foodcategory');

        $this->validate($request, [
            'category_name' => 'required', 
            'ma_ca_id' => 'required', 
        ]);

        $user_id = Auth::user()->employee->user_id;
        $outlet_id = Auth::user()->employee->outlet_id;

        FoodCategory::create([
            'id' => Str::uuid(),
            'category_name' => $request->category_name, 
            'description' => $request->description, 
            'ma_ca_id' => $request->ma_ca_id, 
            'user_id' => $user_id, 
            'outlet_id' => $outlet_id, 
            'del_status' => "Live", 
        ]);

        return redirect()->route('foodc')->with('status', 'Category success is created.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FoodCategory  $foodCategory
     * @return \Illuminate\Http\Response
     */
    public function show(FoodCategory $foodCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FoodCategory  $foodCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(FoodCategory $foodcategory)
    {
        $this->authorize('foodcategory');
        $this->authorize('update', $foodcategory);

        $foodmcategories = FoodmCategory::where('user_id', Auth::user()->employee->user_id)
                            ->where('outlet_id', Auth::user()->employee->outlet_id)->get();

        return view('foodcategories.foodcategories-edit', [
            'foodmcategories' => $foodmcategories
        ])->with(compact('foodcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FoodCategory  $foodCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FoodCategory $foodcategory)
    {
        $this->authorize('foodcategory');
        $this->authorize('update', $foodcategory);

        $foodcategory->category_name = $request->get('category_name', $foodcategory->category_name);
        $foodcategory->description = $request->get('description', $foodcategory->description);
        $foodcategory->ma_ca_id = $request->get('ma_ca_id', $foodcategory->ma_ca_id);

        $foodcategory->save();

        return redirect()->route('foodc')->with('status', 'Category success is updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FoodCategory  $foodCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(FoodCategory $foodCategory)
    {
        //
    }
}
