<?php

namespace App\Http\Controllers;

use App\FoodMenu;
use App\FoodCategory;
use App\Ingredient;
use App\Unit;
use App\FoodMenuIngredient;
use App\Modifier;
use App\FoodModifier;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;

class FoodMenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('foodmenus');

        $foodmenus = FoodMenu::where('user_id', Auth::user()->employee->user_id)
                    ->where('outlet_id', Auth::user()->employee->outlet_id)->paginate(10);

        return view('foodmenus.foodmenus-list', [
            'foodmenus' => $foodmenus
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('foodmenus');

        $foodcategories = FoodCategory::where('user_id', Auth::user()->employee->user_id)
                        ->where('outlet_id', Auth::user()->employee->outlet_id)->get();

        return view('foodmenus.foodmenus-add', [
            'foodcategories' => $foodcategories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('foodmenus');

        $request->validate([
            'code' => 'required', 
            'name' => 'required', 
            'category_id' => 'required', 
            'sale_price' => 'required',
            'photo' => 'required'
        ]);

        $file = $request->file('photo');
        $file_name = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $file_size = $file->getSize();
        $destinationPath = 'uploads';
        $file->move($destinationPath, $file_name);

        $user_id = Auth::user()->employee->user_id;
        $outlet_id = Auth::user()->employee->outlet_id;

        FoodMenu::create([
            'id' => Str::uuid(),
            'code' => $request->code, 
            'name' => $request->name, 
            'description' => $request->description, 
            'category_id' => $request->category_id, 
            'sale_price' => $request->sale_price, 
            'tax_information' => NULL, 
            'vat_id' => NULL, 
            'user_id' => $user_id, 
            'outlet_id' => $outlet_id, 
            'photo' => $file_name, 
            'veg_item' => NULL, 
            'beverage_item' => NULL, 
            'bar_item' => NULL, 
            'del_status' => 'Live', 
        ]);

        return redirect()->route('foodmenus')->with('status', 'Food menu success is created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FoodMenu  $foodMenu
     * @return \Illuminate\Http\Response
     */
    public function show(FoodMenu $foodmenu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FoodMenu  $foodMenu
     * @return \Illuminate\Http\Response
     */
    public function edit(FoodMenu $foodmenu)
    {
        $this->authorize('foodmenus');
        $this->authorize('update', $foodmenu);

        $foodcategories = FoodCategory::where('user_id', Auth::user()->employee->user_id)
                        ->where('outlet_id', Auth::user()->employee->outlet_id)->get();

        return view('foodmenus.foodmenus-edit', [
            'foodcategories' => $foodcategories
        ])->with(compact('foodmenu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FoodMenu  $foodMenu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FoodMenu $foodmenu)
    {
        $this->authorize('foodmenus');
        $this->authorize('update', $foodmenu);

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $destinationPath = 'uploads';
            $file->move($destinationPath, $file->getClientOriginalName());

            $foodmenu->photo = $request->get('photo', $file->getClientOriginalName());
        }

        $foodmenu->code = $request->get('code', $foodmenu->code);
        $foodmenu->name = $request->get('name', $foodmenu->name);
        $foodmenu->description = $request->get('description', $foodmenu->description);
        $foodmenu->category_id = $request->get('category_id', $foodmenu->category_id);
        $foodmenu->sale_price = $request->get('sale_price', $foodmenu->sale_price);

        $foodmenu->save();

        return redirect()->route('foodmenus')->with('status', 'Food menu success is updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FoodMenu  $foodMenu
     * @return \Illuminate\Http\Response
     */
    public function destroy(FoodMenu $foodmenu)
    {
        //
    }

    // ingredient food
    public function ingredient(FoodMenu $foodmenu)
    {
        $this->authorize('foodmenus');
        $this->authorize('update', $foodmenu);

        $foodmenuingredients = FoodMenuIngredient::where('user_id', Auth::user()->employee->user_id)
                        ->where('outlet_id', Auth::user()->employee->outlet_id)
                        ->where('food_menu_id', $foodmenu->id)->paginate(10);
        

        $ingredients = Ingredient::where('user_id', Auth::user()->employee->user_id)->get();
        $units = Unit::all();


        return view('foodmenus.foodmenus-ingredient-list', [
            'foodmenuingredients' => $foodmenuingredients, 
            'ingredients' => $ingredients, 
            'units' => $units
        ])->with(compact('foodmenu'));
    }

    public function addingredient(Request $request, FoodMenu $foodmenu)
    {
        $this->authorize('foodmenus');
        $this->authorize('update', $foodmenu);

        $request->validate([
            'ingredient_id' => 'required', 
            'consumption' => 'required',
            'unit_id' => 'required',
        ]);

        $user_id = Auth::user()->employee->user_id;
        $outlet_id = Auth::user()->employee->outlet_id;

        FoodMenuIngredient::create([
            'id' => Str::uuid(), 
            'ingredient_id' => $request->ingredient_id, 
            'consumption' => $request->consumption, 
            'unit_id' => $request->unit_id, 
            'food_menu_id' => $foodmenu->id, 
            'user_id' => $user_id,
            'outlet_id' => $outlet_id,
            'del_status' => 'Live',
        ]);

        return redirect()->route('foodmenus.addingredient', $foodmenu->id)->with('status', 'Ingredeint success is added.');
    }

    public function destroyingredient(FoodMenuIngredient $foodmenuingredient)
    {
        $this->authorize('foodmenus');
        $this->authorize('delete', $foodmenuingredient);

        $foodmenuingredient->delete();

        return back();
    }


    // modifier food
    public function modifier(FoodMenu $foodmenu)
    {
        $this->authorize('foodmenus');
        $this->authorize('update', $foodmenu);

        $foodmodifiers = FoodModifier::where('user_id', Auth::user()->employee->user_id)
                        ->where('outlet_id', Auth::user()->employee->outlet_id)
                        ->where('food_menu_id', $foodmenu->id)->paginate(10);
        
        $modifiers = Modifier::where('user_id', Auth::user()->employee->user_id)
                    ->where('outlet_id', Auth::user()->employee->outlet_id)->get();


        return view('foodmenus.foodmenus-modifier-list', [
            'foodmodifiers' => $foodmodifiers, 
            'modifiers' => $modifiers
        ])->with(compact('foodmenu'));
    }

    public function addmodifier(Request $request, FoodMenu $foodmenu)
    {
        $this->authorize('foodmenus');
        $this->authorize('update', $foodmenu);

        $request->validate([
            'modifier_id' => 'required', 
        ]);

        $user_id = Auth::user()->employee->user_id;
        $outlet_id = Auth::user()->employee->outlet_id;

        FoodModifier::create([
            'id' => Str::uuid(), 
            'modifier_id' => $request->modifier_id, 
            'food_menu_id' => $foodmenu->id, 
            'user_id' => $user_id,
            'outlet_id' => $outlet_id,
            'del_status' => 'Live',
        ]);

        return redirect()->route('foodmenus.addmodifier', $foodmenu->id)->with('status', 'Modifier success is added.');
    }

    public function destroymodifier(FoodModifier $foodmodifier)
    {
        $this->authorize('foodmenus');
        $this->authorize('delete', $foodmodifier);

        $foodmodifier->delete();

        return back();
    }

    // food promo
}
