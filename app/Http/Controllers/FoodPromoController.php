<?php

namespace App\Http\Controllers;

use App\FoodPromo;
use App\FoodMenu;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class FoodPromoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('foodpromo');

        $foodpromos = FoodPromo::where('user_id', Auth::user()->employee->user_id)
                    ->where('outlet_id', Auth::user()->employee->outlet_id)->paginate(10);

        return view('foodpromos.foodpromos-list', [
            'foodpromos' => $foodpromos
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('foodpromo');

        $foodmenus = FoodMenu::where('user_id', Auth::user()->employee->user_id)
                    ->where('outlet_id', Auth::user()->employee->outlet_id)->get();

        return view('foodpromos.foodpromos-add', [
            'foodmenus' => $foodmenus
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('foodpromo');

        $request->validate([
            'promo_name' => 'required', 
            'promo_type' => 'required', 
            'promo_value' => 'required', 
            'date_start' => 'required', 
            'date_done' => 'required', 
            'food_menu_id' => 'required', 
        ]);

        $user_id = Auth::user()->employee->user_id;
        $outlet_id = Auth::user()->employee->outlet_id;

        FoodPromo::create([
            'id' => Str::uuid(),
            'user_id' => $user_id, 
            'outlet_id' => $outlet_id,
            'promo_name' => $request->promo_name, 
            'promo_type' => $request->promo_type, 
            'promo_value' => $request->promo_value, 
            'date_start' => $request->date_start, 
            'date_done' => $request->date_done, 
            'description' => $request->description, 
            'food_menu_id' => $request->food_menu_id, 
            'del_status' => 'Live',
        ]);

        return redirect()->route('foodp')->with('status', 'Promo success is created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FoodPromo  $foodPromo
     * @return \Illuminate\Http\Response
     */
    public function show(FoodPromo $foodPromo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FoodPromo  $foodPromo
     * @return \Illuminate\Http\Response
     */
    public function edit(FoodPromo $foodpromo)
    {
        $this->authorize('foodpromo');
        $this->authorize('update', $foodpromo);

        $foodmenus = FoodMenu::where('user_id', Auth::user()->employee->user_id)
                    ->where('outlet_id', Auth::user()->employee->outlet_id)->get();

        return view('foodpromos.foodpromos-edit', [
            'foodmenus' => $foodmenus
        ])->with(compact('foodpromo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FoodPromo  $foodPromo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FoodPromo $foodpromo)
    {
        $this->authorize('foodpromo');
        $this->authorize('update', $foodpromo);

        $foodpromo->food_menu_id = $request->get('food_menu_id', $foodpromo->food_menu_id);
        $foodpromo->outlet_id = $request->get('outlet_id', $foodpromo->outlet_id);
        $foodpromo->promo_name = $request->get('promo_name', $foodpromo->promo_name);
        $foodpromo->promo_type = $request->get('promo_type', $foodpromo->promo_type);
        $foodpromo->promo_value = $request->get('promo_value', $foodpromo->promo_value);
        $foodpromo->date_start = $request->get('date_start', $foodpromo->date_start);
        $foodpromo->date_done = $request->get('date_done', $foodpromo->date_done);
        $foodpromo->description = $request->get('description', $foodpromo->description);

        $foodpromo->save();

        return redirect()->route('foodp')->with('status', 'Promo success is updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FoodPromo  $foodPromo
     * @return \Illuminate\Http\Response
     */
    public function destroy(FoodPromo $foodPromo)
    {
        //
    }
}
