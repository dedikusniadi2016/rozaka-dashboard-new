<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Supplier;
use App\User;
use Auth;

class SuppliersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {

        $suppliers = Supplier::where('user_id', Auth::user()->employee->user_id)->paginate(10);
        return view('suppliers.suppliers-list', ['suppliers' => $suppliers]);
    }

    public function create()
    {
        return view('suppliers.suppliers-form');
    }

   public function edit(Supplier $supplier)
    {
        $suppliers = Supplier::where('user_id', Auth::user()->employee->user_id)->get();
       
        return view('suppliers.suppliers-edit', [
            'suppliers' => $suppliers,
        ])->with(compact('supplier'));

    }

     public function update(Request $request, Supplier $supplier)
    {
        
        $supplier->name = $request->get('name', $supplier->name);
        $supplier->contact_person = $request->get('contact_person', $supplier->contact_person);
        $supplier->email = $request->get('email', $supplier->email);
        $supplier->phone = $request->get('phone', $supplier->phone);
        $supplier->address = $request->get('address', $supplier->address);
        $supplier->description = $request->get('description', $supplier->description);
        $supplier->save();

        return redirect('suppliers')->with('status', ' Supplier success is updated.');
    }


    public function store(Request $request) 
    {

        $request->validate([
            'name' => 'required', 
            'contact_person' => 'required', 
            'email' => 'required|email', 
            'phone' => 'required',
            'address' => 'required', 
            'description' => 'required',
        ]);

       $user_id = Auth::user()->employee->user_id;

     Supplier::create([
            'id' => Str::uuid(),
            'name' => $request->name, 
            'contact_person' => $request->contact_person, 
            'email' => $request->email,
            'phone' => $request->phone, 
            'address' => $request->address,
            'description' => $request->description, 
            'user_id' => $user_id, 
            'supplier_id' => $request->supplier_id, 
            'del_status' => 'Live'
        ]);

    return redirect('suppliers')->with('success', 'Data pages telah ditambahkan');     
    
    }

}
