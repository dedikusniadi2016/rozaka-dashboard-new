<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Outlet;
use Auth;

class OutletController extends Controller
{
    public function __construct()
    {
        // parent::__construct();
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('outlets');
        $outlets = Outlet::where('user_id', Auth::user()->owner->id)->paginate(10);
        
        return view('outlets.outlet-list', ['outlets' => $outlets]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('outlets');

        return view('outlets.outlet-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        // return $request;
        $request->validate([
            'outlet_name' => 'required|min:3', 
            'outlet_code' => 'required', 
            'address' => 'required', 
            'phone' => 'required', 
            'invoice_print' => 'required', 
            'starting_date' => 'required|date:Y-m-d', 
            'invoice_footer' => 'required|min:3', 
        ]);
        
        $user_id = Auth::user()->owner->id;

        Outlet::create([
            'id' => Str::uuid(),
            'outlet_name' => $request->outlet_name, 
            'outlet_code' => $request->outlet_code, 
            'address' => $request->address, 
            'phone' => $request->phone, 
            'invoice_print' => $request->invoice_print, 
            'starting_date' => $request->starting_date, 
            'invoice_footer' => $request->invoice_footer, 
            'collect_tax' => '',
            'tax_title' => '',
            'tax_registration_no' => '',
            'tax_is_gst' => '',
            'state_code' => '',
            'pre_or_post_payment' => 'Post Payment', 
            'currency' => 'Rupiah', 
            'timezone' => 'Asia/Jakarta', 
            'date_format' => 'Y-m-d', 
            'user_id' => $user_id,
            'del_status' => 'Live', 
        ]);

        return redirect()->route('outlets')->with('status', 'Outlet success is created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Outlet $outlet)
    {
        $this->authorize('outlets');
        $this->authorize('update', $outlet);
        
        return view('outlets.outlet-edit', compact('outlet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Outlet $outlet)
    {   
        $this->authorize('outlets');
        $this->authorize('update', $outlet);

        $outlet->outlet_name = $request->get('outlet_name', $outlet->outlet_name);
        $outlet->outlet_code = $request->get('outlet_code', $outlet->outlet_code);
        $outlet->address = $request->get('address', $outlet->address);
        $outlet->phone = $request->get('phone', $outlet->phone);
        $outlet->invoice_print = $request->get('invoice_print', $outlet->invoice_print);
        $outlet->starting_date = $request->get('starting_date', $outlet->starting_date);
        $outlet->invoice_footer = $request->get('invoice_footer', $outlet->invoice_footer);

        $outlet->save();

        return redirect()->route('outlets')->with('status', 'Outlet success is updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
