<?php

namespace App\Http\Controllers;

use App\FoodmCategory;
use App\Outlet;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class FoodmCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('maincategory');
        
        $foodmcategories = FoodmCategory::where('user_id', Auth::user()->employee->user_id)
                        ->where('outlet_id', Auth::user()->employee->outlet_id)->paginate(10);

        return view('foodmcategories.foodmcategories-list', [
            'foodmcategories' => $foodmcategories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('maincategory');

        return view('foodmcategories.foodmcategories-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('maincategory');
        
        $request->validate([
            'ma_ca_name' => 'required',
        ]);

        $user_id = Auth::user()->employee->user_id;
        $outlet_id = Auth::user()->employee->outlet_id;

        FoodmCategory::create([
            'id' => Str::uuid(),
            'ma_ca_name' => $request->ma_ca_name, 
            'description' => $request->description, 
            'user_id' => $user_id, 
            'outlet_id' => $outlet_id, 
            'del_status' => "Live", 
        ]);

        return redirect()->route('foodm')->with('status', 'Main Category success is created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FoodmCategory  $foodmCategory
     * @return \Illuminate\Http\Response
     */
    public function show(FoodmCategory $foodmCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FoodmCategory  $foodmCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(FoodmCategory $foodmcategory)
    {
        $this->authorize('maincategory');
        $this->authorize('update', $foodmcategory);

        return view('foodmcategories.foodmcategories-edit', compact('foodmcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FoodmCategory  $foodmCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FoodmCategory $foodmcategory)
    {
        $this->authorize('maincategory');
        $this->authorize('update', $foodmcategory);

        $foodmcategory->ma_ca_name = $request->get('ma_ca_name', $foodmcategory->ma_ca_name);
        $foodmcategory->description = $request->get('description', $foodmcategory->description);
        $foodmcategory->del_status = $request->get('del_status', $foodmcategory->del_status);

        $foodmcategory->save();

        return redirect()->route('foodm')->with('status', 'Main Category success is updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FoodmCategory  $foodmCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(FoodmCategory $foodmCategory)
    {
        //
    }


    // public function search(Request $request) 
    // {
    //     $foodmcategories = FoodmCategory::when($request->q, function ($query) use ($request) {
    //     $query->where('category_name', 'LIKE', "%{$request->q}%")
    //     ->orWhere('description', 'LIKE', "%{$request->q}%");
    //     })->paginate(10);
  
    //     return view('foodmcategories.foodmcategories-list', ['foodmcategories' => $foodmcategories]);
  
    // }
}
