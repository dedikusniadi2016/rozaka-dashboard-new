<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Ingcat;
use Auth;

class IngcatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('ingcats');

        $ingcats = Ingcat::where('user_id', Auth::user()->employee->user_id)->paginate(10);

        return view('ingcats.ingcats-list', ['ingcats' => $ingcats]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('ingcats');

        return view('ingcats.ingcats-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('ingcats');

        $request->validate([
            'category_name' => 'required', 
        ]);

        $user_id = Auth::user()->employee->user_id;

        Ingcat::create([
            'id' => Str::uuid(),
            'category_name' => $request->category_name, 
            'description' => $request->description, 
            'user_id' => $user_id, 
            'del_status' => 'Live'
        ]);

        return redirect()->route('ingcats')->with('status', 'Category Ingredient success is created.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Ingcat $ingcat)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Ingcat $ingcat)
    {
        $this->authorize('ingcats');
        $this->authorize('update', $ingcat);

        return view('ingcats.ingcats-edit', compact('ingcat'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ingcat $ingcat)
    {
        $this->authorize('ingcats');
        $this->authorize('update', $ingcat);

        $ingcat->category_name = $request->get('category_name', $ingcat->category_name);
        $ingcat->description = $request->get('description', $ingcat->description);
        $ingcat->save();

        return redirect()->route('ingcats')->with('status', 'Category Ingredient success is updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request) 
    {
        $ingcats = Ingcat::when($request->q, function ($query) use ($request) {
        $query->where('category_name', 'LIKE', "%{$request->q}%")
        ->orWhere('description', 'LIKE', "%{$request->q}%");
        })->paginate(10);
  
        return view('ingcats.ingcats-list', ['ingcats' => $ingcats]);
  
    }
}
