<?php

namespace App\Http\Controllers;

use App\Modifier;
use App\Ingredient;
use App\Unit;
use App\ModifierIngredient;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ModifierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('modifiers');

        $modifiers = Modifier::where('user_id', Auth::user()->employee->user_id)
                    ->where('outlet_id', Auth::user()->employee->outlet_id)->paginate(10);

        return view('modifiers.modifiers-list', [
            'modifiers' => $modifiers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('modifiers');

        return view('modifiers.modifiers-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('modifiers');

        $request->validate([
            'name' => 'required', 
            'price' => 'required',
        ]);

        $user_id = Auth::user()->employee->user_id;
        $outlet_id = Auth::user()->employee->outlet_id;

        Modifier::create([
            'id' => Str::uuid(), 
            'name' => $request->name, 
            'price' => $request->price, 
            'description' => $request->description, 
            'user_id' => $user_id,
            'outlet_id' => $outlet_id,
            'del_status' => 'Live',
        ]);

        return redirect()->route('modifiers')->with('status', 'Modifier success is created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Modifier  $modifier
     * @return \Illuminate\Http\Response
     */
    public function show(Modifier $modifier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Modifier  $modifier
     * @return \Illuminate\Http\Response
     */
    public function edit(Modifier $modifier)
    {
        $this->authorize('modifiers');

        return view('modifiers.modifiers-edit', compact('modifier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Modifier  $modifier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Modifier $modifier)
    {
        $this->authorize('modifiers');
        $this->authorize('update', $modifier);

        $modifier->name = $request->get('name', $modifier->name);
        $modifier->price = $request->get('price', $modifier->price);
        $modifier->description = $request->get('description', $modifier->description);

        $modifier->save();

        return redirect()->route('modifiers')->with('status', 'Modifier success is updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Modifier  $modifier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Modifier $modifier)
    {
        //
    }

    public function ingredient(Modifier $modifier)
    {
        $this->authorize('modifiers');

        $modifieringredients = ModifierIngredient::where('user_id', Auth::user()->employee->user_id)
                        ->where('outlet_id', Auth::user()->employee->outlet_id)
                        ->where('modifier_id', $modifier->id)->paginate(10);
        
        $ingredients = Ingredient::where('user_id', Auth::user()->employee->user_id)->get();
        $units = Unit::all();


        return view('modifiers.modifiers-ingredient-list', [
            'modifieringredients' => $modifieringredients, 
            'ingredients' => $ingredients, 
            'units' => $units
        ])->with(compact('modifier'));
    }

    public function addingredient(Request $request, Modifier $modifier)
    {
        $this->authorize('modifiers');

        $request->validate([
            'ingredient_id' => 'required', 
            'consumption' => 'required',
            'unit_id' => 'required',
        ]);

        $user_id = Auth::user()->employee->user_id;
        $outlet_id = Auth::user()->employee->outlet_id;

        ModifierIngredient::create([
            'id' => Str::uuid(), 
            'ingredient_id' => $request->ingredient_id, 
            'consumption' => $request->consumption, 
            'consumption' => $request->consumption, 
            'unit_id' => $request->unit_id, 
            'modifier_id' => $modifier->id, 
            'user_id' => $user_id,
            'outlet_id' => $outlet_id,
            'del_status' => 'Live',
        ]);

        return redirect()->route('modifiers.addingredient', $modifier->id)->with('status', 'Ingredeint success is added.');
    }

    public function destroyingredient(ModifierIngredient $modifieringredient)
    {
        $this->authorize('modifiers');
        $this->authorize('delete', $modifieringredient);

        $modifieringredient->delete();

        return back();
    }
}
