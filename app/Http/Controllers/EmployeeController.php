<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Employee;
use App\Outlet;
use App\User;
use Auth;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('employees');

        if(Auth::user()->level === 'Owner') {
            $employees = Employee::where('user_id', Auth::user()->owner->id)->paginate(10);
        } elseif(Auth::user()->level === 'Admin') {
            $employees = Employee::where('user_id', Auth::user()->employee->user_id)->paginate(10);
        }
        
        return view('employees.employee-list', ['employees' => $employees]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('employees');

        if(Auth::user()->level === 'Owner') {
            $outlets = Outlet::where('user_id', Auth::user()->owner->id)->get();
        } elseif(Auth::user()->level === 'Admin') {
            $outlets = Outlet::where('user_id', Auth::user()->employee->user_id)->get();
        }
        return view('employees.employee-add', ['outlets' => $outlets]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('employees');

        $request->validate([
            'name' => 'required', 
            'designation' => 'required', 
            'phone' => 'required', 
            'outlet_id' => 'required',
            'password' => 'required|min:6', 
            'email' => 'required|email|unique:users', 
        ]);

        $user_id = Auth::user()->owner->id;

        DB::beginTransaction();
        $user = User::create([
            'id' => Str::uuid(),
            'name' => $request->name, 
            'email' => $request->email, 
            'level' => $request->designation,
            'password' => bcrypt($request->password),
            'api_token' => bcrypt($request->email)
        ]);

        Employee::create([
            'id' => Str::uuid(),
            'name' => $request->name, 
            'designation' => $request->designation, 
            'phone' => $request->phone, 
            'description' => $request->description, 
            'user_id' => $user_id, 
            'outlet_id' => $request->outlet_id, 
            'user_login_id' => $user->id, 
            'del_status' => 'Live'
        ]);
        DB::commit();

        return redirect()->route('employees')->with('status', 'Employees success is created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $this->authorize('employees');
        
        if(Auth::user()->level === 'Owner') {
            $this->authorize('update', $employee);
            $outlets = Outlet::where('user_id', Auth::user()->owner->id)->get();
        } elseif(Auth::user()->level === 'Admin') {
            $this->authorize('updatebyadmin', $employee);
            $outlets = Outlet::where('user_id', Auth::user()->employee->user_id)->get();
        }
        
        return view('employees.employee-edit', ['outlets' => $outlets])->with(compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        if(Auth::user()->level === 'Owner') {
            $this->authorize('update', $employee);
        } elseif(Auth::user()->level === 'Admin') {
            $this->authorize('updatebyadmin', $employee);
        }

        $employee->name = $request->get('name', $employee->name);
        $employee->designation = $request->get('designation', $employee->designation);
        $employee->phone = $request->get('phone', $employee->phone);
        $employee->outlet_id = $request->get('outlet_id', $employee->outlet_id);

        $employee->user->name = $request->get('name', $employee->user->name);
        $employee->user->level = $request->get('designation', $employee->user->level);
        $employee->user->email = $request->get('email', $employee->user->email);
        if (!empty($request->password)) {
            $employee->user->password = bcrypt($request->get('password'));
        }
        

        $employee->save();
        $employee->user->save();

        return redirect()->route('employees')->with('status', 'Employee success is updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // public function search(Request $request) 
    // {
    //     $employees = Employee::when($request->q, function ($query) use ($request) {
    //               $query->where('name', 'LIKE', "%{$request->q}%")
    //                     ->orWhere('designation', 'LIKE', "%{$request->q}%");
    //               })->paginate(10);
  
    //               return view('employees.employee-list', ['employees' => $employees]);
  
    // }   
}
