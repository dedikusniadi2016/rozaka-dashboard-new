<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Table;
use App\User;
use App\Outlet;
use Auth;

class tableController extends Controller
{
  public function __construct() {

        $this->middleware('auth');
  }

  public function index() {

        $tables = Table::where('user_id', Auth::user()->employee->user_id)->paginate(10);
        return view('Tables.tables-list', ['tables' => $tables]);
    }

    public function create() {

        return view('Tables.tables-form');
    }

     public function edit(Table $table) {

        $tables = Table::where('user_id', Auth::user()->employee->user_id)->get();
       
        return view('tables.tables-edit', [
            'tables' => $tables,
        ])->with(compact('table'));

    }

    public function update(Request $request, Table $table) {
       
        $table->name = $request->get('name', $table->name);
        $table->sit_capacity = $request->get('sit_capacity', $table->sit_capacity);
        $table->position = $request->get('position', $table->position);
        $table->description = $request->get('description', $table->description);
        $table->save();

        return redirect('tables')->with('status', 'Table success is updated.');
    }


    public function store(Request $request)  {
        $request->validate([
            'name' => 'required', 
            'sit_capacity' => 'required', 
            'position' => 'required', 
            'description' => 'required',
        ]);

       $user_id = Auth::user()->employee->user_id;
       $outlet_id = Auth::user()->employee->outlet_id;

     Table::create([
            'id' => Str::uuid(),
            'name' => $request->name, 
            'sit_capacity' => $request->sit_capacity, 
            'position' => $request->position,
            'description' => $request->description, 
            'user_id' => $user_id, 
            'outlet_id' => $outlet_id,
            'del_status' => 'Live'
        ]);

        return redirect('tables')->with('success', 'Data Tabels telah ditambahkan');     
    
    }
}
