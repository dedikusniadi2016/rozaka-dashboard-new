<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Wastes;
use App\User;
use App\Employee;
use App\FoodMenu;
use App\Outlet;
use App\WastesIgredient;
use App\Ingredient;
use Auth;

class WastesController extends Controller
{
    public function index()
    {
    //     $Wastes = Wastes::where('user_id', Auth::user()->employee->user_id)->paginate(10);
    //    return view('Wastes.wastes-list', ['Wastes' => $Wastes]);  
    

    $foodmenu = FoodMenu::where('user_id', Auth::user()->employee->user_id)->get();
    $Wastes = Wastes::where('user_id', Auth::user()->employee->user_id)->paginate(10);

       
    return view('Wastes.wastes-list', [
        'Wastes' => $Wastes,
        'foodmenu' => $foodmenu
    ])->with(compact('Wastes'));
    

    }

    public function create()
    {
        $foodmenu = FoodMenu::where('user_id', Auth::user()->employee->user_id)->get();
       
        return view('Wastes.wastes-form', [
            'foodmenu' => $foodmenu,
        ])->with(compact('foodmenu'));
    }


    public function store(Request $request)
    {
       $request->validate([
            'reference_no' => 'required',
            'food_menu_id' => 'required', 
            'date' => 'required', 
            'total_loss' => 'required',
            'note' => 'required', 
            'food_menu_waste_qty' => 'required',
        ]);

        $user_id = Auth::user()->employee->user_id;
        $outlet_id = Auth::user()->employee->outlet_id;
        $employee_id = Auth::user()->employee->id;

        Wastes::create([
            'id' => Str::uuid(),
            'reference_no' => $request->reference_no, 
            'date' => $request->date, 
            'total_loss' => $request->total_loss,
            'note' => $request->note, 
            'employee_id' => $employee_id, 
            'user_id' => $user_id, 
            'outlet_id' => $outlet_id, 
            'del_status' => 'Live',
            'food_menu_id' => $request->food_menu_id,
            'food_menu_waste_qty' => $request->food_menu_waste_qty
        ]);

        return redirect()->route('wastes')->with('status', 'Iventory success is created.');

    }

     public function show($id)
    {
        //
    }


    public function destroyingredient( $id, WastesIgredient $WastesIgredient)
    {
        $wastesingredients = WastesIgredient::find($id);
        $wastesingredients->delete();
        return redirect()->route('wastes')->with('status', 'Ingredeint success is added.');
    }


     public function addingredient(Request $request, Wastes $wastes)
    {
        $request->validate([
            'ingredient_id' => 'required', 
            'waste_amount' => 'required',
            'last_purchase_price' => 'required',
            'loss_amount' => 'required',
        ]);

        $outlet_id = Auth::user()->employee->outlet_id;

        WastesIgredient::create([
            'id' => Str::uuid(), 
            'ingredient_id' => $request->ingredient_id, 
            'waste_amount' => $request->waste_amount, 
            'last_purchase_price' => $request->last_purchase_price,
            'loss_amount' => $request->loss_amount, 
            'waste_id' => $wastes->id,
            'outlet_id' => $outlet_id,
            'del_status' => 'Live',
        ]);

        return redirect()->route('wastes.addingredient', $wastes->id)->with('status', 'Ingredeint success is added.');
    }


  public function ingredient(WastesIgredient $WastesIgredient, wastes $wastes)
    {

     $wastesingredients = WastesIgredient::where('outlet_id', Auth::user()->employee->outlet_id)
                        ->where('waste_id', $wastes->id)->paginate(10);

        $ingredients = Ingredient::where('user_id', Auth::user()->employee->user_id)->get();
       
        return view('Wastes.wastes-ingredient-list', [
            'wastesingredients' => $wastesingredients, 
            'ingredients' => $ingredients,
        ])->with(compact('wastes'));
    }


    public function edit(Wastes $Wastes)
    {
        $foodmenu = FoodMenu::where('user_id', Auth::user()->employee->user_id)->get();
       
        return view('Wastes.wastes-edit', [
            'foodmenu' => $foodmenu,
        ])->with(compact('Wastes'));

    }

    public function update(Request $request, Wastes $Wastes)
    {
        $Wastes->reference_no = $request->get('reference_no', $Wastes->reference_no);
        $Wastes->food_menu_id = $request->get('food_menu_id', $Wastes->food_menu_id);
        $Wastes->date = $request->get('date', $Wastes->date);
        $Wastes->total_loss = $request->get('total_loss', $Wastes->total_loss);
        $Wastes->note = $request->get('Wastes', $Wastes->note);
        $Wastes->food_menu_waste_qty = $request->get('food_menu_waste_qty', $Wastes->food_menu_waste_qty);
        
        $Wastes->save();

        return redirect('wastes')->with('status', 'Table success is updated.');
    }

    public function destroy($id)
    {
        //
    }
}
