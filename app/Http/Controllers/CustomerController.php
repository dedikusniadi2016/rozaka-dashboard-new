<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Customer;
use App\Outlet;
use App\User;
use Auth;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('customers');
        $customers = Customer::where('user_id', Auth::user()->employee->user_id)
                    ->where('outlet_id', Auth::user()->employee->outlet_id)->paginate(10);

        return view('customers.customer-list', ['customers' => $customers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('customers');
        $outlets = Outlet::where('user_id', Auth::user()->employee->user_id)->get();
       
        return view('customers.customer-add', ['outlets' => $outlets]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('customers');

        $request->validate([
            'name' => 'required', 
            'phone' => 'required', 
            'email' => 'required|email|unique:users', 
            'address' => 'required',
            'date_of_birth' => 'required',
        ]);

        $user_id = Auth::user()->employee->user_id;
        $outlet_id = Auth::user()->employee->outlet_id;

        Customer::create([
            'id' => Str::uuid(),
            'name' => $request->name, 
            'phone' => $request->phone, 
            'email' => $request->email, 
            'address' => $request->address, 
            'date_of_birth' => $request->date_of_birth,
            'user_id' => $user_id, 
            'outlet_id' => $outlet_id, 
            'del_status' => 'Live'
        ]);

        return redirect()->route('customers')->with('status', 'Customer success is created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        $this->authorize('customers');
        $this->authorize('update', $customer);

        $outlets = Outlet::where('user_id', Auth::user()->employee->user_id)->get();

        return view('customers.customer-edit', ['outlets' => $outlets])->with(compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $this->authorize('customers');
        $this->authorize('update', $customer);

        $customer->name = $request->get('name', $customer->name);
        $customer->phone = $request->get('phone', $customer->phone);
        $customer->email = $request->get('email', $customer->email);
        $customer->address = $request->get('address', $customer->address);
        $customer->date_of_birth = $request->get('date_of_birth', $customer->date_of_birth);
        $customer->save();

        return redirect()->route('customers')->with('status', 'Customer success is updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request) 
    {
        $customers = Customer::when($request->q, function ($query) use ($request) {
                  $query->where('name', 'LIKE', "%{$request->q}%")
                        ->orWhere('email', 'LIKE', "%{$request->q}%");
                  })->paginate(10);
  
                  return view('customers.customer-list', ['customers' => $customers]);
  
    }
}
