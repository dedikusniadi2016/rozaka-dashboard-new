<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Ingredient;
use App\Ingcat;
use App\Unit;
use Auth;

class IngredientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('ingredients');
        $ingredients = Ingredient::where('user_id', Auth::user()->employee->user_id)->paginate(10);

        return view('ingredients.ingredients-list', ['ingredients' => $ingredients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('ingredients');

        $ingcats = Ingcat::where('user_id', Auth::user()->employee->user_id)->get();
        $units = Unit::all();

        return view('ingredients.ingredients-add', [
            'ingcats' => $ingcats,
            'units' => $units
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('ingredients');

        $request->validate([
            'code' => 'required', 
            'name' => 'required', 
            'category_id' => 'required', 
            'purchase_price' => 'required',
            'alert_quantity' => 'required|numeric', 
            'unit_id' => 'required', 
        ]);

        $user_id = Auth::user()->employee->user_id;

        Ingredient::create([
            'id' => Str::uuid(),
            'code' => $request->code, 
            'name' => $request->name, 
            'category_id' => $request->category_id, 
            'purchase_price' => $request->purchase_price, 
            'alert_quantity' => $request->alert_quantity, 
            'unit_id' => $request->unit_id, 
            'user_id' => $user_id, 
            'del_status' => 'Live'
        ]);

        return redirect()->route('ingredients')->with('status', 'Ingredient success is created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function search(Request $request) 
    {
        $ingredients = Ingredient::when($request->q, function ($query) use ($request) {
        $query->where('name', 'LIKE', "%{$request->q}%")
        ->orWhere('purchase_price', 'LIKE', "%{$request->q}%");
        })->paginate(10);
  
      return view('ingredients.ingredients-list', ['ingredients' => $ingredients]);
  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Ingredient $ingredient)
    {
        $this->authorize('ingredients');

        $ingcats = Ingcat::where('user_id', Auth::user()->employee->user_id)->get();
        $units = Unit::all();

        return view('ingredients.ingredients-edit', [
            'ingcats' => $ingcats,
            'units' => $units
        ])->with(compact('ingredient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ingredient $ingredient)
    {
        $this->authorize('ingredients');
        $this->authorize('update', $ingredient);

        $ingredient->code = $request->get('code', $ingredient->code);
        $ingredient->name = $request->get('name', $ingredient->name);
        $ingredient->category_id = $request->get('category_id', $ingredient->category_id);
        $ingredient->purchase_price = $request->get('purchase_price', $ingredient->purchase_price);
        $ingredient->alert_quantity = $request->get('alert_quantity', $ingredient->alert_quantity);
        $ingredient->unit_id = $request->get('unit_id', $ingredient->unit_id);
        $ingredient->save();

        return redirect()->route('ingredients')->with('status', 'Category Ingredient success is updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
