<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Outlet;
use App\FoodMenu;
use App\Employee;
use App\Wastesingredients;

class Wastes extends Model {

    protected $table = 'tbl_wastes';
    protected $primaryKey = "id";
    public $incrementing = false;

    protected $fillable = [
        'id', 'reference_no', 'date', 'total_loss', 'note', 
        'employee_id', 'user_id', 'outlet_id', 'del_status', 'food_menu_id', 'food_menu_waste_qty'
    ];

    protected $hidden = [
        'user_id', 'del_status', 'employee_id'
    ];

    
    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

     public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id');
    }

      public function wastesingredients()
    {
        return $this->hasMany(Wastesingredients::class, 'waste_id');
    }
    public function foodmenu()
    {
        return $this->belongsTo(FoodMenu::class, 'id');
    }

    public function employee() {
        return $this->hasMany(Employee::class,'employee_id');
    }
}