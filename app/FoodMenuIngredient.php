<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Ingredient;
use App\FoodMenu;
use App\Unit;

class FoodMenuIngredient extends Model
{
    protected $table = "tbl_food_menus_ingredients";
    protected $primaryKey = "id";
    public $incrementing = false;

    protected $fillable = [
        'id', 'ingredient_id', 'consumption', 'unit_id', 'food_menu_id', 'user_id', 'outlet_id', 'del_status', 
    ];

    protected $hidden = [
        'user_id', 'outlet_id', 'del_status', 
    ];

    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function ingredient()
    {
        return $this->belongsTo(Ingredient::class, 'ingredient_id');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'unit_id');
    }

    public function foodmenu()
    {
        return $this->belongsTo(FoodMenu::class, 'food_menu_id');
    }
}
