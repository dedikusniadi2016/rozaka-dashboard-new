<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Outlet;
use App\ModifierIngredient;
use App\FoodModifier;

class Modifier extends Model
{
    protected $table = "tbl_modifiers";
    protected $primaryKey = "id";
    public $incrementing = false;

    protected $fillable = [
        'id', 'name', 'price', 'description', 'user_id', 'outlet_id', 'del_status', 
    ];

    protected $hidden = [
        'outlet_id', 'user_id', 
    ];

    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id');
    }

    public function ingredients()
    {
        return $this->hasMany(ModifierIngredient::class, 'modifier_id');
    }

    public function foodmodifiers()
    {
        return $this->hasMany(FoodModifier::class, 'modifier_id');
    }
}
