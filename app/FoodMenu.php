<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Outlet;
use App\FoodCategory;
use App\FoodMenuIngredient;
use App\FoodMenu;
use App\Wastes;

class FoodMenu extends Model
{
    protected $table = "tbl_food_menus";
    protected $primaryKey = "id";
    public $incrementing = false;

    protected $fillable = [
        'id', 'code', 'name', 'category_id', 'description', 'sale_price', 'tax_information', 'vat_id', 'user_id', 'outlet_id', 'photo', 'veg_item', 'beverage_item', 'bar_item', 'del_status', 
    ];

    protected $hidden = [
        'user_id', 'outlet_id', 'category_id',
    ];

    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id');
    }

    public function foodcategory()
    {
        return $this->belongsTo(FoodCategory::class, 'category_id');
    }

    public function foodmenuingredients()
    {
        return $this->hasMany(FoodMenuIngredient::class, 'food_menu_id');
    }

    public function foodmenu()
     {
        return $this->hasMany(Wastes::class, 'id');
    }

    public function foodpromos()
    {
        return $this->hasMany(FoodPromo::class, 'id');
    }
}
