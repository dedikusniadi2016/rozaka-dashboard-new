<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Ingredient;
use App\Iventory;

class Adjusment extends Model
{
    protected $table = "tbl_inventory_adjustment_ingredients";
    protected $primaryKey = "id";
    public $incrementing = false;

    protected $fillable = [
        'id', 'ingredient_id', 'consumption_amount', 'inventory_adjustment_id', 'consumption_status', 
         'outlet_id', 'user_id', 'del_status', 
    ];

    protected $hidden = [
        'user_id', 'outlet_id', 'del_status', 
    ];

    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function ingredient()
    {
        return $this->belongsTo(Ingredient::class, 'ingredient_id');
    }

    public function iventory()
    {
        return $this->hasMany(iventory::class, 'inventory_adjustment_id');
    }
}
    