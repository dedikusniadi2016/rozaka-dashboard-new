<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Owner extends Model
{
    protected $table = 'tbl_users';
    protected $primaryKey = 'id';
    public $incrementing = false;


    protected $fillable = [
        'id', 'full_name', 'phone', 'email_address', 'card_name', 'card_number', 'user_login_id', 'language', 'active_status', 'del_status',
    ];

    protected $hidden = [
        'user_login_id', 'active_status', 'del_status', 
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_login_id');
    }
}
