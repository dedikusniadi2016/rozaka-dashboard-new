<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Outlet;

class Customer extends Model
{
    protected $table = "tbl_customers";
    protected $primaryKey = "id";
    public $incrementing = false;

    protected $fillable = [
        'id', 'name', 'phone', 'email', 'address', 'date_of_birth', 'outlet_id', 'user_id', 'del_status'
    ];

    protected $hidden = [
        'user_id', 'outlet_id', 
    ];

    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_login_id');
    }

    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id');
    }
}