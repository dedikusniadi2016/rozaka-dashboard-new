<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Employee;
use App\Iventory;
use App\FoodmCategory;
use App\FoodCategory;
use App\FoodMenu;
use App\Modifier;
use App\ModifierIngredient;
use App\FoodPromo;

class Outlet extends Model
{
    protected $table = 'tbl_outlets';
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $fillable = [
        'id', 'outlet_name', 'outlet_code', 'address', 'phone', 'invoice_print', 'starting_date', 'invoice_footer', 'collect_tax', 'tax_title', 'tax_registration_no', 'tax_is_gst', 'state_code', 'pre_or_post_payment', 'currency', 'timezone', 'date_format', 'user_id', 'del_status',
    ];

    protected $hidden = [
        'user_id', 
    ];

    //scope for order
    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function employees()
    {
        return $this->hasMany(Employee::class, 'outlet_id');
    }

    public function foodmcategories()
    {
        return $this->hasMany(FoodmCategory::class, 'outlet_id');
    }

    public function foodcategories()
    {
        return $this->hasMany(FoodCategory::class, 'outlet_id');
    }

    public function foodmenus()
    {
        return $this->hasMany(FoodMenu::class, 'outlet_id');
    }

    public function iventory() {
        return $this->hasMany(Iventory::class,'outlet_id');
    }

    public function modifiers()
    {
        return $this->hasMany(Modifier::class, 'outlet_id');
    }

    public function modifieringredients()
    {
        return $this->hasMany(ModifierIngredient::class, 'outlet_id');
    }

    public function foodpromos()
    {
        return $this->hasMany(FoodPromo::class, 'outlet_id');
    }
}
